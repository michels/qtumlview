TARGET = UMLView
TEMPLATE = lib
QT += svg
CONFIG += precompile_header
DEFINES += UMLVIEW_LIBRARY

SOURCES += src/umlviewplugin.cpp \
    src/umlviewmode.cpp \
    src/qtumlviewprototype.cpp \
    src/GraphCalc/graphinit.cpp \
    src/View/graphview.cpp \
    src/View/graphscene.cpp \
    src/View/UMLItems/umlnote.cpp \
    src/View/UMLItems/umllineselector.cpp \
    src/View/UMLItems/umlitemcreator.cpp \
    src/View/UMLItems/umlitem.cpp \
    src/View/UMLItems/Connector/umlgeneralization.cpp \
    src/View/UMLItems/Connector/umlconnector.cpp \
    src/View/UMLItems/Connector/umlcomposition.cpp \
    src/View/UMLItems/Connector/umlassociation.cpp \
    src/View/UMLItems/Connector/umlaggregation.cpp \
    src/View/UMLItems/Connector/graphicsconnector.cpp \
    src/View/UMLItems/UMLClass/umlclassentry.cpp \
    src/View/UMLItems/UMLClass/umlclass.cpp \
    src/View/UMLItems/UMLClass/umlbutton.cpp \
    src/View/UMLItems/UMLClass/svgrenderer.cpp \
    src/View/UMLItems/UMLClass/classlayout.cpp \
    src/model/umlmodel.cpp \
    src/model/refactoring.cpp \
    src/model/modelclasses.cpp \
    src/model/modelclass.cpp \
    src/model/modelmodule.cpp \
    src/model/modeldatastore.cpp \
    src/View/UMLItems/umlitemlist.cpp


HEADERS += pch.h \
    src/umlviewplugin.h\
    src/umlview_global.h\
    src/umlviewconstants.h \
    src/umlviewmode.h \
    src/qtumlviewprototype.h \
    src/GraphCalc/graphinit.h \
    src/View/graphview.h \
    src/View/graphscene.h \
    src/View/UMLItems/umlnote.h \
    src/View/UMLItems/umllineselector.h \
    src/View/UMLItems/umlitemcreator.h \
    src/View/UMLItems/umlitem.h \
    src/View/UMLItems/Connector/umlgeneralization.h \
    src/View/UMLItems/Connector/umlconnector.h \
    src/View/UMLItems/Connector/umlcomposition.h \
    src/View/UMLItems/Connector/umlassociation.h \
    src/View/UMLItems/Connector/umlaggregation.h \
    src/View/UMLItems/Connector/graphicsconnector.h \
    src/View/UMLItems/UMLClass/umlclassentry.h \
    src/View/UMLItems/UMLClass/umlclass.h \
    src/View/UMLItems/UMLClass/umlbutton.h \
    src/View/UMLItems/UMLClass/svgrenderer.h \
    src/View/UMLItems/UMLClass/classlayout.h \
    src/model/umlmodel.h \
    src/model/refactoring.h \
    src/model/modelclasses.h \
    src/model/modelclass.h \
    src/model/modelmodule.h \
    src/model/modeldatastore.h \
    src/View/UMLItems/umlitemlist.h

OTHER_FILES = UMLView.pluginspec \
    umlview_dependencies.pri \
    UMLView.pluginspec.in

RESOURCES += \
    resource/resource.qrc

INCLUDEPATH += src
PRECOMPILED_HEADER = pch.h


# Qt Creator linking
IDE_SOURCE_TREE = $$(QTC_SOURCE)
IDE_BUILD_TREE = $$(QTC_BUILD)
isEmpty(IDE_SOURCE_TREE):IDE_SOURCE_TREE=/home/jenkins/lib/qt-creator
isEmpty(IDE_BUILD_TREE):IDE_BUILD_TREE=/home/jenkins/lib/qtcreator-build

USE_USER_DESTDIR = yes
PROVIDER = BartschatSprauer

include($$IDE_SOURCE_TREE/src/qtcreatorplugin.pri)
include($$IDE_SOURCE_TREE/src/plugins/coreplugin/coreplugin.pri)
include(umlview_dependencies.pri)

LIBS += -L$$IDE_PLUGIN_PATH/Digia

INCLUDEPATH += $${_PRO_FILE_PWD_}/../3rdParty/OGDF/
unix:release:LIBS += -L$${_PRO_FILE_PWD_}/../3rdParty/OGDF/_release
unix:debug:LIBS += -L$${_PRO_FILE_PWD_}/../3rdParty/OGDF/_debug
win32:release:LIBS += -L$${_PRO_FILE_PWD_}/../3rdParty/OGDF/Win32/Release -lpsapi
win32:debug:LIBS += -L$${_PRO_FILE_PWD_}/../3rdParty/OGDF/Win32/Debug -lpsapi

LIBS += -lOGDF
