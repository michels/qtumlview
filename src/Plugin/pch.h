#ifndef PCH_H
#define PCH_H

#if defined __cplusplus
#include <QtGlobal>
#ifdef Q_WS_WIN
# define _POSIX_
# include <limits.h>
# undef _POSIX_
#endif

#include <QCoreApplication>
#include <QList>
#include <QVariant>
#include <QObject>
#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QTextCodec>
#include <QPointer>
#include <QScopedPointer>
#include <QSharedPointer>
#include <QDebug>

#include <stdlib.h>

#include <QEvent>
#include <QTimer>
#include <QApplication>
#include <QBitmap>
#include <QCursor>
#include <QDesktopWidget>
#include <QImage>
#include <QLayout>
#include <QPainter>
#include <QPixmap>
#include <QStyle>
#include <QWidget>

#include <QtSvg>

#endif

//QT includes
#ifndef UMLVIEW_LIBRARY
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QFontMetricsF>
#include <QPainter>
#endif


//QT-Creator includes
#include <cplusplus/LookupContext.h>
#include <cplusplus/DependencyTable.h>
#include <cplusplus/CppDocument.h>
#include <cplusplus/CPlusPlus.h>
#include <cplusplus/Overview.h>
#include <cplusplus/Symbol.h>
#include <cplusplus/Symbols.h>
#include <cplusplus/FindUsages.h>
#include <cplusplus/TypeOfExpression.h>
#include <cplusplus/FullySpecifiedType.h>
#include <cplusplus/SymbolVisitor.h>
#include <cpptools/cpprefactoringchanges.h>
#include <cpptools/insertionpointlocator.h>
#include <cpptools/cppfindreferences.h>
#include <cpptools/cppmodelmanager.h>
#include <cpptools/cppfindreferences.h>
#include <texteditor/basefilefind.h>
#include <find/searchresultwindow.h>

using namespace CppTools;
using namespace CPlusPlus;
#endif // PCH_H
