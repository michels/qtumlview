#ifndef UMLVIEW_H
#define UMLVIEW_H

#include "umlview_global.h"

#include <extensionsystem/iplugin.h>
#include <CppDocument.h>

namespace ProjectExplorer {
class Project;
}

namespace UMLView {
namespace Internal {

class UMLViewMode;

class UMLVIEWSHARED_EXPORT UMLViewPlugin : public ExtensionSystem::IPlugin
{
    Q_OBJECT
    
public:
    UMLViewPlugin();
    ~UMLViewPlugin();
    
    bool initialize(const QStringList &arguments, QString *errorString);
    void extensionsInitialized();
    ShutdownFlag aboutToShutdown();

#ifdef WITH_TESTS
    // codegen tests
    void test_codegen_roundtrip_read();
    void test_codegen_roundtrip_write();
#endif

private slots:
    void currentProjectChanged(ProjectExplorer::Project * proj);

private:
    void connectTheSignals();
    UMLViewMode *m_mode;
};

} // namespace Internal
} // namespace UMLView

#endif // UMLVIEW_H

