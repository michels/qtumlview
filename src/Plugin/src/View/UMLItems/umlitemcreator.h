#ifndef UMLITEMCREATOR_H
#define UMLITEMCREATOR_H

#include <QObject>
#include <QGraphicsItem>
#include <View/UMLItems/umlitem.h>

class GraphScene;
class UMLItemList;
namespace UMLView { namespace Model {
class ModelClass;
}}

class UMLItemCreator : public QObject
{
    Q_OBJECT
public:
    static UMLItemCreator* getInstance();
    static void release();

    void setScene( GraphScene* );
    QGraphicsItem *getUMLParentItem( QGraphicsItem* );
    void setMousePos( QPointF );

private:
    UMLItemCreator(QObject *parent = 0);
    static UMLItemCreator* theInstance;

    UMLItemList* umlItemList;
    qint32 counter;

    GraphScene* graphScene;
    bool createConnection;
    UMLItem::Type itemType;
    QPointF mousePos;
    QGraphicsItem* currentItem;
    QGraphicsItem* startItem;
    
signals:
    
public slots:
    //void createClass( QPointF );
    void createClass( UMLView::Model::ModelClass* );
    void createGeneralization( QGraphicsItem* );
    void createComposition( QGraphicsItem* );
    void createAssociation( QGraphicsItem* );
    void createAggregation( QGraphicsItem* );
    void setEndItem( QGraphicsItem*, bool create = true );
    
};

#endif // UMLITEMCREATOR_H
