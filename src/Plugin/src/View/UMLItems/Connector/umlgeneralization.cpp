#include "umlgeneralization.h"

#include "View/UMLItems/umlitem.h"
#include "View/UMLItems/umlitemlist.h"

UMLGeneralization::UMLGeneralization(QGraphicsItem *parent) :
    UMLConnector( parent )
{
    type = UMLItem::GeneralizationType;
}

void UMLGeneralization::setStopPoint( UMLItem *item, bool create )
{
    UMLConnector::setStopPoint( item );

    lines.at( 1 )->setType( UMLItem::GeneralizationType );
    foreach( GraphicsConnector *i, lines ){
        i->updateConnector();
    }
}

void UMLGeneralization::delConnector()
{
    UMLConnector::delConnector();
    UMLItemList::getInstance()->delGeneralization( this );
}
