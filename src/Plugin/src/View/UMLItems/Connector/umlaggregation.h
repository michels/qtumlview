#ifndef UMLAGGREGATION_H
#define UMLAGGREGATION_H

#include "View/UMLItems/Connector/umlconnector.h"

class UMLAggregation : public UMLConnector
{
public:
    UMLAggregation( QGraphicsItem *parent = 0 );

    void setStopPoint( UMLItem *item, bool create = true );
    void delConnector();
};

#endif // UMLAGGREGATION_H
