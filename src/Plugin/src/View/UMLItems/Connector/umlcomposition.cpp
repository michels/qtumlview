#include "umlcomposition.h"
#include "View/UMLItems/UMLClass/umlclass.h"
#include "View/UMLItems/umlitemlist.h"

UMLComposition::UMLComposition( QGraphicsItem *parent ) :
    UMLConnector( parent )
{
    type = UMLItem::CompositionType;
}

void UMLComposition::setStopPoint( UMLItem *item, bool create )
{
    UMLConnector::setStopPoint( item );

    lines.at( 1 )->setType( UMLItem::CompositionType );
    foreach( GraphicsConnector *i, lines ){
        i->updateConnector();
    }

    QString className = ((UMLClass*)endItem)->getClassName();
    QString classNameFirstLetter = className.at( 0 ).toLower();

    QString attribute = className;
    attribute += " ";
    if( 0 == classNameFirstLetter.compare( className.at( 0 ) )){
        className.prepend( "_" );
    }
    else{
        className.replace( 0, 1, classNameFirstLetter );
    }
    attribute += className + ";\n";

    if(create)
        ((UMLClass*)startItem)->addAttribute( attribute );
}

void UMLComposition::delConnector()
{
    UMLConnector::delConnector();
    UMLItemList::getInstance()->delConnection( this );
}
