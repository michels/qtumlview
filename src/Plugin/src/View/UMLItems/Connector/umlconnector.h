#ifndef UMLCONNECTOR_H
#define UMLCONNECTOR_H

#include <QGraphicsItem>

#include "View/UMLItems/Connector/graphicsconnector.h"

class GraphScene;
class UMLItem;
class UMLLineSelector;

class UMLConnector
{
public:
    UMLConnector( QGraphicsItem * parent = 0  );

    void updateConnector( UMLItem* umlItem, QPointF delta, bool isClass );
    void moveEdgeSelector( UMLItem* umlItem, QPointF delta, UMLLineSelector* edgeSelector, UMLLineSelector* firstActiveSelector, GraphicsConnector* line );
    void moveSelectorOrthogonal( UMLLineSelector* previous, UMLLineSelector* movedSelector, UMLLineSelector* next,
                                 GraphicsConnector* firstCon, GraphicsConnector* secondCon );
    void switchEdgeConnector();
    void createSelectorPreviousTo( int num );
    void createSelectorNextTo( int num );
    void delSelectorPreviousTo( int num );

    virtual void delConnector();
    void setScene( GraphScene* );

    void setStartPoint( UMLItem *item );
    void setStopPoint( UMLItem *item, bool create = true );

    UMLItem *getStartItem(){ return startItem; }
    UMLItem *getEndItem(){ return endItem; }

    void calculateDirectConnection();

private:
    QPointF calcIntersection( UMLItem* item, QLineF line );
    void updateLines();
    bool isVertical( QLineF );

protected:
    UMLItem *startItem;
    UMLItem *endItem;

    QPointF startPoint;
    QPointF endPoint;

    UMLItem::Type type;

    GraphScene* scene;
    QList<GraphicsConnector*> lines;
    QList<UMLLineSelector*> selectors;
    QList<UMLItem*> points;
};

#endif // UMLCONNECTOR_H
