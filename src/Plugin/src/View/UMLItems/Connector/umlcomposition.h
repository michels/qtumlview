#ifndef UMLCOMPOSITION_H
#define UMLCOMPOSITION_H

#include "View/UMLItems/Connector/umlconnector.h"

class UMLComposition: public UMLConnector
{
public:
    UMLComposition( QGraphicsItem * parent = 0 );

    void setStopPoint( UMLItem *item, bool create = true  );
    void delConnector();
};

#endif // UMLCOMPOSITION_H
