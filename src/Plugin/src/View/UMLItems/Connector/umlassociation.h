#ifndef UMLASSOCIATION_H
#define UMLASSOCIATION_H

#include "View/UMLItems/Connector/umlconnector.h"

class UMLAssociation : public UMLConnector
{
public:
    UMLAssociation( QGraphicsItem* parent = 0 );

    void setStopPoint( UMLItem *item, bool create = true  );
    void delConnector();
};

#endif // UMLASSOCIATION_H
