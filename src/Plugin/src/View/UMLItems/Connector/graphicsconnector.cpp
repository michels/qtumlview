#include "graphicsconnector.h"

#include "pch.h"

GraphicsConnector::GraphicsConnector( QGraphicsItem *parent ) :
    QGraphicsLineItem( parent )
{
    setZValue( 0 );
    mutex = false;
    lineOrentation = noOrientation;
    arrowHead = new QGraphicsPolygonItem( this );
    type = UMLItem::KeksType;
}

GraphicsConnector::GraphicsConnector( const QLineF &line, QGraphicsItem *parent ) :
    QGraphicsLineItem( line, parent )
{
    setZValue( 0 );
    mutex = false;
    lineOrentation = noOrientation;
    arrowHead = new QGraphicsPolygonItem( this );
    type = UMLItem::KeksType;
}

void GraphicsConnector::updateConnector()
{

    if( startItem->isVisible() )
        startPoint = mapFromItem(startItem, startItem->boundingRect().width()/2, startItem->boundingRect().height()/2 );
    else
        startPoint = mapFromItem(startItem, 0, 0 );

    if( endItem->isVisible() )
        endPoint = mapFromItem(endItem, endItem->boundingRect().width()/2, endItem->boundingRect().height()/2);
    else
        endPoint = mapFromItem(endItem, 0, 0 );


    QLineF line( startPoint, endPoint );
    setLine(line);

    if( type == UMLItem::KeksType )
        return;

    QPointF delta = startItem->pos();
    arrowHead->setPos( delta.x(), delta.y() );
    arrowHead->setRotation( -line.angle() );
}

void GraphicsConnector::setStartPoint( UMLItem *item )
{
    startItem = item;
}

QLineF GraphicsConnector::setTempEndPoint( QPointF point )
{
    return QLineF( startPoint, point );
}

QLineF GraphicsConnector::setStopPoint( UMLItem *item )
{
    endItem = item;

    return QLineF( startPoint, endPoint );
}

void GraphicsConnector::setType(UMLItem::Type type)
{
    this->type = type;
    if( type == UMLItem::GeneralizationType ){
        QPolygon triangle;
        triangle.putPoints( 0, 3, 0,0, 10,10, 10,-10 );
        arrowHead->setPolygon(triangle);
        arrowHead->setBrush( QBrush( Qt::white ));
    }
    if( type == UMLItem::AssociationType ){
        QPolygon triangle;
        triangle.putPoints( 0, 3, 0,0, 10,10, 10,-10 );
        arrowHead->setPolygon(triangle);
        QGraphicsLineItem* line1 = new QGraphicsLineItem( arrowHead );
        line1->setLine( 0,0, 10,10 );
        QGraphicsLineItem* line2 = new QGraphicsLineItem( arrowHead );
        line2->setLine( 0,0, 10,-10 );
        arrowHead->setBrush( QBrush( Qt::transparent ));
        arrowHead->setPen( QPen( Qt::transparent ));
    }
    if( type == UMLItem::CompositionType ){
        QPolygon rect;
        rect.putPoints( 0, 4, 0,0, 10,10, 20,0, 10,-10 );
        arrowHead->setPolygon(rect);
        arrowHead->setBrush( QBrush( Qt::black ));
    }
    if( type == UMLItem::AggregationType ){
        QPolygon rect;
        rect.putPoints( 0, 4, 0,0, 10,10, 20,0, 10,-10 );
        arrowHead->setPolygon(rect);
        arrowHead->setBrush( QBrush( Qt::white ));
    }
}

QPointF GraphicsConnector::calcIntersection()
{
    if (startItem->collidesWithItem(endItem)){
        arrowHead->setVisible( false );
        return QPoint(0,0);
    }

    arrowHead->setVisible( true );
    QLineF centerLine = this->line();
    QPolygonF endPolygon = startItem->polygon();
    QPointF intersectPoint, p1, p2;
    QLineF polyLine;

    // Create a line with two points of Polygon
    // and check intersection
    for (int i = 1; i < endPolygon.count(); ++i){
        p1 = endPolygon.at(i-1) + startItem->pos();
        p2 = endPolygon.at(i) + startItem->pos();
        polyLine = QLineF( p1, p2 );
        if( QLineF::BoundedIntersection ==
               polyLine.intersect(centerLine, &intersectPoint))
            return intersectPoint;
    }
    return QPoint(0,0);
}
