#include "umlconnector.h"

#include "View/graphscene.h"
#include "View/UMLItems/UMLClass/umlclass.h"
#include "View/UMLItems/umllineselector.h"

UMLConnector::UMLConnector(QGraphicsItem *parent)
{
}

void UMLConnector::updateConnector( UMLItem *umlItem, QPointF delta, bool isClass )
{
    selectors.at(0)->setActive( false );
    selectors.at( selectors.size()-1 )->setActive( false );

    if( isClass ){
        // move class
        if( startItem == umlItem ){
            moveEdgeSelector( umlItem, delta, selectors.first(), selectors.at(1), lines.at(1) );
        }else{
            moveEdgeSelector( umlItem, delta, selectors.last(), selectors.at(selectors.size()-2), lines.at(lines.size()-2) );
        }
    }else{
        // move selector
        int num = selectors.indexOf( (UMLLineSelector*)umlItem );

        if( num <= 0 )
            return;
        if( num >= selectors.size()-1 )
            return;

        moveSelectorOrthogonal(selectors.at(num-1), selectors.at(num), selectors.at(num+1), lines.at( num ), lines.at( num + 1 ));
        switchEdgeConnector();
    }

    updateLines();
    // for new selectors
    selectors.at( 1 )->setActive(true);
    selectors.at( selectors.size() - 1 )->setActive(true);
}

void UMLConnector::moveEdgeSelector( UMLItem *umlItem, QPointF delta, UMLLineSelector *edgeSelector, UMLLineSelector *firstActiveSelector, GraphicsConnector* line )
{
    // edge selector
    edgeSelector->setActive( false );
    edgeSelector->setPos( edgeSelector->pos().operator +=(delta) );

    if( line->getOrientation() == GraphicsConnector::horizontal ){
        // left or right of class?
        if( firstActiveSelector->x() > umlItem->x() + umlItem->boundingRect().width() ){
            edgeSelector->setX( umlItem->x() + umlItem->boundingRect().width() );
        }

        if( firstActiveSelector->x() < umlItem->x() ){
            edgeSelector->setX( umlItem->x() );
        }
        firstActiveSelector->setActive( false );
        firstActiveSelector->setY( edgeSelector->y() -3); // -3
        firstActiveSelector->setActive( true );
    }

    if( line->getOrientation() == GraphicsConnector::vertical ){
        // left or right of class?
        if( firstActiveSelector->y() > umlItem->y() + umlItem->boundingRect().height() ){
            edgeSelector->setY( umlItem->y() + umlItem->boundingRect().height() );
        }

        if( firstActiveSelector->y() < umlItem->y() ){
            edgeSelector->setY( umlItem->y() );
        }
        firstActiveSelector->setActive( false );
        firstActiveSelector->setX( edgeSelector->x() -3); // -3
        firstActiveSelector->setActive( true );
    }
}

void UMLConnector::moveSelectorOrthogonal(UMLLineSelector *previous, UMLLineSelector *movedSelector, UMLLineSelector *next,
                                          GraphicsConnector* firstCon, GraphicsConnector* secondCon )
{
    // positioning relative to the one moved
    previous->setActive(false);
    next->setActive(false);
    if( firstCon->getOrientation() == GraphicsConnector::horizontal ){
        previous->setY( movedSelector->y());
    }

    if( firstCon->getOrientation() == GraphicsConnector::vertical ){
        previous->setX( movedSelector->x() );
    }

    if( secondCon->getOrientation() == GraphicsConnector::horizontal ){
        next->setY( movedSelector->y());
    }

    if( secondCon->getOrientation() == GraphicsConnector::vertical ){
        next->setX( movedSelector->x());
    }
    previous->setActive(true);
    next->setActive(true);
}

void UMLConnector::switchEdgeConnector()
{
    int offset = 3;
    // switch head
    if( lines.at( 1 )->getOrientation() == GraphicsConnector::horizontal ){
        if( selectors.at( 1 )->x() < startItem->x() )
            selectors.at( 0 )->setX( startItem->x() );

        if( selectors.at( 1 )->x() > startItem->x() + startItem->boundingRect().width() )
            selectors.at( 0 )->setX( startItem->x() + startItem->boundingRect().width() );

        selectors.at( 0 )->setY( selectors.at(1)->y() + offset ); // + offset

        // out of bounds (create selector)
        if( selectors.at( 0 )->y() < startItem->y() || selectors.at( 0 )->y() > startItem->y() + startItem->boundingRect().height() ){
            createSelectorPreviousTo( 1 );
        }

        // in between ( delete selector)
        if( startItem->x() < selectors.at( 1 )->x() && startItem->x() + startItem->boundingRect().width() > selectors.at( 1 )->x() ){
            //selectors.at( num-1 )->setX( selectors.at(num)->x() + offset );
        }
    }
    if( lines.at( lines.size()-2 )->getOrientation() == GraphicsConnector::horizontal ){
        if( endItem->x() > selectors.at( selectors.size()-2 )->x() )
            selectors.at( selectors.size()-1 )->setX( endItem->x() );

        if( endItem->x() + endItem->boundingRect().width() < selectors.at( selectors.size()-2 )->x() )
            selectors.at( selectors.size()-1 )->setX( endItem->x()+endItem->boundingRect().width() );

        selectors.last()->setY( selectors.at( selectors.size()-2 )->y() + offset ); // + offset

        if( selectors.last()->y() < endItem->y() || selectors.last()->y() > endItem->y() + endItem->boundingRect().height() ){
            createSelectorNextTo( selectors.size()-2 );
        }

        if( endItem->x() < selectors.at( selectors.size()-2 )->x() && endItem->x() + endItem->boundingRect().width() > selectors.at( selectors.size()-2 )->x() ){
            //selectors.at( num+1 )->setX( selectors.at( num )->x() + offset );
        }
    }

    if( lines.at( 1 )->getOrientation() == GraphicsConnector::vertical ){
        if( selectors.at( 1 )->y() < startItem->y() ){
            selectors.at( 0 )->setY( startItem->y() );
        }

        if( selectors.at( 1 )->y() > startItem->y() + startItem->boundingRect().height() ){
            selectors.at( 0 )->setY( startItem->y() + startItem->boundingRect().height());
        }
        selectors.at( 0 )->setX( selectors.at(1)->x() + offset ); // + offset

    }
    if( lines.at( lines.size()-2 )->getOrientation() == GraphicsConnector::vertical ){
        if( selectors.at( selectors.size()-2 )->y() < endItem->y() ){
            selectors.at( selectors.size()-1 )->setY( endItem->y() );
        }

        if( selectors.at( selectors.size()-2 )->y() > endItem->y() + endItem->boundingRect().height() ){
            selectors.at( selectors.size()-1 )->setY( endItem->y() + endItem->boundingRect().height());
        }
        selectors.at( selectors.size()-1 )->setX( selectors.at( selectors.size()-2 )->x() + offset ); // + offset
    }
}

void UMLConnector::createSelectorPreviousTo( int num )
{
    /*
      +-+              ->  +-(+-)+
      (first line sec) -> (first line new new sec)
    */
    UMLLineSelector *firstSel = selectors.at( num - 1 );
    UMLLineSelector *secondSel = selectors.at( num );
    GraphicsConnector *line = lines.at( num );

    // new connector
    UMLLineSelector* newSelector = new UMLLineSelector();
    newSelector->registerConnector( this );

    GraphicsConnector* newConnection = new GraphicsConnector();
    if( line->getOrientation() == GraphicsConnector::horizontal){
        qDebug() << "horizontal";
        newConnection->setOrientation( GraphicsConnector::horizontal );
        line->setOrientation( GraphicsConnector::vertical );
    }else{
        newConnection->setOrientation( GraphicsConnector::vertical );
        line->setOrientation( GraphicsConnector::horizontal );
    }
    line->setStopPoint( newSelector );

    newConnection->setStartPoint( newSelector );
    newConnection->setStopPoint( secondSel );

    newSelector->setFirstConnector( line );
    newSelector->setSecondConnector( newConnection );
    newSelector->setSelectable( true );
    newSelector->setActive(false);
    newSelector->setX( firstSel->x() );
    newSelector->setY( secondSel->y() );
    newSelector->setActive(true);

    secondSel->setFirstConnector( newConnection );

    // update lists
    lines.insert( num + 1, newConnection );
    selectors.insert( num, newSelector );
    scene->addItem(newConnection);
    scene->addItem(newSelector);
}

void UMLConnector::createSelectorNextTo(int num)
{
    UMLLineSelector *firstSel = selectors.at( num );
    UMLLineSelector *secondSel = selectors.at( num + 1 );
    GraphicsConnector *line = lines.at( num + 1 );

    // new connector
    UMLLineSelector* newSelector = new UMLLineSelector();
    newSelector->registerConnector( this );

    GraphicsConnector* newConnection = new GraphicsConnector();
    if( line->getOrientation() == GraphicsConnector::horizontal){
        newConnection->setOrientation( GraphicsConnector::vertical );
        line->setOrientation( GraphicsConnector::horizontal );
    }else{
        newConnection->setOrientation( GraphicsConnector::horizontal );
        line->setOrientation( GraphicsConnector::vertical );
    }
    line->setStopPoint( newSelector );

    newConnection->setStartPoint( newSelector );
    newConnection->setStopPoint( secondSel );

    newSelector->setFirstConnector( line );
    newSelector->setSecondConnector( newConnection );
    newSelector->setSelectable( true );
    newSelector->setActive(false);
    newSelector->setX( secondSel->x() );
    newSelector->setY( firstSel->y() );
    newSelector->setActive(true);

    secondSel->setFirstConnector( newConnection );

    // update lists
    lines.insert( num+2, newConnection );
    selectors.insert( num+1, newSelector );
    scene->addItem(newConnection);
    scene->addItem(newSelector);
}

void UMLConnector::delSelectorPreviousTo(int num)
{
    if( num == 0 ){
        return;
    }

    UMLLineSelector *selector = selectors.at( num );
    UMLLineSelector *selToDel = selectors.takeAt( num - 1 );
    GraphicsConnector *lineToDel = lines.takeAt( num );
    selector->setFirstConnector( selToDel->getFirstConnector() );

    scene->removeItem( selToDel );
    delete selToDel;
    scene->removeItem( lineToDel );
    delete lineToDel;
}

void UMLConnector::delConnector()
{
    startItem->delConnector( this );
    endItem->delConnector( this );

    foreach( GraphicsConnector *i, lines ){
        scene->removeItem( i );
        delete i;
    }

    foreach( UMLLineSelector *i, selectors ){
        scene->removeItem( i );
        delete i;
    }
}

void UMLConnector::setScene(GraphScene *graphScene)
{
    scene = graphScene;
}

void UMLConnector::setStartPoint( UMLItem *item )
{
    startItem = item;
}

void UMLConnector::setStopPoint( UMLItem *item, bool create )
{
    endItem = item;
    lines.clear();
    selectors.clear();

    // first item in list = startItem
    UMLLineSelector *startEdgeItem = new UMLLineSelector();
    //setFlag( QGraphicsItem::ItemSendsGeometryChanges );
    startEdgeItem->setVisible( false );
    startEdgeItem->setActive( false );
    //startEdgeItem->shrink();
    startEdgeItem->setFlag( QGraphicsItem::ItemIsSelectable, false );
    selectors.append( startEdgeItem );

    // intermediate points follow
    UMLLineSelector *midItem = new UMLLineSelector();
    midItem->setActive( false );
    selectors.append( midItem );

    UMLLineSelector *midItem2 = new UMLLineSelector();
    midItem2->setActive( false );
    selectors.append( midItem2 );

    // second item in list = endItem
    UMLLineSelector *endEdgeItem = new UMLLineSelector();
    endEdgeItem->setVisible( false );
    endEdgeItem->setActive( false );
    //endEdgeItem->shrink();
    endEdgeItem->setFlag( QGraphicsItem::ItemIsSelectable, false );
    selectors.append( endEdgeItem );

    // doesn't matter
    GraphicsConnector* one = new GraphicsConnector();
    one->setStartPoint( startItem);
    one->setStopPoint( startEdgeItem );
    lines.append( one );

    GraphicsConnector* two = new GraphicsConnector();
    two->setStartPoint( startEdgeItem );
    two->setStopPoint( midItem );
    lines.append( two );

    startEdgeItem->setFirstConnector( one );
    startEdgeItem->setSecondConnector( two );

    GraphicsConnector* three = new GraphicsConnector();
    three->setStartPoint( midItem );
    three->setStopPoint( midItem2 );
    lines.append( three );

    midItem->setFirstConnector( two );
    midItem->setSecondConnector( three );

    GraphicsConnector* four = new GraphicsConnector();
    four->setStartPoint( midItem2 );
    four->setStopPoint( endEdgeItem );
    lines.append( four );

    midItem2->setFirstConnector( three );
    midItem2->setSecondConnector( four );

    GraphicsConnector* five = new GraphicsConnector();
    five->setStartPoint( endEdgeItem );
    five->setStopPoint( endItem );
    lines.append( five );

    endEdgeItem->setFirstConnector( four );
    endEdgeItem->setSecondConnector( five );

    scene->addItem( one );
    scene->addItem( two );
    scene->addItem( three );
    scene->addItem( four );
    scene->addItem( five );
    scene->addItem( startEdgeItem );
    scene->addItem( midItem );
    scene->addItem( midItem2 );
    scene->addItem( endEdgeItem );

    startItem->registerConnector( this );
    startEdgeItem->registerConnector( this );
    midItem->registerConnector( this );
    midItem2->registerConnector( this );
    endEdgeItem->registerConnector( this );
    endItem->registerConnector( this );

    calculateDirectConnection();
}

// only call on creation
void UMLConnector::calculateDirectConnection()
{
    foreach(UMLLineSelector* i, selectors)
        i->setActive( false );

    while( selectors.size() > 4 ){
        delSelectorPreviousTo(selectors.size()-1);
    }

    // Calc Point in between
    startPoint = startItem->getCenter();
    endPoint = endItem->getCenter();

    qreal minX = qMin( startPoint.x(), endPoint.x() );
    qreal maxX = qMax( startPoint.x(), endPoint.x() );
    qreal minY = qMin( startPoint.y(), endPoint.y() );
    qreal maxY = qMax( startPoint.y(), endPoint.y() );

    QLineF directConnection( startPoint, endPoint );
    QPointF midPoint;
    midPoint.setX( minX + (maxX - minX)*0.5 );
    midPoint.setY( minY + (maxY - minY)*0.5 );

    selectors.at( 0 )->setPos( calcIntersection( startItem, directConnection ));
    selectors.at( 3 )->setPos( calcIntersection( endItem, directConnection ));

    // TODO:
    startPoint = selectors.at( 0 )->pos();
    endPoint = selectors.at( 3 )->pos();

    minX = qMin( startPoint.x(), endPoint.x() );
    maxX = qMax( startPoint.x(), endPoint.x() );
    minY = qMin( startPoint.y(), endPoint.y() );
    maxY = qMax( startPoint.y(), endPoint.y() );

    directConnection = QLineF( startPoint, endPoint );
    midPoint.setX( -3 + minX + (maxX - minX)*0.5 );
    midPoint.setY( -3 + minY + (maxY - minY)*0.5 );
//    selectors.at( selectors.size() - 1 )->setPos( midPoint );
//    selectors.at( selectors.size() - 2 )->setPos( midPoint );

    if( isVertical( directConnection )){
        selectors.at( selectors.size() - 2 )->setPos( endPoint.x()-3, midPoint.y() );
        selectors.at( selectors.size() - 3 )->setPos( startPoint.x()-3, midPoint.y() );
        lines.at( 1 )->setOrientation( GraphicsConnector::vertical );
        lines.at( 2 )->setOrientation( GraphicsConnector::horizontal );
        lines.at( 3 )->setOrientation( GraphicsConnector::vertical );
    }else{
        selectors.at( selectors.size() - 2 )->setPos( midPoint.x(), endPoint.y()-3 );
        selectors.at( selectors.size() - 3 )->setPos( midPoint.x(), startPoint.y()-3 );
        lines.at( 1 )->setOrientation( GraphicsConnector::horizontal );
        lines.at( 2 )->setOrientation( GraphicsConnector::vertical );
        lines.at( 3 )->setOrientation( GraphicsConnector::horizontal );
    }

    foreach(UMLLineSelector* i, selectors)
        i->setActive( true );
    updateLines();
}

QPointF UMLConnector::calcIntersection( UMLItem* item, QLineF line )
{
    QPolygonF polygon = item->polygon();
    QPointF intersectPoint, p1, p2;
    QLineF polyLine;

    // Create a line with two points of Polygon
    // and check intersection
    for( int i = 1; i < polygon.count(); ++i ){
        p1 = polygon.at( i-1 ) + item->pos();
        p2 = polygon.at( i ) + item->pos();
        polyLine = QLineF( p1, p2 );
        if( QLineF::BoundedIntersection ==
               polyLine.intersect(line, &intersectPoint))
            return intersectPoint;
    }
    return item->getCenter(); // some Point
}

void UMLConnector::updateLines()
{
    foreach( GraphicsConnector* i, lines ){
        i->updateConnector();
    }
}

bool UMLConnector::isVertical( QLineF line )
{
    qreal angle = line.angle();

    if( angle < 45 || angle > 315 )
        return false;

    if( angle < 135 || angle > 225 )
        return true;

    return false;
}
