#include "umlaggregation.h"
#include "View/UMLItems/UMLClass/umlclass.h"
#include "View/UMLItems/umlitemlist.h"

UMLAggregation::UMLAggregation( QGraphicsItem* parent ) :
    UMLConnector( parent )
{
    type = UMLItem::AggregationType;
}

void UMLAggregation::setStopPoint( UMLItem *item, bool create )
{
    UMLConnector::setStopPoint( item );

    lines.at( 1 )->setType( UMLItem::AggregationType );
    foreach( GraphicsConnector *i, lines ){
        i->updateConnector();
    }

    QString className = ((UMLClass*)endItem)->getClassName();
    QString classNameFirstLetter = className.at( 0 ).toLower();

    QString attribute = className;
    attribute += " ";
    if( 0 == classNameFirstLetter.compare( className.at( 0 ) )){
        className.prepend( "_" );
    }
    else{
        className.replace( 0, 1, classNameFirstLetter );
    }
    className.prepend("*");
    attribute += className + ";\n";

    if(create)
        ((UMLClass*)startItem)->addAttribute( attribute );
}

void UMLAggregation::delConnector()
{
    UMLConnector::delConnector();
    UMLItemList::getInstance()->delConnection( this );
}
