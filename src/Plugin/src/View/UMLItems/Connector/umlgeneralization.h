#ifndef GRAPHICSGENERALIZATIONITEM_H
#define GRAPHICSGENERALIZATIONITEM_H

#include "View/UMLItems/Connector/umlconnector.h"

class UMLGeneralization : public UMLConnector
{
public:
    UMLGeneralization( QGraphicsItem * parent = 0 );

    void setStopPoint(UMLItem *item, bool create = true  );
    void delConnector();

};

#endif // GRAPHICSGENERALIZATIONITEM_H
