#ifndef GRAPHICSCONNECTOR_H
#define GRAPHICSCONNECTOR_H

#include <QGraphicsItem>
#include <QGraphicsLineItem>
#include "View/UMLItems/umlitem.h"

class GraphicsConnector : public QGraphicsLineItem
{
public:
    GraphicsConnector( QGraphicsItem * parent = 0 );
    GraphicsConnector( const QLineF & line, QGraphicsItem * parent = 0 );

    enum LineOrientation{
        noOrientation = 0,
        horizontal = 1,
        vertical = 2
    };

    void updateConnector();

    void setStartPoint( UMLItem *item );
    QLineF setTempEndPoint( QPointF point );
    QLineF setStopPoint( UMLItem *item );

    UMLItem *getStartItem(){ return startItem; }
    UMLItem *getStopItem(){ return endItem; }

    QPointF calcIntersection();
    void setType( UMLItem::Type type);

    void setOrientation( LineOrientation in ){ lineOrentation = in; }
    LineOrientation getOrientation(){ return lineOrentation; }
    /*
    void setStartItem( QGraphicsItem *item ){ startItem = item; }
    void setStopItem( QGraphicsItem *item ){ stopItem = item; }
    */

private:
    bool mutex;
    UMLItem *startItem;
    UMLItem *endItem;

    QPointF startPoint;
    QPointF endPoint;

    QGraphicsPolygonItem *arrowHead;
    UMLItem::Type type;
    LineOrientation lineOrentation;


protected:
    //void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget );

};

#endif // GRAPHICSCONNECTOR_H
