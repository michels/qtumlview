#include "umlitemcreator.h"

#include "View/graphscene.h"
#include "View/UMLItems/Connector/umlgeneralization.h"
#include "View/UMLItems/Connector/umlcomposition.h"
#include "View/UMLItems/Connector/umlaggregation.h"
#include "View/UMLItems/Connector/umlassociation.h"
#include "View/UMLItems/umlitemlist.h"

using namespace UMLView::Model;

UMLItemCreator* UMLItemCreator::theInstance = NULL;

UMLItemCreator::UMLItemCreator( QObject *parent ) :
    QObject( parent )
{
    createConnection = false;
    mousePos = QPointF( 0, 0 );
    currentItem = NULL;
    counter = 0;
    umlItemList = UMLItemList::getInstance();
}

UMLItemCreator *UMLItemCreator::getInstance()
{
    if( theInstance == NULL )
        theInstance = new UMLItemCreator();
    return theInstance;
}

void UMLItemCreator::release()
{
    if( theInstance != NULL )
        delete theInstance;
    theInstance = NULL;
}

/*
void UMLItemCreator::createClass( QPointF p )
{
    UMLClass* item = new UMLClass( graphScene );
    item->renameClass( "Class" );
    item->moveBy( p.x(), p.y() );
    //nodes.append(item);

    graphScene->addItem( item );
}
*/

void UMLItemCreator::createClass( UMLView::Model::ModelClass *mClass)
{
    if(counter > 100)    // limit to 50 classes
        return;
    UMLClass* item = new UMLClass( mClass, graphScene );
    graphScene->addItem( item );
    umlItemList->addClass( item );
    item->setPos( graphScene->getLastContextClickPoint() );
    counter++;
    if( item->pos() == QPointF( 0, 0 )){

        QList<UMLClass* >colItems  = item->collidingClasses();
        qint32 i = 1, j = 0;

        while( !colItems.isEmpty() ){
            item->setPos( 80*i, 200*j );
            colItems  = item->collidingClasses();

            // ten columns -> next row
            i++;
            if( i > 20 ){
                j++;
                i = 0;
            }
        }
    }

    graphScene->clearFocus();
}

void UMLItemCreator::createGeneralization( QGraphicsItem *item )
{
    createConnection = true;
    itemType = UMLItem::GeneralizationType;
    startItem = item;

}

void UMLItemCreator::createComposition( QGraphicsItem *item )
{
    createConnection = true;
    itemType = UMLItem::CompositionType;
    startItem = item;
}

void UMLItemCreator::createAssociation( QGraphicsItem *item )
{
    createConnection = true;
    itemType = UMLItem::AssociationType;
    startItem = item;
}

void UMLItemCreator::createAggregation( QGraphicsItem *item )
{
    createConnection = true;
    itemType = UMLItem::AggregationType;
    startItem = item;
}

void UMLItemCreator::setEndItem( QGraphicsItem *item, bool create )
{
    currentItem = item;

    if( currentItem == NULL)
        createConnection = false;

    if( !createConnection )
        return;

    createConnection = false;

    switch( itemType ){
    case UMLItem::GeneralizationType :{
        UMLGeneralization* connector = new UMLGeneralization();
        umlItemList->addGeneralization( connector );
        connector->setScene( graphScene );
        // arrow should be start of connector, so swich of start, end for this one
        connector->setStartPoint( (UMLClass*)item );
        connector->setStopPoint( (UMLClass*)startItem, create );
        break;
    }
    case UMLItem::AssociationType :{
        UMLAssociation* connector = new UMLAssociation();
        umlItemList->addConnection( connector );
        connector->setScene( graphScene );
        connector->setStartPoint( (UMLClass*)startItem );
        connector->setStopPoint( (UMLClass*)item, create );
        break;
    }
    case UMLItem::CompositionType :{
        UMLComposition* connector = new UMLComposition();
        umlItemList->addConnection( connector );
        connector->setScene( graphScene );
        connector->setStartPoint( (UMLClass*)startItem );
        connector->setStopPoint( (UMLClass*)item, create );
        break;
    }
    case UMLItem::AggregationType :{
        UMLAggregation* connector = new UMLAggregation();
        umlItemList->addConnection( connector );
        connector->setScene( graphScene );
        connector->setStartPoint( (UMLClass*)startItem );
        connector->setStopPoint( (UMLClass*)item, create );
        break;
    }
    default :{
    }
    }

}

void UMLItemCreator::setScene( GraphScene *scene )
{
    graphScene = scene;
}

QGraphicsItem *UMLItemCreator::getUMLParentItem(QGraphicsItem *item )
{
    // search for a parent with User specified type
    while( item != NULL ){
        if( item->type() > QGraphicsItem::UserType ){
            break;
        }
        item = item->parentItem();
    }
    return item;
}

void UMLItemCreator::setMousePos( QPointF p )
{
    mousePos = p;
}
