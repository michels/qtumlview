#ifndef SVGRENDERER_H
#define SVGRENDERER_H

#include <QSvgRenderer>
#include <QGraphicsSvgItem>

class SvgRenderer
{
public:
    static SvgRenderer* getInstance();
    static void release();

    void setClassRenderer( QGraphicsSvgItem* item);
    void setDeleteButtonRenderer( QGraphicsSvgItem *item );
    void setAddButtonRenderer( QGraphicsSvgItem *item );

private:
    SvgRenderer();
    static SvgRenderer* theInstance;

    QSvgRenderer classRenderer;
    QSvgRenderer deleteButtonRenderer;
    QSvgRenderer addButtonRenderer;


};

#endif // SVGRENDERER_H
