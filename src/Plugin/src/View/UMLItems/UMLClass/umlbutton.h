#ifndef UMLBUTTON_H
#define UMLBUTTON_H

#include <QGraphicsSvgItem>

class UMLClassEntry;

class UMLButton : public QGraphicsSvgItem
{
public:
    UMLButton( QGraphicsItem* parentItem = 0 );

    void isAddButton(){ addButton = true; }
    void isMethodButton(){ isMethod = true; }
    void isAttributeButton(){ isMethod = false; }
    void isDelButton(){ addButton = false; }
    void setTextItem( UMLClassEntry* item){ textItem = item; }

private:
    UMLClassEntry* textItem;
    bool addButton;
    bool isMethod;

protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
};

#endif // UMLBUTTON_H
