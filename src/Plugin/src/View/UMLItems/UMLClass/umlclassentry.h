#ifndef UMLCLASSTEXT_H
#define UMLCLASSTEXT_H

#include <QGraphicsTextItem>
#include <QTextDocument>
#include "View/UMLItems/UMLClass/umlbutton.h"
#include "model/modelclasses.h"


class UMLClassEntry : public QGraphicsTextItem
{
    Q_OBJECT
public:
    enum Type {
        ClassType,
        AttributeType,
        MethodType
    };
    typedef QSharedPointer<UMLClassEntry> Ptr;

    UMLClassEntry(const QString &text, Type type, QGraphicsItem *parent = 0);
    static UMLClassEntry::Ptr create(UMLView::Model::ModelClassMethod::Ptr, QGraphicsItem *parent);
    static UMLClassEntry::Ptr create(UMLView::Model::ModelClassAttribute::Ptr, QGraphicsItem *parent);

    void setButtonVisible( bool );
    void setTextItemHight( qint32 hight );
    void setButtonPos();
    void setFontSize( qint32 size, bool bold = false );
    void deleteItem();
    void setVisible(bool visible);
    void setPixmap(QGraphicsPixmapItem* pixmapItem);

    QVariant modelData()const{return modelPtr;}
    bool isMethod()const{ return type==MethodType; }
    bool isAttribute()const{ return type==AttributeType; }
    bool isClass()const{ return type==ClassType; }
    bool isDirty()const{ return dirty; }

    QString getCurrentText();
    QString getOldText();
private:
    UMLClassEntry( QGraphicsItem *parent = 0 );
    UMLButton button;
    qreal textItemHight;
    QGraphicsPixmapItem* pixmap;

    bool dirty;
    Type type;
    QString oldText;
    QVariant modelPtr;

    void init();
    void validateAttribute( QString );
    void validateMethod( QString );

protected:
    QVariant itemChange( GraphicsItemChange change, const QVariant &value );

public slots:
    void textChange();

};

#endif // UMLCLASSTEXT_H
