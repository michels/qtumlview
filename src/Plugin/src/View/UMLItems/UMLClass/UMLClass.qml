import QtQuick 1.1

Rectangle {
    property string className
    MouseArea {
        id: mouse
        anchors.fill: parent
        drag.target: parent.parent
        drag.minimumX: 0
        drag.maximumX: parent.parent.width - parent.width
        onActiveFocusChanged: if(activeFocus){ border.color= "#FF0000" }else{border.color="#000000"}
        //onClicked: {signalReceiver.classClicked(className,parent.parent.pos);parent.border.color= "#FF0000"}
        //onPositionChanged: signalReceiver.classMoved(className,parent.parent.pos)
    }
    width: 100
    height: attributes.pos.y+attributes.height
    radius: 5
    border.color: "#000000"
    border.width: 2

    TextEdit {
        id: text_ClassName
        x: -32
        y: -123
        width: 80
        height: 20
        text: className
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 5
        font.pixelSize: 12
    }

    Rectangle {
        id: separator_Member
        x: -42
        y: -98
        width: 100
        height: 5
        color: "#ff0b0b"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: text_ClassName.bottom
        anchors.topMargin: 5
    }

    ListView {
        id: member
        x: 10
        y: -70
        width: 80
        height: count*20
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: separator_Member.bottom
        anchors.topMargin: 0

        model: ListModel {
            ListElement {
                name: "+public"
            }
            ListElement {
                name: "-private"
            }
            ListElement {
                name: "oProtected"
            }
        }
        delegate: Text {
            text: name
        }
    }

    Rectangle {
        id: seperatot_Attributes
        x: -42
        y: -70
        width: 100
        height: 5
        color: "#ff0b0b"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: member.bottom
        anchors.topMargin: 0
    }

    ListView {
        id: attributes
        x: 10
        y: 93
        width: 80
        height: count*17
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: seperatot_Attributes.bottom
        anchors.topMargin: 0

        model: ListModel {
            ListElement {
                name: "+name"
            }
            ListElement {
                name: "-color"
            }
            ListElement {
                name: "oTEST"
            }
        }
        delegate: Text {
            text: name
        }
    }
}
