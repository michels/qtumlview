#ifndef UMLCLASS_H
#define UMLCLASS_H

#include "pch.h"

#include "View/UMLItems/umlitem.h"
#include "View/UMLItems/UMLClass/umlbutton.h"
#include "View/UMLItems/UMLClass/umlclassentry.h"
#include "model/modelclass.h"

class ClassLayout;
class UMLConnector;
using namespace UMLView::Model;

class UMLClass : public UMLItem
{
    friend class UMLClassEntry;
public:
    friend class ClassLayout;
    UMLClass( UMLView::Model::ModelClass* model, QGraphicsScene* parentScene, QGraphicsItem *parent = 0 );

    int type() const { return ClassType; }

    enum Content{
        Name = 1,
        Attribute = 2,
        Method = 3
    };

    void setClassName(QString in);
    QString getClassName();
    UMLView::Model::ModelClass *modelClass(){return _modelClass;}

    void updateLayout();
    void updateContent();
    void updateMethods();
    void updateAttributes();
    void updateUMLConnector(QPointF delta);

    QList< UMLClass* > collidingClasses();

    void addMethod( QString name );
    QStringList validateMethod( QString name );

    void addAttribute( QString name );
    QStringList validateAttribute( QString name );
    QStringList AttributeTypes();
    QString extractType(QString);

    void renameClass( QString name );
    void removeClass();
private:
    void removeAttribute(const UMLClassEntry *textElement );
    void removeMethod(const UMLClassEntry *textElement );
    void setButtonsVisible(bool visible);

    ClassLayout *classLayout;
    QGraphicsScene *scene;
    UMLView::Model::ModelClass *_modelClass;

    UMLClassEntry *classText;

    QList<UMLClassEntry::Ptr> methods;
    QList<UMLClassEntry::Ptr> attributes;

    UMLButton addMethodsButton;
    UMLButton addAttributeButton;

    QSvgRenderer renderer;
    QGraphicsSvgItem classSVGBackground;
    QGraphicsRectItem classBackground;
    QGraphicsRectItem *classNameRect;
    QGraphicsRectItem memberSpacer;
    QGraphicsRectItem *memberRect;
    QGraphicsRectItem attributesSpacer;
    QGraphicsRectItem *attributesRect;

    struct ItemLayout {
        qint32 width;
        qint32 methodsStart;
        qint32 memberHeight;
        qint32 attributesStart;
        qint32 attributesHeight;
        qint32 height;
    };
    ItemLayout itemLayout;

    void init();

    void activateItems(bool);
    void updateBackend();

protected:
    virtual void hoverEnterEvent ( QGraphicsSceneHoverEvent * event );
    virtual void hoverLeaveEvent ( QGraphicsSceneHoverEvent * event );
    QVariant itemChange( GraphicsItemChange change, const QVariant &value );

};

#endif // UMLCLASS_H
