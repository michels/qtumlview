#include "umlclass.h"

#include <QPen>
#include <QBrush>
#include <QString>
#include <QDir>
#include <QTextDocument>
#include "View/UMLItems/UMLClass/umlclassentry.h"
#include "View/UMLItems/UMLClass/svgrenderer.h"
#include "View/UMLItems/UMLClass/classlayout.h"
#include "View/UMLItems/Connector/umlconnector.h"
#include "View/UMLItems/umlitemlist.h"

UMLClass::UMLClass(UMLView::Model::ModelClass *model, QGraphicsScene *parentScene, QGraphicsItem *parent) :
    UMLItem( parent ),
    _modelClass(model)
{
    classLayout = ClassLayout::getInstance();
    scene = parentScene;
    _modelClass->setUiElement(this);

    init();
    updateContent();
}

void UMLClass::init()
{
    classSVGBackground.setParentItem( this );
    SvgRenderer::getInstance()->setClassRenderer( &classSVGBackground );

    /* new Layout
    classBackground.setParentItem( this );
    classBackground.setRect(0,0,100,100);
    classBackground.setBrush( QBrush(QColor(68,170,0,255)));

    QLinearGradient grad;
    grad.setStart(0,30);
    grad.setFinalStop(0,0);
    grad.setColorAt(0,QColor(255,255,255,200));
    grad.setColorAt(1,QColor(255,255,255,0));

    QGraphicsRectItem* rec2 = new QGraphicsRectItem( &classBackground );
    rec2->setRect(0,0,96,30);
    rec2->setBrush( grad );
    rec2->setPen(QPen(Qt::transparent));
    rec2->setPos(2,68);

    QLinearGradient grad2;
    grad2.setStart(70,0);
    grad2.setFinalStop(100,40);
    grad2.setColorAt(0,QColor(255,255,255,200));
    grad2.setColorAt(1,QColor(255,255,255,0));

    // bezier
    QGraphicsEllipseItem* ell = new QGraphicsEllipseItem(&classBackground);
    ell->setRect(0,0,130,40);
    ell->setPen(QPen(Qt::transparent));
    ell->setBrush(grad2);
    ell->setPos(-35,-10);
    */

    classNameRect = new QGraphicsRectItem( this );
    memberSpacer.setParentItem( this  );
    memberRect = new QGraphicsRectItem( this );
    attributesSpacer.setParentItem( this );
    attributesRect = new QGraphicsRectItem( this );

    addMethodsButton.setParentItem( memberRect );
    addMethodsButton.isAddButton();
    addMethodsButton.isMethodButton();
    addMethodsButton.setFlag( QGraphicsItem::ItemIsSelectable );
    SvgRenderer::getInstance()->setAddButtonRenderer( &addMethodsButton );

    addAttributeButton.setParentItem( attributesRect );
    addAttributeButton.isAddButton();
    addAttributeButton.isAttributeButton();
    addAttributeButton.setFlag( QGraphicsItem::ItemIsSelectable );
    SvgRenderer::getInstance()->setAddButtonRenderer( &addAttributeButton );

    classText = new UMLClassEntry( "className", UMLClassEntry::ClassType, classNameRect );

    setAcceptHoverEvents(true);
    setButtonsVisible(false);
}

void UMLClass::setClassName(QString in)
{
    classText->setPlainText(in);
}

QString UMLClass::getClassName()
{
    return classText->getCurrentText();
}

void UMLClass::updateMethods()
{
    methods.clear();

    foreach(ModelClassMethod::Ptr method, _modelClass->methods() ){
        methods.append( UMLClassEntry::create(method, memberRect ) );
    }
    updateLayout();
}

QStringList UMLClass::validateMethod( QString name )
{
    QRegExp generalReg("[^\\(\\)]+");
    QRegExp reg("\\w+\\s*([<][<\\w\\*\\s]*[>])?\\s*");

    QStringList list;
    qint32 pos = 0;
    qint32 i = 0;
    QString arg = "";

    // extract arguments
    while(( pos = generalReg.indexIn( name, pos )) != -1 ) {
        list << generalReg.cap( 0 );
        pos += generalReg.matchedLength();
        i++;
    }
    if( i > 2 || i == 0 ){
        qDebug() << "method can't be parsed: " << name;
        list.clear();
        return list;
    }
    if( i == 1 ){
        arg = "";
        //qDebug() << "Method has no arguments: " << name;
    }else{
        arg = list.at( 1 );
    }

    name = list.at( 0 );
    list.clear();
    pos = 0;
    i = 0;

    // parse type and name
    while(( pos = reg.indexIn( name, pos )) != -1 ) {
        list << reg.cap( 0 );
        pos += reg.matchedLength();
        i++;
    }
    list << arg;

    if( i == 2 ){
        //qDebug() << "Accept: " << name << "args: " << arg;
    }else{
        qDebug() << "Method not parsable: " << name;
        list.clear();
    }

    return list;
}

// for frontend
void UMLClass::addMethod( QString name )
{
    UMLClassEntry::Ptr entry = UMLClassEntry::Ptr(new UMLClassEntry(name, UMLClassEntry::MethodType, memberRect ));
    entry->setButtonVisible(true);
    //methods.append(entry);
    //updateLayout();

    QStringList list = validateMethod( name );

    if(_modelClass == NULL)
        return;

    if( list.size() == 3 )
        _modelClass->addMethod(name);

}

void UMLClass::removeMethod(const UMLClassEntry* textElement )
{
    if (textElement->modelData().isValid())
        _modelClass->removeMethod( textElement->modelData().value<ModelClassMethod::Ptr>() );
}

void UMLClass::setButtonsVisible(bool visible)
{
    classText->setButtonVisible(visible);
    addMethodsButton.setVisible(visible);
    addAttributeButton.setVisible(visible);
    foreach(UMLClassEntry::Ptr entry, attributes + methods)
        entry->setButtonVisible(visible);
}

void UMLClass::updateAttributes()
{
    attributes.clear();

    if(_modelClass == NULL)
        return;

    foreach( ModelClassAttribute::Ptr attr,  _modelClass->attributes()){
        attributes.append( UMLClassEntry::create(attr, attributesRect ));
    }

    UMLItemList::getInstance()->updateConnector( this );
    updateLayout();
}

QStringList UMLClass::validateAttribute(QString name)
{
    QRegExp reg("\\w+\\s*([<][<>\\w\\*\\s]*[>])?\\s*");
    QStringList list;
    qint32 pos = 0;
    qint32 i = 0;

    while((pos = reg.indexIn( name, pos )) != -1) {
        list << reg.cap( 0 );
        pos += reg.matchedLength();
        i++;
    }

    if( i == 2 ){
        return list;
    }else{
        qDebug() << "Attrubute not parsable: " + name;
        list.clear();
        return list;
    }
}

QStringList UMLClass::AttributeTypes()
{
    QStringList list;
    foreach( UMLClassEntry::Ptr i,attributes ){
        list << extractType( i->getCurrentText() );
    }

    return list;
}

QString UMLClass::extractType( QString name )
{
    QRegExp reg("\\w+\\s*\\*?");
    QString type = "noType";

    if((reg.indexIn( name )) != -1) {
        type = reg.cap( 0 );
    }
    return type;
}

// called from frontend
void UMLClass::addAttribute( QString name )
{
    //UMLClassEntry::Ptr entry = UMLClassEntry::Ptr(new UMLClassEntry(name, UMLClassEntry::AttributeType, attributesRect ));
    //entry->setButtonVisible(true);
    //attributes.append(entry);
    //updateLayout();

    QStringList list = validateAttribute( name );

    if( _modelClass == NULL )
        return;

    if( list.size() == 2)
        _modelClass->addAttribute(name);
}

void UMLClass::removeAttribute(const UMLClassEntry *textElement)
{
    if (textElement->modelData().isValid())
        _modelClass->removeAttribute(textElement->modelData().value<ModelClassAttribute::Ptr>());
}

// frontend function (backend calls updateContent())
void UMLClass::renameClass( QString name )
{
    classText->setPlainText( name );

    if(_modelClass == NULL)
        return;
    _modelClass->setClassName( name );
}

void UMLClass::removeClass()
{
    if( this == NULL ){
        qDebug() << "already removed?";
        return;
    }

    foreach( UMLConnector *i, connectedLines )
        i->delConnector();
    scene->removeItem( this );
    scene->update( this->boundingRect() );
    UMLItemList::getInstance()->delClass( this );
    delete this;
}

void UMLClass::updateLayout()
{
    classLayout->update( this );
}

void UMLClass::updateContent()
{
    if( _modelClass == NULL )
        return;
    renameClass( _modelClass->className() );
    updateAttributes();
    updateMethods();
    updateLayout();
}

void UMLClass::updateUMLConnector( QPointF delta )
{
    foreach( UMLConnector *i, connectedLines ){
        if( i == NULL )
            continue;
        i->updateConnector( this, delta, true );
    }
}

QList<UMLClass *> UMLClass::collidingClasses()
{
    QList<UMLClass *> collidingClasses;
    QList<UMLClass *> classes = UMLItemList::getInstance()->getClasses();
    foreach( UMLClass* i, classes){
        if( this == i )
            continue;

        if( collidesWithItem( i ))
            collidingClasses.append( i );
    }
    return collidingClasses;
}

void UMLClass::activateItems(bool activate)
{
    foreach( UMLClassEntry::Ptr elem, attributes + methods){
        elem->setActive( activate );
    }

    if( activate ){
        setZValue( 6 );
    }else{
        setZValue( 5 );
    }
}

void UMLClass::updateBackend()
{
    if(classText->isDirty()){
        classText->getCurrentText();
        classText->getOldText();
    }

    foreach( UMLClassEntry::Ptr elem, attributes + methods){
        if( elem->isDirty() ){
            elem->getOldText();
            if (elem->isAttribute())
                validateAttribute( elem->getCurrentText() );
            else
                validateMethod( elem->getCurrentText() );
        }
    }

}

void UMLClass::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    setButtonsVisible(true);
}

void UMLClass::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    setButtonsVisible(false);
}

QVariant UMLClass::itemChange( QGraphicsItem::GraphicsItemChange change, const QVariant &value )
{

    if( change == QGraphicsItem::ItemSelectedChange ){
        if(value.toBool()){
            activateItems( true );
        }else{
            activateItems( false );
            updateBackend();
        }
    }
    else if( change == QGraphicsItem::ItemPositionChange ){
        QPointF org = pos();
        updateUMLConnector( value.toPointF().operator -=(org) );
    }

    return UMLItem::itemChange( change, value );
}
