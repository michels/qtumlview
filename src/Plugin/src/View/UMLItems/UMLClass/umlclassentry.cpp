#include "umlclassentry.h"

#include <QRegExp>
#include <QRegExpValidator>
#include "View/UMLItems/UMLClass/umlclass.h"
#include "View/UMLItems/UMLClass/svgrenderer.h"

UMLClassEntry::Ptr UMLClassEntry::create(ModelClassAttribute::Ptr attrib, QGraphicsItem *parent)
{
    UMLClassEntry::Ptr attr = UMLClassEntry::Ptr(new UMLClassEntry(attrib->toString(), AttributeType, parent));
    attr->modelPtr = qVariantFromValue(attrib);
    attr->setPixmap(new QGraphicsPixmapItem(attrib->icon().pixmap(32,32)));

    return attr;
}

UMLClassEntry::Ptr UMLClassEntry::create(ModelClassMethod::Ptr method, QGraphicsItem *parent)
{
    UMLClassEntry::Ptr met = UMLClassEntry::Ptr(new UMLClassEntry(method->toString(), MethodType,parent));
    met->modelPtr = qVariantFromValue(method);
    met->setPixmap(new QGraphicsPixmapItem( method->icon().pixmap(32,32) ));

    return met;
}

UMLClassEntry::UMLClassEntry(const QString &text, Type t, QGraphicsItem *parent) :
    QGraphicsTextItem( text, parent ),
    type(t)
{
    document()->setPlainText( text );
    oldText = text;
    dirty = false;
    init();
    moveBy(10,0);
}

void UMLClassEntry::setButtonVisible(bool vi)
{
    button.setVisible( vi );
}

void UMLClassEntry::init()
{
    QTextDocument *doc = this->document();

    QObject::connect( doc, SIGNAL( contentsChanged() ), SLOT( textChange() ));

    button.setParentItem( parentItem() );
    button.setTextItem( this );
    SvgRenderer::getInstance()->setDeleteButtonRenderer( &button );
    button.setVisible(false);
    setTextInteractionFlags( Qt::TextEditorInteraction );
    button.setFlag( QGraphicsItem::ItemIsSelectable );

    setTextItemHight( 15 );
}

void UMLClassEntry::validateAttribute( QString text )
{
    text.remove('\n');
    QRegExp reg("[+\\-~]?\\w+\\s\\w+");
    QRegExpValidator val(reg);
    qint32 i = 0;
    if( val.validate( text, i ) != QValidator::Acceptable){
        //
    }
}

void UMLClassEntry::validateMethod(QString)
{
}

void UMLClassEntry::setTextItemHight(qint32 hight)
{
    textItemHight = hight;

    qreal x = textItemHight*0.007;
    button.setTransform( QTransform( x, 0, 0, x, 1, 1));
}

void UMLClassEntry::setButtonPos()
{
    button.setPos( parentItem()->boundingRect().width() - (textItemHight) , this->pos().y()+ (textItemHight*0.3));
}

void UMLClassEntry::setFontSize(qint32 size, bool bold)
{
    QFont tempFont = font();
    tempFont.setBold( bold );
    tempFont.setPixelSize( size );
    setFont( tempFont );
}

void UMLClassEntry::deleteItem()
{
    setTextInteractionFlags( Qt::NoTextInteraction );
    setFlag( QGraphicsItem::ItemIsSelectable, false );

    switch(type)
    {
    case MethodType:
        ((UMLClass*)parentItem()->parentItem())->removeMethod( this );
        break;
    case AttributeType:
        ((UMLClass*)parentItem()->parentItem())->removeAttribute( this );
        break;
    case ClassType:
        UMLModel::instance()->removeClass(((UMLClass*)parentItem()->parentItem())->modelClass());
        break;
    }
}

void UMLClassEntry::setVisible( bool visible )
{
    QGraphicsTextItem::setVisible( visible );
    button.setVisible( visible );
}

void UMLClassEntry::setPixmap(QGraphicsPixmapItem *pixmapItem)
{
    pixmap = pixmapItem;
    pixmap->setParentItem( this );
    pixmap->moveBy(-16,0);
}

QString UMLClassEntry::getCurrentText()
{
    return document()->toPlainText();
}

QString UMLClassEntry::getOldText()
{
    dirty = false;
    return oldText;
}

QVariant UMLClassEntry::itemChange( GraphicsItemChange change, const QVariant &value )
{
    setButtonPos();
    if(!dirty)
        oldText = document()->toPlainText();
    return QGraphicsTextItem::itemChange( change, value );
}

void UMLClassEntry::textChange()
{
    dirty = true;
    ((UMLClass*)parentItem()->parentItem())->updateLayout();
    setButtonPos();
    validateAttribute(document()->toPlainText());
}

