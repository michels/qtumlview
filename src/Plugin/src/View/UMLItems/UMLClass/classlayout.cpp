#include "classlayout.h"
#include "View/UMLItems/UMLClass/umlclassentry.h"

ClassLayout* ClassLayout::theInstance = NULL;

ClassLayout *ClassLayout::getInstance()
{
    if( theInstance == NULL )
        theInstance = new ClassLayout();
    return theInstance;
}

void ClassLayout::release()
{
    if( theInstance != NULL )
        delete theInstance;
    theInstance = NULL;
}

//// DEFAULT VALUES (can be modified)/////
ClassLayout::ClassLayout()
{
    // set Default values
    translateToCenter = false;      // translate origin of class to its center
    textHeight = 12;        // 8
    textItemHeight = 16;    // 15
    spacerHeight = 2;       // 2
    minimumWidth = 70;      // 70


    // init of the rest
    countMethods = 0;
    countAttributes = 0;
}

//// Calculation /////
void ClassLayout::update( UMLClass *umlClass )
{
    //// calculation of layout ////
    countMethods = 0; countAttributes = 0; width = minimumWidth;

    //// calculate with, dynamic to longest member, attribute
    // width of class Name
    width = qMax(width, umlClass->classText->boundingRect().width() + 10 );

    //width of member
    foreach(UMLClassEntry::Ptr method, umlClass->methods){
        countMethods++;
        width = qMax(width, method->boundingRect().width() + 16 );
    }
    countMethods = qMax( countMethods, 3);
    countMethods += 2;

    //width of attributes
    foreach(UMLClassEntry::Ptr attrib, umlClass->attributes){
        countAttributes++;
        //i->adjustSize();
        width = qMax(width, attrib->boundingRect().width() + 16 );
    }
    countAttributes = qMax( countAttributes, 3);
    countAttributes += 2;
    width += 25;

    //// calc hight of member and attributes rect
    // calc hight by the counted items
    classLayout.width = qIntCast( width );
    classLayout.methodsStart = textItemHeight*2 + spacerHeight; // more space for class Name
    classLayout.memberHeight = countMethods * textItemHeight;
    classLayout.attributesStart = classLayout.methodsStart + classLayout.memberHeight + spacerHeight;
    classLayout.attributesHeight = countAttributes * textItemHeight;
    classLayout.height = classLayout.attributesStart + classLayout.attributesHeight;

    assignLayout( umlClass );
}
void ClassLayout::assignLayout( UMLClass *umlClass )
{
    //// assign Layout ////
    umlClass->setPen( QPen(Qt::transparent ));
    umlClass->setBrush( QBrush( Qt::transparent ));

    // set size of Class
    QPolygon rect;
    rect.putPoints( 0, 5, 0,0, 0,classLayout.height, classLayout.width,classLayout.height, classLayout.width,0, 0,0);
    if( translateToCenter )
        rect.translate( (qint32)-(classLayout.width/2), (qint32)-(classLayout.height/2) );
    umlClass->setPolygon( rect );

    // set size of content
    umlClass->classNameRect->setRect( 0 ,0, classLayout.width,classLayout.methodsStart-spacerHeight);
    umlClass->classNameRect->setPen( QPen(Qt::transparent));

    umlClass->memberSpacer.setBrush( QBrush( Qt::black ));
    umlClass->memberSpacer.setRect( 1.5, classLayout.methodsStart - spacerHeight, classLayout.width-3, spacerHeight );

    umlClass->memberRect->setRect( 0, classLayout.methodsStart, classLayout.width, classLayout.memberHeight );
    umlClass->memberRect->setPen( QPen(Qt::transparent));

    umlClass->attributesSpacer.setBrush( QBrush( Qt::black ));
    umlClass->attributesSpacer.setRect( 1.5, classLayout.attributesStart - spacerHeight, classLayout.width-3 , spacerHeight );

    umlClass->attributesRect->setRect( 0, classLayout.attributesStart, classLayout.width, classLayout.attributesHeight );
    umlClass->attributesRect->setPen( QPen(Qt::transparent));

    //// set scale factor for background (svg = 100x100 px)
    qreal x = classLayout.width;
    x = x/100.;
    qreal y = classLayout.height;
    y = y/100.;
    umlClass->classSVGBackground.setTransform( QTransform( x, 0, 0, y, 0, 0 ));

    x = textItemHeight;
    x *= 0.007;
    umlClass->addMethodsButton.setTransform( QTransform( x, 0, 0, x, 0, 0 ));
    umlClass->addAttributeButton.setTransform( QTransform( x, 0, 0, x, 0, 0 ));

    //// positioning of textItems
    umlClass->classText->setFontSize( textHeight+2, true );
    umlClass->classText->setTextItemHight( textItemHeight+2 );
    umlClass->classText->setPos( 15, 0);

    countMethods = 0;
    foreach(UMLClassEntry::Ptr method, umlClass->methods){
        method->setPos( 21, classLayout.methodsStart + ( textItemHeight * countMethods ));
        method->setFontSize( textHeight );
        method->setTextItemHight( textItemHeight );
        method->setButtonPos();
        countMethods++;
    }
    umlClass->addMethodsButton.setPos( 10, classLayout.methodsStart + ( textItemHeight * countMethods )+5);

    countAttributes = 0;
    foreach(UMLClassEntry::Ptr attrib, umlClass->attributes){
        attrib->setPos( 21, classLayout.attributesStart + ( textItemHeight * countAttributes ));
        attrib->setFontSize( textHeight );
        attrib->setTextItemHight( textItemHeight );
        attrib->setButtonPos();
        countAttributes++;
    }
    umlClass->addAttributeButton.setPos( 10, classLayout.attributesStart + ( textItemHeight * countAttributes )+5);

    translateClass( umlClass );

    umlClass->itemLayout = classLayout;

}

void ClassLayout::translateClass( UMLClass *umlClass )
{
    if(!translateToCenter)
        return;

    qint32 x = -classLayout.width/2;
    qint32 y = -classLayout.height/2;

    umlClass->classNameRect->setPos( x, y );
    umlClass->memberSpacer.setPos( x, y );
    umlClass->memberRect->setPos( x, y );
    umlClass->attributesSpacer.setPos( x, y );
    umlClass->attributesRect->setPos( x, y );
    umlClass->classSVGBackground.setPos( x, y );
}
