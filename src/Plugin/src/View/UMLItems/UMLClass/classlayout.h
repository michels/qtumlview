#ifndef CLASSLAYOUT_H
#define CLASSLAYOUT_H

#include "View/UMLItems/UMLClass/umlclass.h"

class ClassLayout
{

public:
    static ClassLayout* getInstance();
    static void release();

    void update( UMLClass* umlClass );

private:
    ClassLayout();
    static ClassLayout *theInstance;

    bool translateToCenter;

    qint32 countMethods;
    qint32 countAttributes;
    qint32 textHeight;
    qint32 textItemHeight;
    qint32 spacerHeight;
    qreal width;
    qreal minimumWidth;
    UMLClass::ItemLayout classLayout;

    void assignLayout( UMLClass *umlClass );
    void translateClass( UMLClass *umlClass );
};

#endif // CLASSLAYOUT_H
