#include "svgrenderer.h"
#include <QDir>

SvgRenderer* SvgRenderer::theInstance = NULL;

SvgRenderer::SvgRenderer()
{
    QDir file(":/view/Images/");

    classRenderer.load( file.filePath("classItem.svg") );
    deleteButtonRenderer.load( file.filePath("deleteButton.svg") );
    addButtonRenderer.load( file.filePath("addButton.svg") );
}

SvgRenderer *SvgRenderer::getInstance()
{
    if( theInstance == NULL )
        theInstance = new SvgRenderer();
    return theInstance;
}

void SvgRenderer::release()
{
    if( theInstance != NULL )
        delete theInstance;
    theInstance = NULL;
}

void SvgRenderer::setClassRenderer(QGraphicsSvgItem *item)
{
    item->setSharedRenderer( &classRenderer );
}

void SvgRenderer::setDeleteButtonRenderer(QGraphicsSvgItem *item)
{
    item->setSharedRenderer( &deleteButtonRenderer );
}

void SvgRenderer::setAddButtonRenderer(QGraphicsSvgItem *item)
{
    item->setSharedRenderer( &addButtonRenderer );
}


