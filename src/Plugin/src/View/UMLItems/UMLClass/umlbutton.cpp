#include "umlbutton.h"

#include "View/UMLItems/UMLClass/umlclassentry.h"
#include "View/UMLItems/UMLClass/umlclass.h"

UMLButton::UMLButton(QGraphicsItem *parentItem) :
    QGraphicsSvgItem( parentItem )
{
    isDelButton();
    setAcceptedMouseButtons(Qt::LeftButton);
    setVisible(false);
}

void UMLButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (!boundingRect().contains(event->pos()))
        return;
    if( addButton ){
        if( isMethod ){
            ((UMLClass*)parentItem()->parentItem())->addMethod( "void newMember();\n" );
            return;
        }
        ((UMLClass*)parentItem()->parentItem())->addAttribute( "int newAttribute;\n" );
        return;
    }
    textItem->deleteItem();
    parentItem()->parentItem()->setFocus( Qt::MouseFocusReason );
    event->accept();
}
