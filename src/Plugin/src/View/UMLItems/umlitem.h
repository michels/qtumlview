#ifndef UMLITEM_H
#define UMLITEM_H

#include <QGraphicsPolygonItem>

class UMLConnector;

class UMLItem : public QGraphicsPolygonItem
{
public:
    UMLItem( QGraphicsItem *parent = 0 );

    enum Type{
        KeksType = UserType,
        ClassType = UserType + 1,
        NoteType = UserType + 2,

        LineSelectorType = UserType + 8,

        GeneralizationType = QGraphicsItem::UserType + 10,
        AssociationType = QGraphicsItem::UserType + 11,
        CompositionType = QGraphicsItem::UserType + 12,
        AggregationType = QGraphicsItem::UserType + 13
    };

    void registerConnector( UMLConnector *item );
    QList<UMLConnector*> getConnector();
    virtual void updateUMLConnector( QPointF ) = 0;
    QPointF getCenter();
    void delConnector( UMLConnector* );

protected:
    QList<UMLConnector*> connectedLines;
    QVariant itemChange( GraphicsItemChange change, const QVariant &value );
};

#endif // UMLITEM_H
