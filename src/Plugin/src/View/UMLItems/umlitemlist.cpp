#include "umlitemlist.h"

#include "View/UMLItems/UMLClass/umlclass.h"
#include "View/UMLItems/Connector/umlconnector.h"
#include "View/UMLItems/Connector/umlgeneralization.h"
#include "View/UMLItems/umlitemcreator.h"

UMLItemList* UMLItemList::theInstance = NULL;

UMLItemList::UMLItemList( QObject *parent ) :
    QObject( parent )
{
}

void UMLItemList::addClass( UMLClass *umlClass )
{
    nodes.append( umlClass );

    UMLItemCreator* itemCreator = UMLItemCreator::getInstance();

    // add Generalistions
    foreach( UMLView::Model::ModelClass* i, umlClass->modelClass()->superClasses() ){
        qDebug() << i->className();
        itemCreator->createGeneralization( umlClass );
        itemCreator->setEndItem(i->getUiElement());
    }

    // add Connections
    foreach( ModelClassAttribute::Ptr attr,  umlClass->modelClass()->attributes()){
        qDebug() << "check Attribute: " << attr->toString();
        foreach( UMLClass* i, nodes ){
            QString attributeType = attr->toString();
            if(attributeType.contains( i->modelClass()->className() )){
                itemCreator->createComposition( umlClass );
                itemCreator->setEndItem( i );
            }
        }
    }
}

void UMLItemList::updateConnector( UMLClass *, bool temp ){

    //if(temp)
    //    return;

    UMLItemCreator* itemCreator = UMLItemCreator::getInstance();
    QStringList allAttributeTypes;
    QList<UMLConnector*> classConnector;
    UMLClass* umlClassToConnect;
    bool done = false;
    bool aggregation = false;

    foreach( UMLClass *umlClass, nodes){

    allAttributeTypes = umlClass->AttributeTypes();
    classConnector = umlClass->getConnector();


    foreach(QString i, allAttributeTypes){
        aggregation = false;
        done = false;

        i.remove(" ");
        if(i.endsWith("*")){
            i.remove(i.size()-1,1);
            aggregation = true;
        }
        umlClassToConnect = getClass( i );
        if(i == NULL){
            continue;
        }

        // search connector
        for( int i = 0; i < classConnector.size(); i++ ){
            if( classConnector.at(i)->getStartItem() == umlClass ){
                if( classConnector.at(i)->getEndItem() == umlClassToConnect ){
                    done = true;
                    break;
                }
            }else{
//                if( classConnector.at(i)->getStartItem() == umlClassToConnect ){
//                    done = true;
//                    break;
//                }
            }
        }

        // if not available create it
        if(!done){
            if(aggregation)
                itemCreator->createAggregation( umlClass );
            else
                itemCreator->createComposition( umlClass );

            itemCreator->setEndItem( umlClassToConnect, false );
        }
    }
    }
    // inverse
}

void UMLItemList::addConnection( UMLConnector *umlConnector )
{
    connectingEdges.append( umlConnector );
}

void UMLItemList::addGeneralization( UMLGeneralization *umlGeneralization )
{
    hierarchicalEdges.append( umlGeneralization );
}

QList<UMLClass *> UMLItemList::getClasses()
{
    return nodes;
}

UMLClass *UMLItemList::getClass( QString className )
{
    foreach( UMLClass* i, nodes ){
        if(i->getClassName().compare( className ) == 0)
            return i;
    }
    return NULL;
}

void UMLItemList::delClass( UMLClass *umlClass )
{
    nodes.removeOne( umlClass );
}

void UMLItemList::delConnection( UMLConnector *umlConnector )
{
    connectingEdges.removeOne( umlConnector );
}

void UMLItemList::delConnections()
{
    foreach(UMLConnector* i, connectingEdges){
        i->delConnector();
    }
}

void UMLItemList::delGeneralization( UMLGeneralization *umlGeneralization )
{
    hierarchicalEdges.removeOne( umlGeneralization );
}

UMLItemList *UMLItemList::getInstance()
{
    if( theInstance == NULL )
        theInstance = new UMLItemList();
    return theInstance;
}

void UMLItemList::release()
{
    if( theInstance != NULL )
        delete theInstance;
    theInstance = NULL;
}
