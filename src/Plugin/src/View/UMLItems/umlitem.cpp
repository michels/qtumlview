#include "umlitem.h"

#include "View/UMLItems/Connector/umlconnector.h"

UMLItem::UMLItem( QGraphicsItem *parent ) :
    QGraphicsPolygonItem( parent )
{
    connectedLines.clear();
    setZValue( 5 );

    setFlag( QGraphicsItem::ItemSendsGeometryChanges );
    setFlag( QGraphicsItem::ItemIsSelectable );
    setFlag( QGraphicsItem::ItemIsMovable );
}

void UMLItem::registerConnector( UMLConnector *item )
{
    connectedLines.append( item );
}

QList<UMLConnector *> UMLItem::getConnector()
{
    return connectedLines;
}

QPointF UMLItem::getCenter()
{
    return mapFromItem( this,
                 scenePos().x() + boundingRect().width()/2,
                        scenePos().y() + boundingRect().height()/2);
}

void UMLItem::delConnector( UMLConnector *con )
{
    connectedLines.removeOne( con );
}

QVariant UMLItem::itemChange( QGraphicsItem::GraphicsItemChange change, const QVariant &value )
{
    return QGraphicsPolygonItem::itemChange( change, value );
}
