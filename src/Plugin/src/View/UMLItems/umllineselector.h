#ifndef UMLLINESELECTOR_H
#define UMLLINESELECTOR_H

#include "View/UMLItems/umlitem.h"

class GraphicsConnector;

class UMLLineSelector : public UMLItem
{
public:
    UMLLineSelector();

    int type() const { return LineSelectorType; }
    void setSelectable( bool in );
    void setActive(bool in){ active = in; }

    void setFirstConnector( GraphicsConnector* con );
    void setSecondConnector( GraphicsConnector* con );

    GraphicsConnector* getFirstConnector();
    GraphicsConnector* getSecondConnector();

    void shrink();
    qint32 getOrientation();
    void updateUMLConnector(QPointF delta);

    UMLLineSelector* duplicateSelector( QPointF, GraphicsConnector* );

private:
    bool active;
    bool isSelectable;
    GraphicsConnector* firstConnector;
    GraphicsConnector* secondConnector;

protected:
    QVariant itemChange( GraphicsItemChange change, const QVariant &value );
};

#endif // UMLLINESELECTOR_H
