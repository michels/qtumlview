#include "umllineselector.h"

#include <QBrush>
#include "View/UMLItems/Connector/umlconnector.h"
#include "View/UMLItems/Connector/graphicsconnector.h"


UMLLineSelector::UMLLineSelector()
{
    setZValue( 13 );
    isSelectable = true;

    QPolygon rect;
    rect.putPoints(0, 5, 0,0, 6,0, 6,6, 0,6, 0,0);
    setPolygon( rect );

    setBrush( QBrush( Qt::black ));
}

void UMLLineSelector::setSelectable(bool in)
{
    isSelectable = in;

    setFlag( QGraphicsItem::ItemIsSelectable, in );
    setFlag( QGraphicsItem::ItemIsMovable, in );
}

void UMLLineSelector::setFirstConnector(GraphicsConnector *con)
{
    firstConnector = con;
}

void UMLLineSelector::setSecondConnector(GraphicsConnector *con)
{
    secondConnector = con;
}

GraphicsConnector *UMLLineSelector::getFirstConnector()
{
    return firstConnector;
}

GraphicsConnector *UMLLineSelector::getSecondConnector()
{
    return secondConnector;
}

void UMLLineSelector::shrink()
{
    QPolygon rect;
    rect.putPoints(0, 5, 0,0, 0,0, 0,0, 0,0, 0,0);
    setPolygon( rect );
}

qint32 UMLLineSelector::getOrientation()
{
    return 0;
}

void UMLLineSelector::updateUMLConnector(QPointF delta)
{
    foreach( UMLConnector *i, connectedLines ){
        if( i == NULL )
            continue;
        i->updateConnector( this, delta, false );
    }
}

UMLLineSelector *UMLLineSelector::duplicateSelector( QPointF point, GraphicsConnector *con )
{
    UMLLineSelector* selector = new UMLLineSelector();
    selector->setSelectable( true );
    selector->setFirstConnector( con );
    selector->setSecondConnector( this->getSecondConnector() );

    setSecondConnector( con );

    // maybe two cases ( horizontal and vertical )
    selector->setX( this->x() );
    selector->setY( point.y() );

    setX( point.y() );

    updateUMLConnector(QPoint(0,0));

    return selector;
}

QVariant UMLLineSelector::itemChange( QGraphicsItem::GraphicsItemChange change, const QVariant &value )
{
    if(!active){
        return UMLItem::itemChange( change, value );
    }

    if( change == QGraphicsItem::ItemPositionChange ){
        QPointF org = pos();
        updateUMLConnector(value.toPointF().operator -=(org));
    }

    return UMLItem::itemChange( change, value );
}
