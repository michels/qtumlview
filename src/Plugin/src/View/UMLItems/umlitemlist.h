#ifndef UMLITEMLIST_H
#define UMLITEMLIST_H

#include <QObject>
#include "pch.h"

class UMLClass;
class UMLConnector;
class UMLGeneralization;

class UMLItemList : public QObject
{
    Q_OBJECT
public:
    static UMLItemList* getInstance();
    static void release();
    
public slots:
    void addClass( UMLClass* umlClass );

    void addConnection( UMLConnector* umlConnector );
    void addGeneralization( UMLGeneralization* umlGeneralization ); // hierarchical

    QList< UMLClass* > getClasses();
    UMLClass* getClass( QString );
    void updateConnector( UMLClass *umlClass, bool temp = true );

    void delClass( UMLClass* umlClass );
    void delConnection( UMLConnector* umlConnector );
    void delConnections();
    void delGeneralization( UMLGeneralization* umlGeneralization ); // hierarchical

    QList< UMLClass* > getNodes(){return nodes;}
    QList< UMLConnector* > getHorizontalEdges(){return connectingEdges;}
    QList< UMLGeneralization* > getVerticalEdges(){return hierarchicalEdges;}
    
private:
    explicit UMLItemList(QObject *parent = 0);

    static UMLItemList* theInstance;
    QList< UMLClass* > nodes;
    QList< UMLConnector* > connectingEdges;
    QList< UMLGeneralization* >  hierarchicalEdges;
};

#endif // UMLITEMLIST_H
