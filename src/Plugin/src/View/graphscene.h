#ifndef GRAPHSCENE_H
#define GRAPHSCENE_H

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QBrush>
#include <QAction>
#include <QMenu>

#include "View/UMLItems/umlitem.h"
#include "View/UMLItems/UMLClass/umlclass.h"

class SideBar;
class ModelClass;
class UMLItemCreator;

class GraphScene : public QGraphicsScene
{
    Q_OBJECT

public:
    GraphScene( QObject *parent = 0 );
    GraphScene( const QRectF & sceneRect, QObject * parent = 0 );
    GraphScene( qreal x, qreal y, qreal width, qreal height, QObject * parent = 0 );
    ~GraphScene();

    void clearScene();
    void setSidebar( SideBar* );
    QPointF getLastContextClickPoint();

private slots:
    void addNewClass();
    void addNewNote();
    void removeClass();
    void updateView();

private:
    bool createConnector;
    QPointF contextClickPoint;
    UMLItemCreator* itemCreator;
    QGraphicsItem *currentItem;

    QMenu generalContextMenu;
    QMenu classContextMenu;

    QList<UMLClass*> nodes;

    void init();
    void contextMenuAction(QString action, QPointF point, QGraphicsItem* item = 0 );
    QGraphicsItem *getParentItem(QGraphicsItem*);

protected:
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
};

#endif // GRAPHSCENE_H
