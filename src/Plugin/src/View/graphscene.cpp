#include "graphscene.h"

#include <QMenu>
#include "View/UMLItems/UMLClass/classlayout.h"
#include "View/UMLItems/UMLClass/svgrenderer.h"
#include "View/UMLItems/umlitemlist.h"
#include "View/UMLItems/umlitemcreator.h"
#include "GraphCalc/graphinit.h"
#include <projectexplorer/projectexplorer.h>
#include <coreplugin/coreconstants.h>
#include <coreplugin/icore.h>
#include <coreplugin/actionmanager/actionmanager.h>
#include <coreplugin/dialogs/iwizard.h>

GraphScene::GraphScene(QObject *parent) :
    QGraphicsScene( parent )
{
    init();
}

GraphScene::GraphScene( const QRectF &sceneRect, QObject *parent ) :
    QGraphicsScene( sceneRect, parent )
{
    init();
}

GraphScene::GraphScene(qreal x, qreal y, qreal width, qreal height, QObject *parent ) :
    QGraphicsScene( x, y, width, height, parent )
{
    init();
}

GraphScene::~GraphScene()
{
    ClassLayout::release();
    SvgRenderer::release();
    UMLItemCreator::release();
    UMLItemList::release();
}

void GraphScene::clearScene()
{
    QList<UMLClass*> classes = UMLItemList::getInstance()->getClasses();
    foreach( UMLClass* i, classes ){
        i->removeClass();
    }
}

void GraphScene::init()
{
    createConnector = false;
    itemCreator = UMLItemCreator::getInstance();
    itemCreator->setScene( this );

    contextClickPoint = QPointF( 0, 0 );

    generalContextMenu.addAction(QIcon(QLatin1String(Core::Constants::ICON_PLUS)),
                                 tr("New Class", "Add a new class"), this, SLOT(addNewClass()));

    generalContextMenu.addAction(QIcon(QLatin1String(Core::Constants::ICON_NEWFILE)),
                                 tr("Note", "Add a new note"), this, SLOT(addNewNote()));
    generalContextMenu.addSeparator();
    generalContextMenu.addAction("update View", this, SLOT(updateView()), QKeySequence::Refresh );

    classContextMenu.addAction("addGeneralization");
    classContextMenu.addAction("addAssociation");
    classContextMenu.addAction("addComposition");
    classContextMenu.addAction("addAggregation");
    classContextMenu.addSeparator();
    classContextMenu.addAction(QIcon(QLatin1String(Core::Constants::ICON_MINUS)),
                                 tr("Remove Class", "Remove this class"), this, SLOT(removeClass()));

    //setSceneRect( -2000, -1000, 4000, 2000 );
}

void GraphScene::contextMenuAction(QString action, QPointF point, QGraphicsItem *item )
{
    if( item == NULL )
        return;

    if( action.compare( "addGeneralization" ) == 0 ){
        itemCreator->createGeneralization( item );
    }
    else if( action.compare( "addAssociation" ) == 0 ){
        itemCreator->createAssociation( item );
    }
    else if( action.compare( "addComposition" ) == 0 ){
        itemCreator->createComposition( item );
    }
    else if( action.compare( "addAggregation" ) == 0 ){
        itemCreator->createAggregation( item );
    }
}

QGraphicsItem* GraphScene::getParentItem( QGraphicsItem *item )
{
    // search for a parent with User specified type
    while( item != NULL ){
        if( item->type() > QGraphicsItem::UserType ){
            break;
        }
        item = item->parentItem();
    }
    return item;
}

void GraphScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem* currentItem = itemAt( event->scenePos() );
    currentItem = getParentItem( currentItem );
    itemCreator->setEndItem( currentItem );

    QGraphicsScene::mouseReleaseEvent( event );
}


void GraphScene::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    currentItem = itemAt( event->scenePos() );
    currentItem = getParentItem( currentItem );
    contextClickPoint = event->scenePos();

    if( NULL == currentItem ){
        generalContextMenu.exec( event->screenPos() );
    }else{
        if( currentItem->type() == UMLItem::ClassType ){
            QAction *action  = classContextMenu.exec( event->screenPos() );
            if( NULL != action ){
                contextMenuAction( action->text(), event->scenePos(), currentItem );
            }
        }
    }
}

void GraphScene::addNewClass()
{
    QVariantMap map;
    map.insert(QLatin1String(ProjectExplorer::Constants::PREFERED_PROJECT_NODE),
               ProjectExplorer::ProjectExplorerPlugin::currentProject()->projectDirectory());
    Core::ICore::showNewItemDialog(tr("New File", "Title of dialog"),
                                   Core::IWizard::wizardsOfKind(Core::IWizard::FileWizard)
                                   + Core::IWizard::wizardsOfKind(Core::IWizard::ClassWizard),
                                   ProjectExplorer::ProjectExplorerPlugin::currentProject()->projectDirectory());
}

void GraphScene::addNewNote()
{
}

void GraphScene::removeClass()
{
    if (NULL != currentItem && currentItem->type() == UMLItem::ClassType)
    {
        UMLClass *cls =(UMLClass*)currentItem;
        UMLModel::instance()->removeClass(cls->modelClass());
    }
}

void GraphScene::updateView()
{
    GraphInit::getInstance()->calcOrthogonalLayout();

    QRectF boundingRect = itemsBoundingRect();
    boundingRect.setTopLeft( QPointF( -4000, -4000 ));
    QPointF br = boundingRect.bottomRight();
    br.setX( br.x() + 4000 );
    br.setY( br.y() + 4000 );
    boundingRect.setBottomRight( br );
    setSceneRect( boundingRect );

}

QPointF GraphScene::getLastContextClickPoint()
{
    QPointF ret = contextClickPoint;
    contextClickPoint.setX(0);
    contextClickPoint.setY(0);
    return ret;
}
