#include "graphview.h"

#include <QScrollBar>

GraphView::GraphView( QWidget *parent ) :
    QGraphicsView( parent )
{
    init();
}

GraphView::GraphView( QGraphicsScene* scene, QWidget *parent ) :
    QGraphicsView( scene, parent )
{
    init();
}

void GraphView::init()
{
    // doesn't rock
    //setViewport( new QGLWidget( QGLFormat(QGL::SampleBuffers | QGL::DirectRendering )) ); // for hardware rendering
    setDragMode( QGraphicsView::ScrollHandDrag );
    viewport()->setCursor( Qt::ArrowCursor );
    setTransformationAnchor( QGraphicsView::AnchorUnderMouse );
    this->setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    this->setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );

    _numScheduledScalings = 0;
    minZoom = 0.1;
    maxZoom = 5;
}

void GraphView::scalingTime(qreal x)
{
    //QPointF currentViewPoint = mapToScene(0,0);
    qint32 xScroll = horizontalScrollBar()->value();
    qint32 yScroll = verticalScrollBar()->value();

    // scale view
    qreal factor = 1.0 + qreal(_numScheduledScalings) / 300.0;
    scale(factor, factor);

    // set scale limitations
    if( transform().m11() < minZoom ){
        setTransform( QTransform( minZoom, 0 , 0, minZoom, 1, 1 ));

        horizontalScrollBar()->setValue( xScroll );
        verticalScrollBar()->setValue( yScroll );
    }

    if( transform().m11() > maxZoom ){
        setTransform( QTransform( maxZoom, 0 , 0, maxZoom, 1, 1 ));

        horizontalScrollBar()->setValue( xScroll );
        verticalScrollBar()->setValue( yScroll );
    }

}

void GraphView::animFinished()
{
    if (_numScheduledScalings > 0)
        _numScheduledScalings--;
    else
        _numScheduledScalings++;
    sender()->~QObject();
}

void GraphView::wheelEvent(QWheelEvent *event)
{
    int numDegrees = event->delta() / 8;
    int numSteps = numDegrees / 15;  // see QWheelEvent documentation
    _numScheduledScalings += numSteps;
    if (_numScheduledScalings * numSteps < 0)  // if user moved the wheel in another direction, we reset previously scheduled scalings
        _numScheduledScalings = numSteps;

    QTimeLine *anim = new QTimeLine(350, this);
    anim->setUpdateInterval(20);

    connect(anim, SIGNAL(valueChanged(qreal)), SLOT(scalingTime(qreal)));
    connect(anim, SIGNAL(finished()), SLOT(animFinished()));
    anim->start();
}

void GraphView::keyPressEvent( QKeyEvent *event )
{
    if( event->key() == Qt::Key_Control ) {
        setDragMode( QGraphicsView::RubberBandDrag );
        return;
    }
    QGraphicsView::keyPressEvent(event);
    setCursor( Qt::ArrowCursor );
}

void GraphView::keyReleaseEvent(QKeyEvent *event)
{
    if( event->key() == Qt::Key_Control ) {
        setDragMode( QGraphicsView::ScrollHandDrag );
        viewport()->setCursor( Qt::ArrowCursor );
        return;
    }
    QGraphicsView::keyReleaseEvent(event);
}

void GraphView::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent( event );
    viewport()->setCursor( Qt::ArrowCursor );
}
