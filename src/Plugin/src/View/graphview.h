#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>
#include <QTimeLine>

class GraphView : public QGraphicsView
{
    Q_OBJECT

public:
    GraphView( QWidget *parent = 0 );
    GraphView( QGraphicsScene* scene, QWidget* parent = 0 );

private:
    qint32 _numScheduledScalings;
    qreal minZoom;
    qreal maxZoom;

    void init();

public slots:
    void scalingTime( qreal x );
    void animFinished();

protected:
    void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void mouseReleaseEvent(QMouseEvent *event);
};

#endif // GRAPHVIEW_H
