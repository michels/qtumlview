#ifndef UMLVIEWCONSTANTS_H
#define UMLVIEWCONSTANTS_H

namespace UMLView {
namespace Constants {


const char MODE_UMLVIEW[] = "UMLView";
const int  P_MODE_UMLVIEW = 76;

const char UMLVIEWTASK_ID[] = "UMLView.TaskId";

const char C_UMLVIEWMODE[] = "UMLView.UMLViewMode";

} // namespace UMLView
} // namespace Constants

#endif // UMLVIEWCONSTANTS_H

