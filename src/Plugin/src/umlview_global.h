#ifndef UMLVIEW_GLOBAL_H
#define UMLVIEW_GLOBAL_H

#include <QtCore/QtGlobal>

#if defined(UMLVIEW_LIBRARY)
#  define UMLVIEWSHARED_EXPORT Q_DECL_EXPORT
#else
#  define UMLVIEWSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // UMLVIEW_GLOBAL_H

