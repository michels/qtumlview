#include "umlviewplugin.h"
#include "umlviewconstants.h"
#include "umlviewmode.h"

#include <projectexplorer/projectexplorer.h>

#include <QtCore/QtPlugin>

#include "model/refactoring.h"
#include "View/UMLItems/umlitemcreator.h"

using namespace UMLView::Internal;
using namespace CPlusPlus;
using namespace ProjectExplorer;
using namespace UMLView::Model;

UMLViewPlugin::UMLViewPlugin()
    :m_mode(NULL)
{
    // Create your members
}

UMLViewPlugin::~UMLViewPlugin()
{
    // Unregister objects from the plugin manager's object pool
    // Delete members
}

bool UMLViewPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    addAutoReleasedObject(m_mode = new UMLViewMode());
    addAutoReleasedObject(Refactoring::instance());
    addAutoReleasedObject(UMLModel::instance());

    connectTheSignals();

    return true;
}

void UMLViewPlugin::connectTheSignals()
{
    connect(CppModelManagerInterface::instance(), SIGNAL(documentUpdated(CPlusPlus::Document::Ptr)),
            UMLModel::instance(), SLOT(onDocumentUpdated(CPlusPlus::Document::Ptr)));
    connect(CppModelManagerInterface::instance(), SIGNAL(aboutToRemoveFiles(QStringList)),
            UMLModel::instance(), SLOT(onAboutToRemoveFiles(QStringList)));
    connect(ProjectExplorerPlugin::instance(), SIGNAL(currentProjectChanged(ProjectExplorer::Project*)),
            SLOT(currentProjectChanged(ProjectExplorer::Project *)));

    connect(UMLModel::instance(), SIGNAL(newClass(UMLView::Model::ModelClass*)), UMLItemCreator::getInstance(),
            SLOT(createClass(UMLView::Model::ModelClass*)));
}

void UMLViewPlugin::extensionsInitialized()
{
    // Retrieve objects from the plugin manager's object pool
    // "In the extensionsInitialized method, a plugin can be sure that all
    //  plugins that depend on it are completely initialized."
}

ExtensionSystem::IPlugin::ShutdownFlag UMLViewPlugin::aboutToShutdown()
{
    // Save settings
    // Disconnect from signals that are not needed during shutdown
    // Hide UI (if you add UI that is not in the main window directly)
    return SynchronousShutdown;
}

void UMLViewPlugin::currentProjectChanged(Project *newProject)
{
    if (newProject == NULL)
    {
        m_mode->setEnabled(false);
        UMLModel::instance()->clear();
    }
    else
        m_mode->setEnabled(true);
}

Q_EXPORT_PLUGIN2(UMLView, UMLViewPlugin)

