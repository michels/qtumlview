#include "umlviewmode.h"
#include "model/modelclass.h"

#include <QListWidget>


using namespace UMLView::Internal;


UMLViewMode::UMLViewMode(QObject *parent)
    : IMode(parent)
{
    setContext(Context(C_EDITORMANAGER, C_UMLVIEWMODE, C_NAVIGATION_PANE));
    setDisplayName(tr("UMLView"));
    setIcon(QIcon(QLatin1String(":/view/Images/uml.png")));
    setPriority(P_MODE_UMLVIEW);
    setId(MODE_UMLVIEW);
    setType(MODE_EDIT_TYPE);
    setEnabled(false);
    m_widget = ui = new QtUmlViewPrototype();
    setWidget(m_widget);
}

