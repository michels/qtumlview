#include "qtumlviewprototype.h"


QtUmlViewPrototype::QtUmlViewPrototype( QWidget *parent ) :
    QWidget( parent )
{
    // Horizontal Layout (Drawing | Sidebar)

    setMinimumSize( 800, 600 );
    QHBoxLayout *mainLayout = new QHBoxLayout( this );
    setLayout( mainLayout );
    view.setScene( &scene );

    mainLayout->addWidget( &view );
}

QtUmlViewPrototype::~QtUmlViewPrototype()
{
}
