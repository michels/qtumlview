#ifndef QTUMLVIEWPROTOTYPE_H
#define QTUMLVIEWPROTOTYPE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QGraphicsView>

#include "View/graphview.h"
#include "View/graphscene.h"

class QtUmlViewPrototype : public QWidget
{
    Q_OBJECT
public:
    explicit QtUmlViewPrototype(QWidget *parent = 0);
    ~QtUmlViewPrototype();
    
private:
    GraphView view;
    GraphScene scene;
};

#endif // QTUMLVIEWPROTOTYPE_H
