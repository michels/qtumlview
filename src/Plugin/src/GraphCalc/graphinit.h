#ifndef GRAPHINIT_H
#define GRAPHINIT_H

#include <QObject>
#include "pch.h"

class GraphicsBasic;
class UMLGeneralization;

#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/planarity/PlanarizationLayout.h>
#include <ogdf/planarity/VariableEmbeddingInserter.h>
#include <ogdf/planarity/FastPlanarSubgraph.h>
#include <ogdf/orthogonal/OrthoLayout.h>
#include <ogdf/planarity/EmbedderMinDepthMaxFaceLayers.h>

using namespace ogdf;

class GraphInit : public QObject
{
    Q_OBJECT
public:
    static GraphInit* getInstance();
    static void release();


private:
    explicit GraphInit(QObject *parent = 0);
    void loadCurrentGraph();
    void drawCalculatedLayout();

    static GraphInit* theInstance;
    Graph G;
    GraphAttributes GA;

    QList<node> graphNodes;
    
signals:
    
public slots:
    void calcOrthogonalLayout();
    
};

#endif // GRAPHINIT_H
