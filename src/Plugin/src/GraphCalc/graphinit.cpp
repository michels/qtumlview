#include "graphinit.h"

#include "View/UMLItems/UMLClass/umlclass.h"
#include "View/UMLItems/umlitemlist.h"
#include "View/UMLItems/Connector/umlgeneralization.h"

GraphInit* GraphInit::theInstance = NULL;

GraphInit::GraphInit(QObject *parent) :
    QObject(parent)
{
    // init Graph
    GA = GraphAttributes(G, GraphAttributes::nodeGraphics | GraphAttributes::edgeGraphics );
}
GraphInit *GraphInit::getInstance()
{
    if( theInstance == NULL )
        theInstance = new GraphInit();
    return theInstance;
}

void GraphInit::release()
{
    if( theInstance != NULL )
        delete theInstance;
    theInstance = NULL;
}

void GraphInit::calcOrthogonalLayout()
{
    qDebug() << "calc orthogonal layout";
    loadCurrentGraph();

    PlanarizationLayout pl;

    FastPlanarSubgraph *ps = new FastPlanarSubgraph;
    ps->runs(100);
    VariableEmbeddingInserter *ves = new VariableEmbeddingInserter;
    ves->removeReinsert(EdgeInsertionModule::rrAll);
    pl.setSubgraph(ps);
    pl.setInserter(ves);

    EmbedderMinDepthMaxFaceLayers *emb = new EmbedderMinDepthMaxFaceLayers;
    pl.setEmbedder(emb);

    OrthoLayout *ol = new OrthoLayout;
    ol->separation(100.0);
    ol->cOverhang(0.4);
    ol->setOptions(2+4);
    pl.setPlanarLayouter(ol);

    pl.call(GA);

    drawCalculatedLayout();
    //GA.writeGML("manual_graph.gml");
}

void GraphInit::loadCurrentGraph()
{
    UMLItemList* itemList = UMLItemList::getInstance();
    QList<UMLClass*> nodes = itemList->getNodes();
    QList<UMLConnector*> edgesG = itemList->getHorizontalEdges();

    graphNodes.clear();

    foreach(UMLClass* i, nodes){
        node classNode = G.newNode();
        graphNodes.append(classNode);
        //GA.x(classNode) = i->x();
        //GA.y(classNode) = i->y();
        GA.width(classNode) = i->boundingRect().width();
        GA.height(classNode) = i->boundingRect().height();
    }

    foreach(UMLConnector* i, edgesG){
        int start = nodes.indexOf((UMLClass*)i->getStartItem());
        int end = nodes.indexOf((UMLClass*)i->getEndItem());
        G.newEdge( graphNodes.at(start), graphNodes.at(end));
    }
}

void GraphInit::drawCalculatedLayout()
{
    UMLItemList* itemList = UMLItemList::getInstance();
    QList<UMLClass*> nodes = itemList->getNodes();
    QList<UMLConnector*> edgesG = itemList->getHorizontalEdges();

    for(int i = 0; i < graphNodes.size(); i++){
        nodes.at(i)->setX( GA.x(graphNodes.at(i)) );
        nodes.at(i)->setY( GA.y(graphNodes.at(i)) );
    }

    foreach(UMLConnector* i, edgesG){
        i->calculateDirectConnection();
    }
}

