#ifndef UMLVIEWMODE_H
#define UMLVIEWMODE_H

#include "umlviewplugin.h"
#include "umlviewconstants.h"
#include "model/umlmodel.h"

#include <coreplugin/coreconstants.h>
#include <coreplugin/icore.h>
#include <coreplugin/imode.h>

#include "qtumlviewprototype.h"

using namespace Core;
using namespace Core::Constants;
using namespace UMLView::Constants;

namespace UMLView {
namespace Internal {

class UMLVIEWSHARED_EXPORT UMLViewMode : public IMode
{
    Q_OBJECT

public:
    UMLViewMode(QObject *parent = 0);

private:
    QtUmlViewPrototype *ui;
};

}
}


#endif // UMLVIEWMODE_H
