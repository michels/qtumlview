#ifndef MODELMODULE_H
#define MODELMODULE_H

#include "pch.h"

namespace UMLView {
namespace Model {
class ModelClass;

class ModelModule : public QObject
{
    Q_OBJECT
public:
    explicit ModelModule(QString mName, QObject *parent = 0);

    void addClass(ModelClass *cls);
    void removeClass(ModelClass*cls);
    void clear();
    QList<ModelClass*> classList(){return classes;}

    QString moduleName(){return modName;}
    void setModuleName(const QString&modName){this->modName = modName;}

signals:
    void classAdded(ModelClass *cls);
    void classRemoved(ModelClass *cls);

private:
    QList<ModelClass*> classes;
    QString modName;
};

}}// end namespaces

#endif // MODELMODULE_H
