#include "pch.h"
#include "refactoring.h"
#include "umlmodel.h"

#include <coreplugin/progressmanager/progressmanager.h>
#include <coreplugin/icore.h>
#include <texteditor/basefilefind.h>
#include <texteditor/refactoringchanges.h>
#include <utils/changeset.h>
#include <utils/fileutils.h>
#include <CppDocument.h>
#include <FindUsages.h>
#include <cpptools/cppmodelmanager.h>
#include <ASTPath.h>
#include <BackwardsScanner.h>
#include <CppRewriter.h>

using namespace CPlusPlus;
using namespace CppTools;
using namespace UMLView::Model;

Refactoring *Refactoring::_instance=NULL;

namespace {

static QString getSource(const QString &fileName,
                         const CppModelManagerInterface::WorkingCopy &workingCopy)
{
    if (workingCopy.contains(fileName)) {
        return workingCopy.source(fileName);
    } else {
        Utils::FileReader reader;
        if (!reader.fetch(fileName, QFile::Text)) // ### FIXME error reporting
            return QString();

        return QString::fromLocal8Bit(reader.data()); // ### FIXME encoding
    }
}

class ProcessFile: public std::unary_function<QString, QList<Occurrence> >
{
    const CppModelManagerInterface::WorkingCopy workingCopy;
    const Snapshot snapshot;
    Document::Ptr symbolDocument;
    Symbol *symbol;
    QFutureInterface<Occurrence> *future;

public:
    ProcessFile(const CppModelManagerInterface::WorkingCopy &workingCopy,
                const Snapshot snapshot,
                Document::Ptr symbolDocument,
                Symbol *symbol,
                QFutureInterface<Occurrence> *future)
        : workingCopy(workingCopy),
          snapshot(snapshot),
          symbolDocument(symbolDocument),
          symbol(symbol),
          future(future)
    { }

    QList<Occurrence> operator()(const QString &fileName)
    {
        QList<Occurrence> occurrences;
        if (future->isCanceled())
            return occurrences;
        const Identifier *symbolId = symbol->identifier();

        if (Document::Ptr previousDoc = snapshot.document(fileName)) {
            Control *control = previousDoc->control();
            if (! control->findIdentifier(symbolId->chars(), symbolId->size()))
                return occurrences; // skip this document, it's not using symbolId.
        }
        Document::Ptr doc;
        const QString unpreprocessedSource = getSource(fileName, workingCopy);

        if (symbolDocument && fileName == symbolDocument->fileName()) {
            doc = symbolDocument;
        } else {
            doc = snapshot.documentFromSource(
                        snapshot.preprocessedCode(unpreprocessedSource, fileName), fileName);
            doc->tokenize();
        }

        Control *control = doc->control();
        if (control->findIdentifier(symbolId->chars(), symbolId->size()) != 0) {
            if (doc != symbolDocument)
                doc->check();

            FindUsages process(unpreprocessedSource.toUtf8(), doc, snapshot);
            process(symbol);

            QList<Usage> lst = process.usages();
            foreach(Usage u,lst){
                Occurrence o;
                o.column = u.col;
                o.length = u.len;
                o.lineNumber = u.line;
                o.path = u.path;
                o.doc =doc;
                o.symbol = symbol;
                o.snapshot = snapshot;
                occurrences.append(o);
            }
        }

        return occurrences;
    }
};

} // end private namespace

class UpdateUI: public std::binary_function<QList<Occurrence> &, QList<Occurrence>, void>
{
    QFutureInterface<Occurrence> *future;

public:
    UpdateUI(QFutureInterface<Occurrence> *future): future(future) {}

    void operator()(QList<Occurrence> &, const QList<Occurrence> &occurrences)
    {
        foreach (const Occurrence &u, occurrences)
            future->reportResult(u);

        future->setProgressValue(future->progressValue() + 1);
    }
};

static Occurrence find_helper(QFutureInterface<Occurrence> &futureInterface,
                              const CppModelManagerInterface::WorkingCopy workingCopy,
                              Refactoring *findRefs,
                              Refactoring::RefactoringOperation *operation)
{
    Symbol* symbol = operation->symbol;
    const Identifier *symbolId = symbol->identifier();
    const Snapshot snapshot = operation->context.snapshot();

    const QString sourceFile = QString::fromUtf8(symbol->fileName(), symbol->fileNameLength());
    QStringList files(sourceFile);

    if (symbol->isClass() || symbol->isForwardClassDeclaration() || (symbol->enclosingScope() && ! symbol->isStatic() &&
                                                                     symbol->enclosingScope()->isNamespace())) {
        foreach (const Document::Ptr &doc, snapshot) {
            if (doc->fileName() == sourceFile)
                continue;

            Control *control = doc->control();

            if (control->findIdentifier(symbolId->chars(), symbolId->size()))
                files.append(doc->fileName());
        }
    } else {
        DependencyTable dependencyTable = findRefs->updateDependencyTable(snapshot);
        files += dependencyTable.filesDependingOn(sourceFile);
    }
    files.removeDuplicates();

    futureInterface.setProgressRange(0, files.size());

    ProcessFile process(workingCopy, snapshot, operation->context.thisDocument(), symbol, &futureInterface);
    UpdateUI reduce(&futureInterface);
    // This thread waits for blockingMappedReduced to finish, so reduce the pool's used thread count
    // so the blockingMappedReduced can use one more thread, and increase it again afterwards.
    QThreadPool::globalInstance()->releaseThread();
    QtConcurrent::blockingMappedReduced<QList<Occurrence> > (files, process, reduce);
    QThreadPool::globalInstance()->reserveThread();
    futureInterface.setProgressValue(files.size());
    futureInterface.reportFinished();
    return Occurrence();
}

void Refactoring::findReferences(RefactoringOperation *op)
{
    const CppModelManagerInterface::WorkingCopy workingCopy = CppModelManagerInterface::instance()->workingCopy();

    QFutureInterface<Occurrence> futureInterface;
    futureInterface.reportStarted();
    QFuture<Occurrence> result = futureInterface.future();
    QtConcurrent::run(&find_helper,futureInterface, workingCopy, this, op);

    createWatcher(result, op);

    Core::ProgressManager *progressManager = Core::ICore::progressManager();
    progressManager->addTask(result, tr("Refactoring"), CppTools::Constants::TASK_SEARCH);
}

void Refactoring::renameSymbol(Symbol *symbol, const QString &newName, LookupContext context)
{
    if (!symbol->identifier())
        return;
    Overview ov;
    if (ov.prettyName(symbol->name()) == newName)
        return;

    RenameOperation *op = new RenameOperation();
    op->context = context;
    op->symbol = symbol;
    op->replacement = newName;

    findReferences(op);
}

void Refactoring::removeSymbol(Symbol *symbol)
{
    RefactoringOperation *op = new RemoveOperation();
    op->context = LookupContext();
    op->symbol = symbol;

    findReferences(op);
}

Declaration * Refactoring::insertDeclarationInto(Class *targetClass, QString documentFileName, const QString &declaration, InsertionPointLocator::AccessSpec access)
{
    CppRefactoringChanges refactoring = CppRefactoringChanges(Snapshot());
    InsertionPointLocator find(refactoring);
    Utils::ChangeSet changeSet;
    CppRefactoringFilePtr file = refactoring.file(documentFileName);
    InsertionLocation loc = find.methodDeclarationInClass(documentFileName, targetClass, access);

    Declaration * decl = NULL;
    if(!loc.isValid())
        qDebug("No insertion point found");
    else
    {
        insertAndIndent(file, &changeSet, loc, declaration);
        file->apply();
        Document::Ptr doc = file->cppDocument();
        Scope *scope = doc->scopeAt(loc.line(),loc.column());
        for(unsigned i=0;i<scope->memberCount();i++)
        {
            Symbol *sym = scope->memberAt(i);
            if (sym->isClass() && sym->hashCode() == targetClass->hashCode())
            {
                scope = sym->asScope();
                i=-1;
            }
            else if(sym->line() == loc.line() && sym->isDeclaration() && declaration.contains(sym->name()->identifier()->chars()))
            {
                decl = sym->asDeclaration();
                break;
            }
        }
    }

    return decl;
}

void Refactoring::insertImplementation(Declaration *decl, const QString &implementation, QString implementationFile)
{
    if (!(decl->type() &&
          decl->type()->isFunctionType() &&
          decl->enclosingScope() &&
          decl->enclosingScope()->isClass()))
        return;

    CppRefactoringChanges refactoring = CppRefactoringChanges(Snapshot());
    InsertionPointLocator find(refactoring);
    QList<InsertionLocation> locs = find.methodDefinition(decl);
    if (locs.size()>1 && implementationFile.isEmpty())
        qWarning("Multiple (%d) insertion points found and no implementation file name given. Choosing first!", locs.size());
    foreach(const InsertionLocation&loc, locs)
    {
        if (loc.fileName() == implementationFile && loc.isValid())
            insertAndIndent(refactoring.file(loc.fileName()), decl, loc, implementation);
        return;
    }
    qWarning("No insertion point in implementation file '%s' found", qPrintable(implementationFile));
}

void Refactoring::insertAndIndent(const CppRefactoringFilePtr &targetFile, Utils::ChangeSet *changeSet, const InsertionLocation &loc, const QString &text)
{
    int targetPosition1 = targetFile->position(loc.line(), loc.column());
    int targetPosition2 = qMax(0, targetFile->position(loc.line(), 1) - 1);
    changeSet->insert(targetPosition1, loc.prefix() + text + loc.suffix());
    targetFile->setChangeSet(*changeSet);
    targetFile->appendIndentRange(Utils::ChangeSet::Range(targetPosition2, targetPosition1));
    targetFile->apply();
}

void Refactoring::insertAndIndent(const CppRefactoringFilePtr &targetFile, Declaration* decl, const InsertionLocation &loc, const QString &implementation)
{
    // shamelessly copied from "cppinsertdecldef.cpp" class InsertDefOperation
    Overview oo;
    oo.setShowFunctionSignatures(true);
    oo.setShowReturnTypes(true);
    oo.setShowArgumentNames(true);

    // make target lookup context
    Document::Ptr targetDoc = targetFile->cppDocument();
    Scope *targetScope = targetDoc->scopeAt(loc.line(), loc.column());
    LookupContext targetContext(targetDoc, Snapshot());
    ClassOrNamespace *targetCoN = targetContext.lookupType(targetScope);
    if (!targetCoN)
        targetCoN = targetContext.globalNamespace();

    // setup rewriting to get minimally qualified names
    SubstitutionEnvironment env;
    env.setContext(targetContext);
    env.switchScope(decl->enclosingScope());
    UseMinimalNames q(targetCoN);
    env.enter(&q);
    Control *control = targetContext.control().data();

    // rewrite the function type
    FullySpecifiedType tn = rewriteType(decl->type(), &env, control);

    // rewrite the function name
    QString name = oo(LookupContext::minimalName(decl, targetCoN, control));

    QString defText = oo.prettyType(tn, name) + implementation;

    int targetPos = targetFile->position(loc.line(), loc.column());
    int targetPos2 = qMax(0, targetFile->position(loc.line(), 1) - 1);

    Utils::ChangeSet target;
    target.insert(targetPos,  loc.prefix() + defText + loc.suffix());
    targetFile->setChangeSet(target);
    targetFile->appendIndentRange(Utils::ChangeSet::Range(targetPos2, targetPos));
    targetFile->apply();
}

void Refactoring::createWatcher(const QFuture<Occurrence> &future, RefactoringOperation *operation)
{
    QFutureWatcher<Occurrence> *watcher = new QFutureWatcher<Occurrence>();
    watcher->setPendingResultsLimit(1);
    connect(watcher, SIGNAL(resultsReadyAt(int,int)), this, SLOT(resultsReadyAt(int,int)));
    connect(watcher, SIGNAL(finished()), this, SLOT(searchFinished()));
    watchers.insert(watcher, operation);
    watcher->setFuture(future);
}

void Refactoring::setDependencyTable(const DependencyTable &newTable)
{
    QMutexLocker locker(&m_depsLock);
    Q_UNUSED(locker);
    deps = newTable;
}


DependencyTable Refactoring::updateDependencyTable(CPlusPlus::Snapshot snapshot)
{
    DependencyTable oldDeps = dependencyTable();
    if (oldDeps.isValidFor(snapshot))
        return oldDeps;

    DependencyTable newDeps;
    newDeps.build(snapshot);
    setDependencyTable(newDeps);
    return newDeps;
}

DependencyTable Refactoring::dependencyTable() const
{
    QMutexLocker locker(&m_depsLock);
    Q_UNUSED(locker);
    return deps;
}

void Refactoring::resultsReadyAt(int first, int last)
{
    QFutureWatcher<Occurrence> *watcher = static_cast<QFutureWatcher<Occurrence> *>(sender());
    RefactoringOperation *operation = watchers[watcher];
    for (int index = first; index != last; ++index) {
        operation->occurences.append(watcher->future().resultAt(index));
    }
}

void Refactoring::updateChangedFiles()
{
    QMutexLocker lock(&filenamesLock);
    if (!changedFilenames.isEmpty()) {
        CppModelManagerInterface::instance()->updateSourceFiles(changedFilenames);
        changedFilenames.clear();
    }
}

void Refactoring::searchFinished()
{
    QFutureWatcher<Occurrence> *watcher = static_cast<QFutureWatcher<Occurrence> *>(sender());
    RefactoringOperation *operation= watchers.value(watcher);
    QStringList lst = doChange(operation);
    {
        QMutexLocker lock(&filenamesLock);
        changedFilenames.append(lst);
    }
    watchers.remove(watcher);
    delete operation;
}

QStringList Refactoring::doChange(Refactoring::RefactoringOperation *operation)
{
    TextEditor::RefactoringChanges refactoring;
    QHash<QString, QList<Occurrence> > changes;
    foreach (const Occurrence &item, operation->occurences)
        changes[item.path].append(item);

    QHashIterator<QString, QList<Occurrence> > it(changes);
    while (it.hasNext()) {
        it.next();
        const QString fileName = it.key();
        const QList<Occurrence> changeItems = it.value();

        Utils::ChangeSet changeSet;
        TextEditor::RefactoringFilePtr file = refactoring.file(fileName);
        QSet<QPair<int, int> > processed;
        foreach (const Occurrence &item, changeItems) {
            const QPair<int, int> &pair = qMakePair(item.lineNumber, item.column);
            if (processed.contains(pair))
                continue;
            processed.insert(pair);

            (*operation)(changeSet, file, item);
        }
        file->setChangeSet(changeSet);
        file->apply();
    }

    return changes.keys();
}

void Refactoring::RenameOperation::operator ()(Utils::ChangeSet &changeSet, TextEditor::RefactoringFilePtr file, const Occurrence &item)
{
    const int start = file->position(item.lineNumber, item.column + 1);
    const int end = file->position(item.lineNumber, item.column + item.length + 1);
    changeSet.replace(start, end, replacement);
}

void Refactoring::RemoveOperation::operator()(Utils::ChangeSet &changeSet, TextEditor::RefactoringFilePtr file, const Occurrence &item)
{
    if (file.isNull())
        return;

    int start = -1;
    int end = -1;
    Symbol *sym = item.symbol;
    TranslationUnit *tu = item.doc->translationUnit();
    if (sym->isScope())
    {
        Scope *scope = sym->asScope();
        end = scope->endOffset();
        if (scope->isClass())
        {
            // find the beginning 'class' Token
            // search backwards
            for(int i=sym->sourceLocation();i>=0;i--)
            {
                const Token &tok =tu->tokenAt( i );
                if (tok.is(T_CLASS))
                {
                    start = tok.begin()-1;
                    break;
                }
            }
        }
    }
    else if (sym->isDeclaration())
    {
        /**\todo This does not cover all corner cases!!! */
        const Token &prevTok = tu->tokenAt( sym->sourceLocation()-1 );
        unsigned index = sym->sourceLocation()+1;
        while(tu->tokenKind(index) != T_SEMICOLON && index < tu->tokenCount())
            index++;
        const Token &nextTok = tu->tokenAt( index );
        start = prevTok.begin()-1;
        end = nextTok.begin();
        qDebug("token at %d: remove from %d to %d", tu->tokenAt(sym->sourceLocation()).begin(), start, end);
    }

    if (start > 0 && end > 0)
    {
        changeSet.remove(start,end);
    }
}
