#include "modelclass.h"
#include "refactoring.h"
#include <coreplugin/icore.h>
#include <coreplugin/settingsdatabase.h>
#include <projectexplorer/projectexplorer.h>
#include <View/UMLItems/UMLClass/umlclass.h>

using namespace UMLView::Model;

ModelClass::ModelClass(Class *_cls)
    :cls(_cls),
      uiElement(NULL)
{
    declarationFile = QString::fromUtf8(cls->fileName(), cls->fileNameLength());


    QString key = "UML/Layout/" + fullQualifiedName();
    pos = ProjectExplorer::ProjectExplorerPlugin::instance()->currentProject()->namedSettings(key + "/position").toPointF();
    userPos = ProjectExplorer::ProjectExplorerPlugin::instance()->currentProject()->namedSettings(key + "/userPosition").toPointF();
}

ModelClass::~ModelClass()
{
    uiElement->removeClass();
}

void ModelClass::update(Class *_cls){
    cls = _cls;
    delayLoad();
    if (uiElement)
    {
        uiElement->updateContent();
    }
}

const ModelClassAttribute::Ptr ModelClass::attribute(QString name)
{
    for(int i=0;i<clsAttributes.size();i++) {
        if (clsAttributes[i]->name == name)
            return clsAttributes[i];
    }

    return ModelClassAttribute::Ptr(NULL);
}


const ModelClassMethod::Ptr ModelClass::method(QString name)
{

    for(int i=0;i<clsMethods.size();i++){
        if (clsMethods[i]->name == name)
            return clsMethods[i];
    }

    return ModelClassMethod::Ptr();
}

Namespaces ModelClass::namespaces()
{
    return nspaces;
}

Attributes ModelClass::attributes()
{
    return clsAttributes;
}

Methods ModelClass::methods()
{
    return clsMethods;
}

const SuperClasses ModelClass::superClasses()
{
    return clsSuperClasses;
}

void ModelClass::addMethodImplementation(Function *fn)
{
    QString filePath = QString::fromUtf8(fn->fileName(),fn->fileNameLength());
    if (!implementationFiles.contains(filePath))
        implementationFiles.append(filePath);
    clsImplementations.append(ModelClassMethod::create(fn));
}

void ModelClass::addMethod(Declaration *decl)
{
    clsMethods.append( ModelClassMethod::create(decl));
    uiElement->updateMethods();
}

void ModelClass::addAttribute(Declaration *decl)
{
    clsAttributes.append( ModelClassAttribute::create(decl) );
    uiElement->updateAttributes();
}

void ModelClass::delayLoad()
{
    nspaces.clear();
    clsMethods.clear();
    clsAttributes.clear();
    clsSuperClasses.clear();
}

void ModelClass::delayLoadMembers()
{
    clsMethods.clear();
    clsAttributes.clear();
    for(uint i=0;i<cls->memberCount();i++)
    {
        Symbol *member = cls->memberAt(i);
        if(member->isDeclaration())
        {
            Declaration *decl = member->asDeclaration();
            clsAttributes.append(ModelClassAttribute::create(decl));
        }
        else if (member->isFunction())
            clsMethods.append(ModelClassMethod::create(member->asFunction()));
        else
            qDebug() << "unkown member" << UMLModel::symbolToFQN(member);
    }
}

void ModelClass::delayLoadNamespaces()
{
    Symbol *sym = cls;
    while(sym->enclosingNamespace()!=NULL)
    {
        ModelNamespace::Ptr ns = ModelNamespace::create(sym->enclosingNamespace());
        nspaces.prepend(ns);
        sym = sym->enclosingNamespace();
    }
}

void ModelClass::delayLoadSuperClasses()
{
    for(uint i=0;i<cls->baseClassCount();i++)
    {
        ModelClass *superCls = UMLModel::instance()->findClassBySymbol( cls->baseClassAt(i) );
        if(superCls!=NULL)
            clsSuperClasses.append(superCls);
        else
            qWarning("required super class '%s' for class '%s' was not loaded!",
                     qPrintable(UMLModel::symbolToFQN(cls->baseClassAt(i))),
                     qPrintable(UMLModel::symbolToFQN(cls)));
    }
}

void ModelClass::setClassName(const QString &name)
{
    if (className() == name)
        return;
    Refactoring::instance()->renameSymbol(cls, name);
}

void ModelClass::setPosition(QPointF &point)
{
    pos=point;
    emit onPositionChanged(pos);

    QString setting = "UML/Layout/" + fullQualifiedName() + "/position";
    QVariant value = qVariantFromValue(pos);
    ProjectExplorer::ProjectExplorerPlugin::instance()->currentProject()->setNamedSettings(setting, value);
}

void ModelClass::setUserPosition(QPointF &point)
{
    userPos=point;
    emit onUserPositionChanged(pos);

    QString setting = "UML/Layout/" + fullQualifiedName() + "/userPosition";
    QVariant value = qVariantFromValue(pos);
    ProjectExplorer::ProjectExplorerPlugin::instance()->currentProject()->setNamedSettings(setting, value);
}

void ModelClass::addAttribute(QString attribString, Visibility vis)
{
    Refactoring::instance()->insertDeclarationInto(cls, declarationFile, attribString, (InsertionPointLocator::AccessSpec)vis);
}

void ModelClass::addMethod(QString methodString, Visibility vis, QString implementation, QString implementationFile)
{
    Declaration * decl =Refactoring::instance()->insertDeclarationInto(cls,declarationFile, methodString, (InsertionPointLocator::AccessSpec)vis);
    // TODO: Fix this!
    //    if (decl)
    //        Refactoring::instance()->insertImplementation(decl, implementation, implementationFile);
}

void ModelClass::addSuperclass(QString className, Visibility vis)
{
}

void ModelClass::removeAttribute(ModelClassAttribute::Ptr attr)
{
    qDebug() << "remove " << attr->toString();
    Refactoring::instance()->removeSymbol(attr->declaration);
}

void ModelClass::removeMethod(ModelClassMethod::Ptr function)
{
    qDebug() << "remove " << function->toString();
    if (function->isDeclaration())
        Refactoring::instance()->removeSymbol(function->declaration);
    else
        Refactoring::instance()->removeSymbol(function->function);
}

void ModelClass::removeSuperclass(QString className)
{
}
