#ifndef MODELCLASS_H
#define MODELCLASS_H

#include "pch.h"
#include "umlmodel.h"
#include "modelclasses.h"

class UMLClass;
using namespace CPlusPlus;
namespace UMLView {
namespace Model {

class ModelClass: public QObject
{
    friend class UMLModel;
    friend class ModelDataStore;
    Q_OBJECT
    Q_PROPERTY(QString className READ className WRITE setClassName)
    Q_PROPERTY(QPointF userPosition READ userPosition WRITE setUserPosition NOTIFY onUserPositionChanged)
    Q_PROPERTY(QPointF position READ position WRITE setPosition NOTIFY onPositionChanged)
    Q_PROPERTY(Namespaces namespaces READ namespaces)
    Q_PROPERTY(Attributes attributes READ attributes)
    Q_PROPERTY(Methods methods READ methods)
    Q_PROPERTY(SuperClasses superClasses READ superClasses)

public:
    ModelClass(Class *cls);
    virtual ~ModelClass();

    const ModelClassAttribute::Ptr attribute(int i)const {return clsAttributes[i];}
    const ModelClassAttribute::Ptr attribute(QString name);

    const ModelClassMethod::Ptr method(int i)const {return clsMethods[i];}
    // With overloaded methods you never know what you get from this call.
    const ModelClassMethod::Ptr method(QString name);

    const SuperClasses superClasses();
    void setUiElement(UMLClass * uiElem){uiElement = uiElem;}
    UMLClass* getUiElement(){return uiElement;}

public slots:
    QString fullQualifiedName()const{ return UMLModel::symbolToFQN(cls);}
    QString className()const {return (clsName.isEmpty())?UMLModel::nameToString(cls->name()):clsName;}
    QPointF position()const{ return pos;}
    QPointF userPosition()const{ return userPos;}
    Namespaces namespaces();
    Attributes attributes();
    Methods methods();

    void setClassName(const QString &name);
    void setPosition(QPointF &point);
    void setUserPosition(QPointF &point);

    void addAttribute(QString attribString, Visibility vis=Private);
    void addMethod(QString methodString, Visibility vis=Public, QString implementation="", QString implementationFile="");
    void addSuperclass(QString className, Visibility vis=Public);
    void removeAttribute(ModelClassAttribute::Ptr attr);
    void removeMethod(ModelClassMethod::Ptr function);
    void removeSuperclass(QString className);

signals:
    void onPositionChanged(const QPointF &pos);
    void onUserPositionChanged(const QPointF &pos);

private:
    void update(Class *cls = NULL);
    void addMethodImplementation(Function* fn);
    void addMethod(Declaration *decl);
    void addAttribute(Declaration *decl);

    void delayLoad();
    void delayLoadMembers();
    void delayLoadNamespaces();
    void delayLoadSuperClasses();
    Class *cls;
    QString clsName;
    QString declarationFile;
    QStringList implementationFiles;
    Namespaces nspaces;
    Attributes clsAttributes;
    Methods clsMethods;
    Methods clsImplementations;
    SuperClasses clsSuperClasses;
    QPointF pos;
    QPointF userPos;
    UMLClass *uiElement;
};

}} // end namespaces
Q_DECLARE_METATYPE(UMLView::Model::ModelClass*)

#endif // MODELCLASS_H
