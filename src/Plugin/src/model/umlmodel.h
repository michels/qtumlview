#ifndef UMLVIEWSCENE_H
#define UMLVIEWSCENE_H

#include "pch.h"
#include "modeldatastore.h"

using namespace CPlusPlus;
namespace UMLView {
namespace Model {

class ModelClass;
class Refactoring;

class UMLModel : public QObject, public CPlusPlus::SymbolVisitor
{
    Q_OBJECT
public:
    static UMLModel * instance();
    virtual ~UMLModel();
    void clear();

    static QString nameToString(const CPlusPlus::Name *name);
    static QString symbolToFQN(const CPlusPlus::Symbol *sym);
    static bool fileBelongsToCurrentProject(QString fileName);

    ModelClass* findClassBySymbol(Symbol*sym);
public slots:
    bool removeFiles(const QStringList& headers, const QStringList &sources, bool removeFromFilesystem=true);
    bool removeClass(ModelClass* cls, bool removeFromFilesystem=true);
    void onDocumentUpdated(CPlusPlus::Document::Ptr doc);
    void onAboutToRemoveFiles(QStringList files);

signals:
    void newClass(UMLView::Model::ModelClass *cls);

private:
    explicit UMLModel(QObject *parent = 0);
    bool visit(CPlusPlus::Class *cls);
    bool visit(CPlusPlus::Function *cls);
    bool visit(CPlusPlus::Namespace *symbol);
    bool visit(CPlusPlus::Declaration *decl);

    static CPlusPlus::Overview overview;
    static UMLModel *_instance;
    ModelDataStore dataStore;
    Refactoring *refactoring;
    QMutex documentUpdateMutex;
};

}} // end namespaces
#endif // UMLVIEWSCENE_H
