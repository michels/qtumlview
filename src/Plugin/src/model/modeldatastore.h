#ifndef MODELDATASTORE_H
#define MODELDATASTORE_H

#include "pch.h"

namespace UMLView {
namespace Model {
class ModelClass;

class ModelDataStore
{
    friend class UMLModel;
public:
    ModelDataStore();

    ModelClass *findClassBySymbol(CPlusPlus::Symbol *symbol);
    ModelClass* findClass(CPlusPlus::Class *cls);
    ModelClass* addClass(CPlusPlus::Class *cls);
    void removeClass(ModelClass *cls, bool actuallyDeleteTheClass = true);
    void removeClassesInFile(QString file,  bool actuallyDeleteTheClass = false);

    void addImplementation(CPlusPlus::Function*fn);
    void addDeclaration(CPlusPlus::Declaration*decl);

    bool thisIsTheOnlyClassInItsFile(ModelClass *cls);

    void clear();
    bool isEmpty(){return classesByFileName.isEmpty();}
private:
    QMap<unsigned ,ModelClass*> classesByHash;
    QMap<QString /*filename */, QMap<QString /*className*/, ModelClass*> > classesByFileName;
    QMap<QString /*className*/, ModelClass*> classesByName;
    CPlusPlus::Function *mainFunction;
};

}}//end namespaces

#endif // MODELDATASTORE_H
