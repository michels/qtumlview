#ifndef REFACTORING_H
#define REFACTORING_H

#include <QObject>
#include "pch.h"

namespace CPlusPlus {
class Symbol;
class LookupContext;
}

using namespace CPlusPlus;
namespace UMLView {
namespace Model {

class Occurrence {
public:
    QString path;
    int lineNumber;
    int length;
    int column;
    Symbol *symbol;
    Document::Ptr doc;
    Snapshot snapshot;
};

class Refactoring : public QObject
{
    Q_OBJECT
public:
    class RefactoringOperation {
    public:
        virtual ~RefactoringOperation(){}
        Symbol *symbol;
        LookupContext context;
        QList<Occurrence> occurences;
        enum Type {
            Rename,
            Remove,
            Insert
        };
        virtual Type type() = 0;
        virtual void operator()(Utils::ChangeSet &changeSet, TextEditor::RefactoringFilePtr file, const Occurrence &item)=0;
    };
    class RenameOperation: public RefactoringOperation {
    public:
        QString replacement;
        Type type(){ return Rename;}
        void operator ()(Utils::ChangeSet &changeSet, TextEditor::RefactoringFilePtr file, const Occurrence &item);
    };
    class RemoveOperation: public RefactoringOperation {
    public:
        Type type(){ return Remove;}
        void operator ()(Utils::ChangeSet &changeSet, TextEditor::RefactoringFilePtr file, const Occurrence &item);
        void startOfPrevious(Symbol *sym, TranslationUnit *tu, int start);
    };

public:
    static Refactoring* instance(){
        if (_instance == NULL)
            _instance=new Refactoring();
        return _instance;
    }
    void renameSymbol(Symbol *symbol, const QString &newName, LookupContext ctx= LookupContext ());
    void removeSymbol(Symbol *symbol);
    Declaration *insertDeclarationInto(Class *targetClass, QString documentFileName, const QString &declaration, InsertionPointLocator::AccessSpec access);
    void insertImplementation(Declaration *decl, const QString& implementation="", QString implementationFile="");

    DependencyTable updateDependencyTable(CPlusPlus::Snapshot snapshot);

    void findReferences(RefactoringOperation *op);
    void updateChangedFiles();
private Q_SLOTS:
    void searchFinished();
    void resultsReadyAt(int first, int last);

private:
    void insertAndIndent(const CppRefactoringFilePtr &file, Utils::ChangeSet *changeSet, const InsertionLocation &loc, const QString &text);
    void insertAndIndent(const CppRefactoringFilePtr &targetFile, Declaration* decl, const InsertionLocation &loc, const QString &implementation);
    QStringList doChange(RefactoringOperation *operation);
    void findAll_helper(Find::SearchResult *search);
    void createWatcher(const QFuture<Occurrence> &future, RefactoringOperation *operation);
    void setDependencyTable(const CPlusPlus::DependencyTable &newTable);
    DependencyTable dependencyTable() const;

private:
    explicit Refactoring(QObject *parent = 0):QObject(parent){}
    QMap<QFutureWatcher<Occurrence> *, RefactoringOperation*> watchers;

    mutable QMutex m_depsLock;
    CPlusPlus::DependencyTable deps;
    mutable QMutex filenamesLock;
    QStringList changedFilenames;
    
    static Refactoring *_instance;
};
}} // end namespaces
#endif // REFACTORING_H
