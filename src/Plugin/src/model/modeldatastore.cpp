#include "modeldatastore.h"
#include "umlmodel.h"
#include "modelclass.h"
#include <Icons.h>

using namespace UMLView::Model;

ModelDataStore::ModelDataStore()
{
}

ModelClass *ModelDataStore::findClassBySymbol(Symbol *symbol)
{
    if (classesByHash.contains(symbol->hashCode()))
        return classesByHash.value(symbol->hashCode());
    else
        return NULL;
}

void ModelDataStore::clear()
{
    while(!classesByHash.isEmpty())
    {
        QMap<unsigned, ModelClass*>::Iterator it= classesByHash.begin();
        delete it.value();
        classesByHash.remove(it.key());
        classesByFileName.clear();
        classesByName.clear();
    }
}


ModelClass *ModelDataStore::findClass(Class *cls)
{
    if (classesByHash.contains(cls->hashCode()))
    {
        return classesByHash[cls->hashCode()];
    }
    else
    {
        QString fileName = QString::fromUtf8(cls->fileName(), cls->fileNameLength());
        QString clsName = UMLModel::symbolToFQN(cls);
        if (classesByFileName.contains(fileName) && classesByFileName[fileName].contains(clsName))
            return classesByFileName[fileName][clsName];
    }
    return NULL;
}

ModelClass* ModelDataStore::addClass(CPlusPlus::Class *cls)
{
    QString fileName = QString::fromUtf8(cls->fileName(), cls->fileNameLength());

    if (UMLModel::fileBelongsToCurrentProject(fileName))
    {
        ModelClass *mdlClass = new ModelClass(cls);
        classesByHash.insert(mdlClass->cls->hashCode(),mdlClass);
        classesByFileName[fileName].insert(mdlClass->fullQualifiedName(), mdlClass);
        classesByName.insert(mdlClass->fullQualifiedName(), mdlClass);
        qDebug() << "class added: \t"  << mdlClass->fullQualifiedName();
        return mdlClass;
    }
    return NULL;
}


void ModelDataStore::removeClass(ModelClass *cls, bool actuallyDeleteTheClass)
{
    classesByHash.remove(cls->cls->hashCode());
    classesByName.remove(cls->fullQualifiedName());
    classesByFileName[cls->declarationFile].remove(cls->fullQualifiedName());
    if (classesByFileName[cls->declarationFile].isEmpty())
        classesByFileName.remove(cls->declarationFile);
    qDebug() << "class removed:\t"<<cls->fullQualifiedName();
    if (actuallyDeleteTheClass)
        cls->deleteLater();
}

void ModelDataStore::removeClassesInFile(QString file, bool actuallyDeleteTheClass)
{
    if (classesByFileName.contains(file))
    {
        for(int i=0;i<classesByFileName[file].count();i++)
        {
            ModelClass *cls = classesByFileName[file].end().value();
            removeClass(cls, actuallyDeleteTheClass);
        }
    }
}

void ModelDataStore::addImplementation(CPlusPlus::Function *fn)
{
    QString clsName = UMLModel::nameToString( fn->name()->asQualifiedNameId()->base() );
    if (classesByName.contains(clsName))
    {
        classesByName[clsName]->addMethodImplementation(fn);
    }
}

void ModelDataStore::addDeclaration(Declaration *decl)
{
    if (classesByHash.contains(decl->enclosingClass()->hashCode()))
    {
        if (decl->isDeclaration() && decl->type() && decl->type()->isFunctionType())
            classesByHash[decl->enclosingClass()->hashCode()]->addMethod(decl);
        else
            classesByHash[decl->enclosingClass()->hashCode()]->addAttribute(decl);
    }
}

bool ModelDataStore::thisIsTheOnlyClassInItsFile(ModelClass *cls)
{
    bool singleClassInThisFile=true;

    // search the declaration files
    if (classesByFileName.contains(cls->declarationFile))
        singleClassInThisFile = classesByFileName[cls->declarationFile].count() == 1;
    else
    { // Search the implementations
        foreach(QString implFile, cls->implementationFiles)
            singleClassInThisFile &= classesByFileName[implFile].count() == 1;
    }
    if (singleClassInThisFile)
    {
        QSet<QString> implementationFiles = cls->implementationFiles.toSet();
        foreach(ModelClass*mdlCls, classesByHash)
        {
            if (mdlCls != cls && !mdlCls->implementationFiles.toSet().intersect(implementationFiles).isEmpty())
                return false;
        }

    }
    return singleClassInThisFile;
}


