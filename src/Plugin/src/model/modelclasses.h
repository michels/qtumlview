#ifndef MODELCLASSES_H
#define MODELCLASSES_H

#include "pch.h"

using namespace CPlusPlus;
namespace UMLView {
namespace Model {
class ModelClass;

enum Visibility {
    Invalid = InsertionPointLocator::Invalid,
    Signals = InsertionPointLocator::Signals,

    Public = InsertionPointLocator::Public,
    Protected = InsertionPointLocator::Protected,
    Private = InsertionPointLocator::Private,

    SlotBit = InsertionPointLocator::SlotBit,

    PublicSlot    = InsertionPointLocator::PublicSlot,
    ProtectedSlot = InsertionPointLocator::ProtectedSlot,
    PrivateSlot   = InsertionPointLocator::PrivateSlot
};

enum MethodKey {
    NormalMethod,
    SlotMethod,
    SignalMethod,
    InvokableMethod
};

class ModelArgument {
public:
    QString name;
    QString toString()const;
};
typedef QList<ModelArgument> ModelArguments;

class ModelClassAttribute {
    friend class ModelClass;
public:
    typedef QSharedPointer<ModelClassAttribute> Ptr;
    static ModelClassAttribute::Ptr create(Declaration* decl);

    Visibility visibility()const{return _visibility;}
    QString toString()const;
    QIcon icon()const;
private:
    Declaration* declaration;
    QString name;
    Visibility _visibility;
};


class ModelClassMethod {
    friend class ModelClass;
public:
    typedef QSharedPointer<ModelClassMethod> Ptr;
    static ModelClassMethod::Ptr create(Function* fn);
    static ModelClassMethod::Ptr create(Declaration* decl);

    bool isDeclaration(){return _isDeclaration;}
    bool isImplementation(){return !_isDeclaration;}
    Visibility visibility()const{return _visibility;}
    MethodKey methodKey()const{return _methodKey;}
    QString toString()const;
    QIcon icon()const;
private:
    QString name;
    union {
        Function* function;
        Declaration* declaration;
    };
    bool _isDeclaration;
    ModelArguments arguments;
    Visibility _visibility;
    MethodKey _methodKey;
};

class ModelNamespace {
    friend class ModelClass;
public:
    typedef QSharedPointer<ModelNamespace> Ptr;
    static ModelNamespace::Ptr create(Namespace* ns);

private:
    QString name;
    Namespace* nspace;
};

typedef QList<ModelClassAttribute::Ptr> Attributes;
typedef QList<ModelClassMethod::Ptr> Methods;
typedef QList<ModelNamespace::Ptr> Namespaces;
typedef QList<UMLView::Model::ModelClass*> SuperClasses;


}} // end namespaces

Q_DECLARE_METATYPE(UMLView::Model::ModelClassMethod::Ptr)
Q_DECLARE_METATYPE(UMLView::Model::ModelClassAttribute::Ptr)
Q_DECLARE_METATYPE(UMLView::Model::ModelNamespace::Ptr)

#endif // MODELCLASSES_H
