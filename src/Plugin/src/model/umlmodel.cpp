
#include "umlmodel.h"
#include "modelclass.h"
#include "refactoring.h"
#include <projectexplorer/projectexplorer.h>
#include <projectexplorer/projectnodes.h>
#include <coreplugin/vcsmanager.h>
#include <coreplugin/icore.h>

using namespace UMLView::Model;
using namespace ProjectExplorer;

CPlusPlus::Overview UMLModel::overview;
UMLModel *UMLModel::_instance;

UMLModel *UMLModel::instance()
{
    if (_instance == NULL)
    {
        _instance = new UMLModel();
    }
    return _instance;
}

UMLModel::UMLModel(QObject *parent) :
    QObject(parent)
{
    refactoring = Refactoring::instance();
}

UMLModel::~UMLModel()
{
}

void UMLModel::onDocumentUpdated(CPlusPlus::Document::Ptr doc)
{
    if (doc.isNull())
        return;
    if (!fileBelongsToCurrentProject(doc->fileName()))
        return;

    if (!doc->isParsed())
        return; // skip that document

    if (!doc->diagnosticMessages().empty())
    { // there are errors
        foreach(Document::DiagnosticMessage msg, doc->diagnosticMessages())
        {
            if (msg.level()== Document::DiagnosticMessage::Error ||
                    msg.level() == Document::DiagnosticMessage::Fatal)
                return;
        }
    }

    QMutexLocker lock(&documentUpdateMutex);
    for (unsigned i = 0; i < doc->globalSymbolCount(); ++i)
    {
        CPlusPlus::Symbol *sym = doc->globalSymbolAt(i);
        accept(sym);
    }
}

void UMLModel::onAboutToRemoveFiles(QStringList files)
{
    if (files.isEmpty() || dataStore.isEmpty())
        return;
    foreach(QString file, files)
    {
        dataStore.removeClassesInFile(file);
    }
}


void UMLModel::clear()
{
    dataStore.clear();
}

bool UMLModel::visit(CPlusPlus::Namespace *nspace)
{
    for(uint i=0;i<nspace->memberCount();i++)
        accept(nspace->memberAt(i));
    return false;
}

bool UMLModel::visit(CPlusPlus::Class *cls)
{
    if (cls->isForwardClassDeclaration())
        return false;
    if (!fileBelongsToCurrentProject( QString::fromUtf8(cls->fileName(),cls->fileNameLength())))
        return false;

    // TODO: How to detect if the class is beeing renamed currently?
    ModelClass * existingClass = dataStore.findClass(cls);
    if (existingClass == NULL)
    {
        existingClass = dataStore.addClass(cls);
        emit newClass(existingClass);
    }

    existingClass->update(cls);

    for(uint i=0;i<cls->memberCount();i++)
        accept(cls->memberAt(i));
    return false;
}

bool UMLModel::visit(CPlusPlus::Declaration *decl)
{
    if (decl->enclosingClass() != NULL)
    {
        dataStore.addDeclaration(decl);
    }
    return false;
}

bool UMLModel::visit(CPlusPlus::Function *fn)
{
    if (fn->name()->isQualifiedNameId() && fileBelongsToCurrentProject( QString::fromUtf8(fn->fileName(),fn->fileNameLength())))
    {
        dataStore.addImplementation(fn);
    }
    return false;
}

bool UMLModel::fileBelongsToCurrentProject(QString fileName)
{
    return ProjectExplorer::ProjectExplorerPlugin::instance()->currentProject()->files(ProjectExplorer::Project::AllFiles).contains(fileName);
}

ModelClass *UMLModel::findClassBySymbol(Symbol *sym)
{
    return dataStore.findClassBySymbol(sym);
}

bool UMLModel::removeFiles(const QStringList &headers,const QStringList &sources, bool removeFromFilesystem)
{
    qDebug() << "Remove files: " << headers << sources;
    Project *currentProj = ProjectExplorer::ProjectExplorerPlugin::currentProject();
    if (!currentProj->rootProjectNode()->removeFiles(ProjectExplorer::SourceType, sources) ||
            !currentProj->rootProjectNode()->removeFiles(ProjectExplorer::HeaderType, headers) )
        return false;

    bool deleteOK=true;
    if (removeFromFilesystem)
    {
        QStringList allFiles;
        allFiles << headers << sources;
        foreach(QString filePath, allFiles )
        {
            Core::ICore::vcsManager()->promptToDelete(filePath);
            QFile file(filePath);
            if (file.exists()) {
                // could have been deleted by vc
                if (!file.remove()) {
                    QMessageBox::warning(Core::ICore::mainWindow(), tr("Deleting File Failed"),
                                         tr("Could not delete file %1.").arg(filePath));
                    deleteOK = false;
                }
            }
        }
    }
    return deleteOK;
}

bool UMLModel::removeClass(ModelClass *cls, bool removeFromFilesystem)
{
    bool successfull = true;
    if (dataStore.thisIsTheOnlyClassInItsFile(cls)){
        successfull = removeFiles(QStringList() << cls->declarationFile, cls->implementationFiles, removeFromFilesystem);
    }

    if (successfull) // remove remaining occurences
        refactoring->removeSymbol(cls->cls);
    dataStore.removeClass(cls);
    return successfull;
}

QString UMLModel::nameToString(const Name *name)
{
    return overview(name);
}

QString UMLModel::symbolToFQN(const Symbol *sym)
{
    QString clsName = nameToString(sym->name());
    if (sym->enclosingClass())
        clsName = nameToString( sym->enclosingClass()->name() ) + "::" + clsName;
    QString fqn;
    while(sym->enclosingNamespace()!=NULL)
    {
        Namespace *nspace = sym->enclosingNamespace();
        fqn = nameToString(nspace->name()) + "::" + fqn;
        sym = nspace;
    }
    fqn = fqn.remove(0,2) + clsName;
    return fqn;
}

