#include "modelclasses.h"
#include "umlmodel.h"
#include <Icons.h>

using namespace UMLView::Model;

ModelClassAttribute::Ptr ModelClassAttribute::create(Declaration *decl)
{
    ModelClassAttribute::Ptr attrib(new ModelClassAttribute());
    attrib->declaration = decl;
    attrib->name = UMLModel::nameToString(decl->name());
    return attrib;
}


ModelClassMethod::Ptr ModelClassMethod::create(Function *fn)
{
    ModelClassMethod::Ptr method(new ModelClassMethod());
    method->function = fn;
    method->_isDeclaration = false;
    method->name = UMLModel::nameToString(fn->name());
    return method;
}

ModelClassMethod::Ptr ModelClassMethod::create(Declaration *decl)
{
    ModelClassMethod::Ptr method(new ModelClassMethod());
    method->declaration = decl;
    method->_isDeclaration = true;
    method->name = UMLModel::nameToString(decl->name());
    return method;
}


ModelNamespace::Ptr ModelNamespace::create(Namespace *ns)
{
    ModelNamespace::Ptr space(new ModelNamespace());
    space->nspace = ns;
    space->name = UMLModel::nameToString(ns->name());
    return space;
}

QString ModelClassAttribute::toString() const
{
    Overview oo;
    oo.setShowReturnTypes(true);
    if (declaration->name() == NULL)
        return oo(declaration->type());
    else
        return oo(declaration->type(),declaration->name());
}

QIcon ModelClassAttribute::icon() const
{
    return Icons().iconForSymbol(declaration);
}


QString ModelClassMethod::toString() const
{
    Overview oo;
    oo.setShowReturnTypes(true);
    oo.setShowArgumentNames(true);
    oo.setShowFunctionSignatures(true);
    oo.setShowReturnTypes(true);
    oo.setShowTemplateParameters(true);
    if (_isDeclaration)
        return oo.prettyName(declaration->name());
    else
        return oo.prettyName(function->name());
}

QIcon ModelClassMethod::icon() const
{
    if (_isDeclaration)
        return Icons().iconForSymbol(declaration);
    else
        return Icons().iconForSymbol(function);
}
