#include "modelmodule.h"

using namespace UMLView::Model;

ModelModule::ModelModule(QString mName, QObject *parent) :
    QObject(parent),
    modName(mName)
{
}

void ModelModule::addClass(ModelClass *cls)
{
    classes.append(cls);
    emit classAdded(cls);
}

void ModelModule::removeClass(ModelClass *cls)
{
    classes.removeAll(cls);
    emit classRemoved(cls);
}

void ModelModule::clear()
{
    while(!classes.isEmpty())
        removeClass(classes.takeLast());
}
