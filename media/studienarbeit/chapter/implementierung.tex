\section{Implementierung}
\label{sec:Implementierung}
Folgend wird die Software vorgestellt, die für dieses Projekt entwickelt wurde.
Der erste Abschnitt (\ref{subsec:Impl:Realisierung}) beschreibt die Hilfsmittel
zur Realisierung des Plugins. Der zweite Abschnitt
(\ref{subsec:Impl:QtCreatorPlugin}) konzentriert sich eher generell auf das
Schreiben eines Plugins für den Qt-Creator. Dann folgt ein Kapitel
(\ref{subsec:Impl:CodeModell}) über das Codemodell und darüber, wie die
notwendigen Informationen zur Darstellung der Klassen aus dem Qt-Creator
ausgelesen werden können.
Der letzte Teil (\ref{subsec:Impl:View}) beschreibt die grafische Darstellung
der Klassen.


\subsection{Realisierung (M.S)}
\label{subsec:Impl:Realisierung}
Zur Entwicklung des Plugins für den Qt-Creator wird der Qt-Creator selbst als
\ac{IDE} verwendet. Die Entwicklung findet parallel sowohl unter Windows als
auch unter Linux statt. Damit lässt sich die notwendige Plattformunabhängigkeit
bereits zur Entwicklungszeit erreichen. Da das Debuggen unter Windows sehr viel
langsamer ist, wird auf diese Weise auch Entwicklungszeit gespart. Unter Linux,
wie unter Windows, wird die stabile Version 2.5 des Qt-Creator eingesetzt. Dies
ist gleichzeitig auch die Zielplattform. Eine kurze Evaluation der aktuellen
Version 2.6 verursachte Probleme wegen kleiner Abweichungen in dem \ac{API}, 
daher wurde auf einen Wechsel während der Entwicklung verzichtet.

\begin{figure}[H]
\begin{center}
  \includegraphics[width=0.8\textwidth]{jenkins.png}
  \caption{Das \ac{CI}-Tool Jenkins}
  \label{img:Jenkins}
\end{center}
\end{figure}

Als Quellcodeverwaltung kommt Git zum Einsatz. Das Repository ist auf
Bitbucket\footnote{https://bitbucket.org/michels/qtumlview} gehostet. Dort
findet sich auch der Issue-Tracker
\footnote{https://bitbucket.org/michels/qtumlview/issues}.
Als \ac{CI}-Tool wird Jenkins\footnote{http://www.jenkins-ci.org}
(siehe Abbildung \ref{img:Jenkins})verwendet.

\subsection{Qt-Creator Plugin (M.S.)}
\label{subsec:Impl:QtCreatorPlugin}

Der Qt-Creator wurde sehr modular entwickelt. Nahezu jede Funktionalität - vom
Editor bis zum Debugger - ist als Plugin entwickelt (siehe Abbildung
\ref{img:QtCreator:Plugins}). Dieses Konzept erlaubt die einfache Erweiterung um
neue Features.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.8\textwidth]{qt_creator_plugins.png}
\caption{Modularer Aufbau des Qt-Creator mit seinen Plugins}
\label{img:QtCreator:Plugins}
\end{center}
\end{figure}

Da nun ein Plugin auch mit anderen Plugins interagieren muss oder Funktionalität 
von anderen Plugins verwendet, ist die Ladereihenfolge und damit die
Abhängigkeiten der Plugins untereinander sehr wichtig. In der Datei
\emph{UMLView.Pluginspec.in} werden die Abhängigkeiten hinterlegt:

\begin{lstlisting}[caption={Metainformationen
und Abhängigkeiten des Plugins},label={lst:PluginDependencies}]
<plugin name=\"UMLView\" version=\"0.0.2\" compatVersion=\"0.0.1\">
    <vendor>BartschatSprauer</vendor>
    <copyright>(C) Andreas Bartschat, Michael Sprauer</copyright>
    <license>Copyright (c) 2013 Andreas Bartschat, Michael Sprauer
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</license>
    <description>Provides an class editor</description>
    <url>https://bitbucket.org/michels/qtumlview/</url>
    <dependencyList>
        <dependency name=\"Core\" version=\"$$QTCREATOR_VERSION\"/>
        <dependency name=\"ProjectExplorer\" version=\"$$QTCREATOR_VERSION\"/>
        <dependency name=\"CppTools\" version=\"$$QTCREATOR_VERSION\"/>
    </dependencyList>
</plugin>
\end{lstlisting}

Hier sind insbesondere die Zeilen 11 bis 13 interessant. Dort werden die
Abhängigkeiten zu dem \emph{Core-Plugin}, dem \emph{ProjectExplorer-Plugin} und
dem \emph{CppTools-Plugin} definiert. Die Abhängigkeit zum \emph{Core-Plugin} hat
jedes Plugin. Dort ist das Interface für alle Plugins definiert. In
\emph{ProjectExplorer-Plugin} sind die Events hinterlegt, die das Laden einer
Datei anzeigen und das \emph{CppTools-Plugin} stellt letztlich die
Parsingfunktionalität bereit.

Jedes Plugin muss eine definierte Schnittstelle bereitstellen, damit es von der
IDE geladen werden kann:
Es muss das \lstinline$Q_EXPORT_PLUGIN2$ Makro
aufgerufen werden für eine Klasse die von \lstinline$ExtensionSystem::IPlugin$
erbt. Das zwingt zur Implementierung der beiden Methoden \lstinline$initialize$
und \lstinline$extensionsInitialized$. In der ersten Methode wird das
Plugin aufgefordert alle notwendigen Ressourcen zu laden und die sogenannten Hooks
- also die Menüs und Eregnisse auf die das Plugin reagieren soll - zu setzen.
Die zweite Methode teilt dem Plugin mit, dass nun alle anderen Plugins geladen wurden,
die von diesem Plugin abhängen.
In Listing \ref{lst:PluginInit} wird die \lstinline$initialize$ Methode aus diesem
Projekt gezeigt. Die Klasse, die von \lstinline$ExtensionSystem::IPlugin$ erbt
heißt \lstinline$UMLViewPlugin$.

\begin{lstlisting}[caption={Initialisierung des Plugins UMLView},label={lst:PluginInit}]
bool UMLViewPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    Q_UNUSED(errorString)

    addAutoReleasedObject(m_mode = new UMLViewMode());
    addAutoReleasedObject(Refactoring::instance());
    addAutoReleasedObject(UMLModel::instance());

    connectTheSignals();

    return true;
}
\end{lstlisting}

Hier ist zu sehen, dass insgesamt drei Ressourcen allokiert werden. Der Aufruf
der Methode \lstinline$addAutoReleasedObject$, die in
\lstinline$ExtensionSystem::IPlugin$ implementiert ist, sorgt beim Entladen des
Plugins dafür, dass die Objekte automatisch in umgekehrter Reihenfolge wieder
gelöscht werden.
Eregnisse, wie das Laden einer Datei oder das Öffnen eines Projekts teilt die
IDE jedem Plugin über das Signal-Slot-Konzept von Qt mit. Die notwendigen
Ereignisse werden in der Methode \lstinline$connectTheSignals$ verbunden. Das
erfolgreiche Laden des Plugins wird durch den Rückgabewert \lstinline$true$
mitgeteilt.

\begin{figure}[H]
\begin{center}
  \includegraphics[width=0.8\textwidth]{qt_creator.png}
  \caption{Oberfläche des Qt-Creators}
  \label{img:QtCreator}
\end{center}
\end{figure}

In Abbildung \ref{img:QtCreator} ist beispielhaft die Oberfläche des Qt-Creators zu sehen.
Neben der Menüleiste oben sind zwei weitere Bereiche von Interesse: 
\begin{enumerate}
  \item Links die Modi
  \item Unten die Ausgabe und die Suche
\end{enumerate}

Während die Ausgabe- und Suchleiste fast selbsterklärend ist, muss zu den Modi
noch etwas gesagt werden:
Der Qt-Creator versteht die Modi als Sichten auf verschieden Aspekte der
Programmierung. Den Anfang macht eine Wilkommens-Sicht, die zuletzt bearbeitete
Projekte auflistet oder Hilfe zum Umgang mit dem Qt-Creator anbietet, sowie
Codebeispiele und Anleitungen bereithält. Die zweite Sicht, der Edit-Mode, ist
die wichtigste Sicht. Hier wird der Quellcode angezeigt und kann bearbeitet
werden. Im Desinger-Mode - Sicht Nummer 4 - können grafische Oberflächen
grafisch gestaltet werden. Dann folgen noch Sichten für das Debugging, die
Projekteinstellungen, das Analysieren und die Dokumentation der Qt-Bibliothek.

Es ist also naheliegend die Klassendarstellung ebenfalls als Sicht bzw. Mode 
zu implementieren, da auch ein Klassendiagramm eine Sicht auf das Projekt
darstellt. Im Listing \ref{lst:PluginInit} Zeile 6 wird der Mode initialisiert.
Der Mode ist implementiert wie es im folgenden Listing \ref{lst:UMLViewMode} zu
sehen ist:

\begin{lstlisting}[caption={Initialisierung des Modes},label={lst:UMLViewMode}]
class UMLVIEWSHARED_EXPORT UMLViewMode : public IMode
{
    Q_OBJECT
public:
    UMLViewMode(QObject *parent = 0) : IMode(parent)
    {
        setContext(Context(C_EDITORMANAGER, C_UMLVIEWMODE, C_NAVIGATION_PANE));
        setDisplayName(tr("UMLView"));
        setIcon(QIcon(QLatin1String(":/view/Images/uml.png")));
        setPriority(P_MODE_UMLVIEW);
        setId(MODE_UMLVIEW);
        setType(MODE_EDIT_TYPE);
        setEnabled(false);
        m_widget = ui = new QtUmlViewPrototype();
        setWidget(m_widget);
    }
private:
    QtUmlViewPrototype *ui;
};
\end{lstlisting}

\lstinline$UMLViewMode$ erbt von \lstinline$IMode$. Neben dem Anlegen des
Widgets in Zeile 14 und dem Festlegen der Bezeichnung und des Icons in Zeile 9
und 10 ist nicht viel zu tun. Die Defines sind in der Datei
\emph{src/Plugin/src/umlviewconstants.h} zu finden.


\subsection{Code Modell (M.S.)}
\label{subsec:Impl:CodeModell}

Der Qt-Creator stellt im \emph{Singleton} \lstinline$CppModelManagerInterface$
Events bereit, die das Laden einer Datei anzeigen. In der Methode
\lstinline$connectTheSignals$ aus Listing \ref{lst:PluginInit} werden die Events
an den Singleton \lstinline$UMLModel$ weitergeleitet. Dort - genauer in der
Methode \lstinline$onDocumentUpdate$ - werden alle Dateien, die zum aktuellen
Projekt gehören, analysiert. Nach dem \emph{Visitor}-Muster werden alle Symbole
in der Datei besucht. Abhängig von dem jeweiligen Typ des Symbols wird eine der
folgenden Aktionen ausgeführt:

\begin{description}
  \item[Namespace]: Jedes Element des Namespaces wird besucht.
  \item[Declaration]: Wird zur zugehörigen, umgebenden Klasse hinzugefügt. Eine
  	Deklaration kann entweder ein Attribut oder eine Methode sein. Um zwischen
  	Attribut und Methode zu unterscheiden muss die folgende Überprüfung gemacht
  	werden:\\
  		\lstinline$decl->isDeclaration() && decl->type() && decl->type()->isFunctionType()$\\
  	Wenn der Ausdruck zu $true$ evaluiert, dann handelt es sich um eine Methode.
  	Sonst ist es ein Attribut.
  		
  \item[Function]: Wenn \lstinline$fn->name()->isQualifiedNameId() == true$ 
  	ist, also wenn der Name ein \emph{Full-Qualified-Name} ist, wird die Funktion
  	als Implementierung einer Membermethode zu der zugehörigen, umgebenden Klasse
 	hinzugefügt. Welche Klasse das ist, wird aus dem \emph{Full-Qualified-Name}
 	bestimmt.
  \item[Class]: Wird im Codemodel gespeichert. Alle Member-Elemente werden
  	ebenfalls besucht, um verschachtelte Klassen\footnote{nested classes} zu
  	erkennen. Wenn die Methode \lstinline$isForwardClassDeclaration()$ der
  	Klasse \lstinline$true$ liefert, dann wird sie ignoriert.
\end{description}

Der \emph{Full-Qualified-Name} einer Klasse wird zusammengesetzt aus allen
Namespaces, die diese Klasse umgeben, getrennt durch ein :: und dem Namen der
Klasse selbst.

Das Codemodel wird durch die interne Klasse \lstinline$ModelDataStore$
repräsentiert. Jede Klasse, die dort hinzugefügt wird, löst ein Signal aus, das
an den View gesendet wird. Wenn Klassen geändert wurden, wird der View
aufgefordert die Änderungen zu übernehmen. Wie die Klassen dort dargestellt
werden ist in Kapitel \ref{subsec:Impl:View} beschrieben. Um die richtige Klasse
aus allen vorhandenen Klassen schnell zu finden, werden alle Klassen nach drei
verschiedenen Kriterien in assoziativen Hashtables abgelegt. 

\begin{itemize}
  \item Nach Hash: Jedem Symbol - und damit auch jeder Klasse - wird vom
  Qt-Creator ein Hash zugeordnet, der diese Klasse eindeutig identifiziert.
  \item Nach Dateiname und dem \emph{Full-Qualified-Name} der Klasse.
  \item Nach \emph{Full-Qualified-Name}.
\end{itemize}

Änderungen am Codemodell werden in der Klasse \lstinline$ModelClass$
vorgenommen. Diese verwendet den Singleton \lstinline$Refactoring$, um die
Änderunge im Code abzubilden. Hier war die größte Arbeit notwendig. Die Klasse
Refactoring bietet die folgende \ac{API} an:

\begin{lstlisting}[caption={Auszug aus der API der
Refactoring-Klasse},label={lst:Refactoring}] class Refactoring : public QObject
{
    Q_OBJECT
public:
    static Refactoring* instance();
	void renameSymbol(Symbol *symbol, const QString &newName, LookupContext ctx=LookupContext ()); 
	void removeSymbol(Symbol *symbol);
    Declaration *insertDeclarationInto(Class *targetClass, QString documentFileName, const QString &declaration, InsertionPointLocator::AccessSpec access);
    void insertImplementation(Declaration *decl, const QString& implementation="", QString implementationFile="");
};
\end{lstlisting}

Es können alle Symbole umbenannt, gelöscht und eingefügt werden. Die
Umnennen- und Löschenoperation läuft dabei nach einem Schema ab:

\begin{enumerate}
  \item Alle Vorkommen der angegebenen Symbole werden gesucht.
  \item Die entsprechende Operation wird auf jedes Symbol angewendet.
\end{enumerate}

Da das Suchen und das Ersetzen bzw. Löschen auf mehrere Kerne verteilt wird, ist
hier eine überraschend gute Performance zu erwarten. 

Die Einfügeoperation ruft eine Funktionalität des Qt-Creators auf: Den
\lstinline$InsertionPointLocator$. Dieser sucht für den angegebenen Zweck einen
passenden Punkt zum Einfügen. Unter Umständen können mehrere Punkte/Dateien
gefunden werden, in die das neue Symbol eingefügt werden kann. Dann wird
automatisch der erste Eintrag ausgewählt.

\subsection{Klassendiagramm (A.B.)}
\label{subsec:Impl:Klassendiagramm}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.95\textwidth]{UMLClassDiagram.pdf}
\caption{Klassendiagramm der erstellten Software}
\label{img:UMLClassDiagram}
\end{center}
\end{figure}
Abbildung \ref{img:UMLClassDiagram} zeigt das Klassendiagramm der entwickelten
Software. Zur Steigerung der Übersichtlichkeit sind nur die Klassennamen und
wichtigsten Verbindungen zu sehen. Das Diagramm ist in die zwei Teile Model und
View eingeteilt. Die oberste Zeile zeigt die Klassen \lstinline$UmlViewPlugin$,
\lstinline$UmlViewMode$ und \lstinline$QtUmlViewPrototype$, die zur
Initialisierung des Plugins dienen. Die Klasse \lstinline$UmlModel$ bildet die
Schnittstelle zum Qt-Creator. Sie organisiert das Laden der Klassen und die
refactoring Aufgaben. Die Klasse \lstinline$ModelClass$ ist eine Abbildung einer
C++ Klasse im Modell. Die Komponenten der Klassen werden in der Klasse
\lstinline$ModelClasses$ zusammengefasst. Hier sind die Attribute, Methoden und
Namespaces der Klassen vertreten. Die Klasse \lstinline$GraphInit$ bildet die
Schittstelle zum \ac{OGDF}.
Der View wird mit den Klassen \lstinline$GraphScene$ und \lstinline$GraphView$
initialisiert. Die UML Items werden vom \lstinline$UmlItemCreator$ erzeugt und
erben alle von der Klasse \lstinline$UmlItem$. Die Klasse
\lstinline$UmlItemList$ ist für die Organisation der angezeigten Items zuständig
und beinhaltet eine Liste aller angezeigten Klassen und Verbindungen. Die Items
werden in drei Gruppen unterteilt: Die erste Gruppe sind die Klassen
(\lstinline$UmlClass$). Um den Quellcode übersichtlich zu halten wurden einige
Funktionen und Elemente in extra Klassen angelegt. Dazu zählen die Gestaltung
des Layouts (\lstinline$ClassLayout$), das Rendern der SVG Grafiken
(\lstinline$SVGRenderer$), die Einträge für die Member
(\lstinline$UmlClassEntry$) und die Button (\lstinline$UmlButton$). Die Klasse
UmlNote soll verwendet werden um zusätzliche Informationen im Klassendiagramm
anzuzeigen. Die Verbindunen (\lstinline$UmlConnector$) werden wiederum in die
einzelnen Verbindungstypen Assoziation (\lstinline$UmlAssoziation$), Aggregation
(\lstinline$UmlAggregation$), Komposition (\lstinline$UmlKomposition$) und
Generalisierung (\lstinline$UmlGeneralization$) aufgeteilt. Die Elemente aus
denen die Verbindungen bestehen, die Linien (\lstinline$GraphicsConnector$) und
Selektoren (\lstinline$UmlLine$\lstinline$Selector$), sind für alle Typen
einheitlich und werden deshalb in der Superklasse \lstinline$UmlConnector$
angelegt.

\subsection{View (A.B.)}
\label{subsec:Impl:View}
Im Folgenden werden die Elemente der View-Komponente der Software vorgestellt.

\subsubsection{Aufbau}
\label{subsec:Impl:Aufbau}
Die View der entwickelten Software basiert auf dem QGraphicsView Framework von Qt.
Da die Anforderungen an die Interaktivität und Funktionalität der Oberfläche über
die bereitgestellte Funktionalität hinausgeht, wurde sowohl die Szene 
(\lstinline$QGraphicsScene$), als auch die Anzeige (\lstinline$QGraphicsView$)
überschrieben. Dadurch besteht die Möglichkeit auf alle Benutzereingaben
einzugehen und diese auf jeder Ebene abzufangen.

Die Elemente, die auf der Szene angezeigt werden, sind abgeleitet von
\lstinline$QGraphicsItem$ und grundsätzlich dynamisch.
%TODO: Was ist hier mit "`Dynamisch"' gemeint?
Die implementierten Elemente können in zwei Gruppen
aufgeteilt werden. Die erste Gruppe besteht aus Elementen, mit denen der Benutzer
direkt interagieren kann. Dazu zählen die angezeigten Klassen
(\lstinline$UMLClass$), die Notizen (\lstinline$UMLNote$) und die Selektoren zum
Verändern der Verbindungsverläufe.
Diese Elemente erben alle von der Klasse \lstinline$UMLItem$, sind selektierbar
und können auf der Szene verschoben werden. Die zweite Gruppe besteht aus den
Verbindungslinien der Elemente. Sie können nicht direkt verändert werden,
sondern zeichnen sich automatisch neu, wenn das Element verändert wird,
das sie verbinden. 

\subsubsection{Visualisierung von Klassen}
Eine Klasse besteht aus mehreren \lstinline$QGraphicsItem$, die schichtenweise
angeordnet sind.
Die unterste Schicht bildet ein Rechteck, das die Größe des Elements definiert.
Es kann selektiert und verschoben werden, wodurch auch alle anderen Schichten
mit verschoben werden. Die zweite Schicht besteht aus mehreren Rechtecken, die
vertikal angeordnet sind. Das erste Element enthält ein
\lstinline$QGraphicsTextItem$, das den Namen der Klasse repräsentiert. Das
zweite Element ist ein schwarzer Balken, der als Trennung zum nächsten Element
fungiert. Das dritte Element enthält die \lstinline$QGraphicsTextItem$ für die
Methoden der Klasse. Das Vierte Element ist wieder ein Trennbalken, an das sich
das letzte Element anschließt, welches die Namen der Attribute enthält.
Der Aufbau ist auch in Abbildung \ref{img:UMLConnection} zu sehen.

\subsubsection{Verbindungen zwischen Klassen}
Zu den implementierten Verbindungen zwischen Klassen gehören die Assoziationen,
Komposition und Aggregation, sowie die Generalisierung. Sie erben alle von der
Klasse \lstinline$UMLConnector$. Der Aufbau einer Verbindung ist in Abbildung
\ref{img:UMLConnection} dargestellt. Eine Verbindung besteht aus mehreren Linien
(\lstinline$GraphicsConnector$) und Selektoren (\lstinline$UMLLineSelector$),
die zum Verschieben der Linie benutzt werden.
Die Verbindung wird mit einer Start- und einer Endklasse initialisiert, die den
Anfangs- und Endpunkt der Verbindung bilden.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.7\textwidth]{UMLConnection.pdf}
\caption{Aufbau einer Verbindung von zwei Klassen}
\label{img:UMLConnection}
\end{center}
\end{figure}

\subsubsection{Interaktion mit Verbindungslinien}
Das orthogonale Layout schreibt vor, dass Linien nur horizontal bzw. vertikal
verlaufen dürfen. Da der Benutzer jedoch alle Elemente einer Verbindung und auch
die Klassen frei verschieben kann, müssen auch Nachbarelemente auf
Benutzerinteraktionen reagieren. Nur so kann gewährleistet werden, das die
Eigenschaften des orthogonalen Layouts erhalten bleiben. Ein Beispiel für das
Problem ist in Abbildung \ref{img:UMLMoveSelektor} zu sehen: Der Benutzer
verschiebt einen Selektor nach unten. Damit alle Linien die Eigenschaften des
orthogonalen Layouts nicht verletzen, muss sich auch der benachbarte Selektor
links im Bild verschieben. Bei einer Verschiebung nach rechts oder links muss
sich auch der obere Selektor verschieben. Das bedeutet, dass alle Selektoren
reagieren müssen, wenn ein Nachbar verschoben wird.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.5\textwidth]{UMLMoveSelektor.pdf}
\caption{Verschiebung eines Selektors}
\label{img:UMLMoveSelektor}
\end{center}
\end{figure}

Eine Ausnahme für dieses Vorgehen stellen die Endpunkte bzw. die Klassen dar.
Ihre Position ist fest und soll nicht verändert werden, wenn ein benachbarter
Selektor verschoben wird. Um die Orthogonalität zu erhalten, werden neue
Selektoren und Linien zu einer Verbindung hinzugefügt. Abbildung
\ref{img:UMLAddSelektor} verdeutlicht das Vorgehen an einem Beispiel. Da der
Benutzer den Selektor zu weit nach unten geschoben hat, kann keine horizontale
Linie mehr zwischen Selektor und Klasse gezeichnet werden. Deshalb wird ein
neuer Selektor zwischen Klasse und verschobenem Selektor eingefügt.
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.5\textwidth]{UMLAddSelektor.pdf}
\caption{Hinzufügen von Selektoren zur Aufrechterhaltung der Orthogonalität}
\label{img:UMLAddSelektor}
\end{center}
\end{figure}

In Abbildung \ref{img:UMLAddSelektor} ist auch zu sehen, dass es nötig sein
kann, den Kopf einer Verbindung zu drehen. Je nach Richtung der ersten Linie
einer Verbindung, muss auch der Kopf der Verbindung ausgerichtet werden. Um die
Anforderungen des orthogonalen Layouts zu erfüllen sind zwar nur $90^\circ$
Schritte erforderlich, um aber auch symmetrische Layouts zu unterstützen, passt
sich der Kopf dem Winkel der Linie an.

