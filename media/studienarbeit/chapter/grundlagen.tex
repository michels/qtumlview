\section{Grundlagen}
\label{sec:Grundlagen}

\subsection{Framework oder Bibliothek? (M.S.)}
\label{subsec:Framework oder Bibliothek}

Üblicherweise wird in der Fachliteratur von Frameworks oder Bibliotheken
gesprochen ohne genau zu definieren, was damit gemeint ist. Dieser Abschnitt
soll mögliche Definitionen aufzeigen und dann festlegen, wie diese Begriffe in
der vorliegenden Arbeit verwendet werden.

Im Kontext der objektorientierten Programmierung wird mit Frameworks oder
Bibliotheken meist eine Sammlung von Klassen bezeichnet, die ein bestimmtes
Problem generisch lösen und damit in anderen Programmen wiederverwendet werden
können. Damit endet auch schon die Gemeinsamkeit. Eine offizielle Definition 
ist nicht zu finden.
Auf Stackoverflow
\footnote{http://stackoverflow.com/questions/148747/what-is-the-difference-between-a-framework-and-a-library}
findet sich eine Diskussion, die sich mit diesem Thema befasst.
Im Allgemeinen werden dort zwei Meinungen vertreten:

\begin{enumerate}
  \item Ein Framework enthält mehrere Bibliotheken
  \item Ein Framework folgt dem \ac{IoC}-Muster (auch Hollywood-Prinzip\footnote{Don't call us, we call you!} genannt)
\end{enumerate}

Auf Wikipedia\footnote{https://en.wikipedia.org/wiki/Software\_framework} finden sich drei weitere Kriterien um zwischen 
Bibliothek und Framework zu unterscheiden:
\begin{description}
	\item[default behavior] - A framework has a default behavior. This default
		behavior must actually be some useful behavior and not a series of no-ops.
	\item[extensibility] - A framework can be extended by the user usually by
		selective overriding or specialized by user code to provide specific
		functionality.
	\item[non-modifiable framework code] - The framework code, in general, is not
		allowed to be modified, excepting extensibility. Users can extend the framework,
		but not modify its code.
\end{description}

Ein Framework ist also eine Programmgerüst, das vom Programmierer auf seine
Bedürfnisse angepasst wird. Es stellt einen Rahmen bereit in dem, an
verschiedenen Stellen, benutzerdefiniertes Verhalten implementiert werden kann.
Es läuft selbstständig und hat ein sinnvolles Standardverhalten.

Während diese Definitionen sehr trennscharf sind, kann das in der Praxis nicht
immer so umgesetzt werden. Oft mischen sich die verschiedenen Konzepte innerhalb
einer Bibliothek oder eines Frameworks. Daher wird in dieser Studienarbeit
Framework und Bibliothek synonym verwendet.

\subsection{Qt Framework}
\label{subsec:QtFramework}
Folgend werden die Funktionen des Qt Frameworks und der daraus verwendeten Komponenten
erläutert.
\subsubsection{Überblick (M.S.)}
\label{subsec:Qt:Ueberblick}
Qt ist ein Framework das zunächst von Trolltech, einer finnischen Firma,
entwickelt wurde. Bis zur Version 3 im Jahr 2001 war Qt relativ unbekannt. Doch
spätestens mit der Version 4 ab dem Jahr 2005 war es international in allen
Branchen bekannt und akzeptiert. Von 2008 bis 2012 wurde es von seinem
Eigentümer Nokia weiter entwickelt und dort vor allem für den Einsatz im Smartphone
umgestaltet.
Heute wird das Framework von Digia vertrieben und in nahezu allen Bereichen
eingesetzt. Von Handys über Navigationssysteme bis hin zur Deutschen
Flugsicherung wird es verwendet aber auch in freien Projekten wie VLC und in
einer der bekanntesten Desktop Environments für Linux, dem \ac{KDE}-Projekt,
wird Qt eingesetzt.

Auch bei den Plattformen hat Qt sich rasant entwickelt. Anfangs wurde nur Linux
unterstützt. Heute läuft es zusätzlich auf Linux Embedded Plattformen,
Handy-Betriebssysteme, Windows, Windows CE, Windows Mobile und Mac OS X.

Wir setzen das Framework in der stabilen Version 4 ein, obwohl gerade die zweite
Beta für Version 5 erschienen ist\footnote{Stand Anfang Dez. 2012} und noch für das
Jahr 2012 die finale Version von Qt 5 erwartet wird. Das Framework 4 ist - wie
Version 5 auch - modular aufgebaut und besteht im Groben aus den folgenden 2
Hauptmodulen und 11 Hilfsmodulen (Siehe Abbildung \ref{fig:Qt:Module}). In der
Version 5 ist eine weitere Modularisierung vorangetrieben worde. Dort wird mehr
Funktionalität aus dem Core-Module und dem GUI-Modul in weitere Hilfsmodule
ausgelagert und die Kohäsion zwischen den Modulen weiter reduziert. Abbildung
\ref{fig:Qt:Module} zeigt die aktuelle Aufteilung der Verwendeten Version 4.

\begin{figure}
\begin{center}
  \includegraphics[width=13cm]{qt_diagram_02.png}
  \caption[Module des Qt-Frameworks]{Module des Qt-Frameworks - Quelle: http://qt.digia.com/Product/Library/}
  \label{fig:Qt:Module}
\end{center}
\end{figure}

Hier zeigen sich die Probleme der Definition aus Kapitel \ref{subsec:Framework
oder Bibliothek} (Framework oder Bibliothek?). Auf der einen Seite enthält das
Framework Qt mehrere Bibliotheken, die hier Module genannt werden, auf der
anderen Seite ist Qt aber nicht konsequent nach dem \ac{IoC}-Muster entwickelt.
Es ist zwar \emph{extensible} (erweiterbar), hat aber nicht immer ein sinnvolles
\emph{default behavior}.

Im Folgenden soll eine kurze Erläuterung der wichtigsten Module und ihrer
Aufgaben erfolgen, soweit es nicht aus dem Modulname hervorgeht \footnote{Quelle
- http://qt.digia.com/Product/Library/}.

\begin{description}
  \item[Core]~
  \begin{itemize}
  		\item Datei Ein-/Ausgabe
  		\item Ereignis- und Objektbehandlung
  		\item Plugins und Settings
  		\item Meta-Objektsystem
  		\item Signale und Slots
   		\item Multithreaded \begin{itemize}
		 	\item Map-Reduce-Framework
 		 	\item Asynchrone Methoden
 			\item Thread-Pool
		\end{itemize}
	\end{itemize}
  \item[GUI]~
  	\begin{itemize}
  		\item Umfangreiche Widget-Bibliothek
  		\item 2D Grafik
  		\item Font-, Style- und Layoutengine
   		\item 2D \& Canvas
			\begin{itemize}
				 \item Qt Graphics View
				 \item Zooming, Rotation, perspektivische Transformation, Animation
				 \item Drag-and-drop
				 \item Grafikbeschleunigung OpenGL und OpenGL ES
			\end{itemize}
	\end{itemize}
  \item[Network]~
	\begin{itemize}
	  \item \ac{SSL}-Support
	  \item \ac{TCP}- und \ac{UDP}-Verbindungen
	  \item Server sockets
	  \item Proxy Unterstützung
	  \item Implementierungen für die Protokolle \ac{HTTP} und \ac{FTP} 
	\end{itemize}
  \item[Declarative]~
	\begin{itemize}
		 \item Deklarativer Ansatz zur Oberflächenbeschreibung
		 \item Trennung von Code und Oberfläche
		 \item Moderne Touch-Oberflächen mit Kacheloptik
	\end{itemize}
\end{description}


Folgend werden die Qt Funktionen ausführlicher erläutert, die in diesem Projekt
zum Einsatz kommen.

\subsubsection{Graphics View Framework (A.B.)}
Ein Bestandteil des \ac{GUI} Moduls im Qt Framework ist das Graphics View
Framework zur Visualisierung von interaktiven 2D Grafiken
\cite{Summerfield2011}. Die Zeichenfläche dieses Frameworks bildet die Klasse
QGraphicsScene, auf der 2D Elemente gezeichnet, positioniert und angezeigt
werden können. Die Szene spannt hierfür ein Koordinatensystem auf, dessen
Ursprung sich im linken oberen Rand des Fensters befindet.

Als grundlegende Elemente beinhaltet das Framework geometrische Objekte wie
Linien, Kreise und Rechtecke. Für die Erstellung von komplexeren Formen sind
auch Polygone und Unterstützungen für Kurven wie z. B. Bézierkurven vorhanden.
Um Grafiken aus mehreren Elementen zu kombinieren, können die Grafikelemente
auch gruppiert oder zu einem Element zusammengefasst werden.
Somit lassen sich mehrere Elemente komfortabel organisieren, verschieben und
rotieren.
Neben den geometrischen Grundformen gibt es auch ''TextItems'' um Texte
darstellen zu können, die auch interaktiv durch den Benutzer verändert werden
können.
Für grafisch anspruchsvolle Anwendungen lassen sich auch Bilder und SVG Grafiken
rendern und darstellen.

Um die interaktive Bedienung des Frameworks zu aktivieren, können vordefinierte
Flags der grafischen Elemente gesetzt werden. Dazu gehört unter anderem die
Selektierbarkeit von Elementen und die Möglichkeit sie zu verschieben.

Die Anzeige der Szene wird durch ein QGraphicsView realisiert. Durch ihn ist es
möglich auch Ausschnitte der Scene darzustellen und dabei nur den Teil zu
rendern, der tatsächlich angezeigt wird, wodurch die Performance des Systems
gesteigert werden kann. Diese Architektur des Frameworks ermöglicht die
Implementierung vieler Features, die eine Anwendung interaktiv bedienbar macht.
Durch die Veränderung der Größe des Ausschnitts ist beispielsweise eine
Zoom-Funktion realisierbar. Das Verschieben des View auf der Scene mithilfe der
Maus, ist vom Framework bereits fertig implementiert und kann mit dem Setzen
einer Flag aktiviert werden.

\subsection{Alternativen zu Qt (M.S.)}
Um Qt mit anderen Bibliotheken zu vergleichen sollen zunächst die
Unterscheidungskriterien festgelegt werden. Die wichtigste Eigenschaft einer
Softwarebibliothek heute ist die Möglichkeit auf vielen verschiedenen
Plattformen eingesetzt zu werden. Das bedeutet, dass man mit der selben
Codebasis in einer heterogenen Umgebung ausliefern kann. Als Mindestanforderung
an den Funktionsumfang für mögliche Alternativen müssen die drei wichtigsten
Desktopplattformen unterstützt werden:
\begin{itemize}
  \item Windows
  \item Linux
  \item MacOS X
\end{itemize}

Neben dieser Eigenschaft ist die Programmiersprache nicht ganz unwesentlich. Da
wir in dieser Arbeit die Möglichkeiten einer UML-Implementierung für den Qt-Creator
untersuchen wollen, sind wir auf die Sprache C++ festgelegt. Denkbar wären auch
andere Sprachen, allerdings wäre der Aufwand der Integration nicht ganz unerheblich
und könnte Thema einer eigenen Studienarbeit sein.

Im Rahmen einer Studienarbeit kommt es auch darauf an, nicht unnötig Kosten zu
verursachen. Und da die Open-Source-Gemeinde weltweit groß genug ist und teilweise
sogar eine bessere Qualität liefert als vergleichbare kommerzielle Produkte, schließen
wir auch kommerzielle Bibliotheken von der Betrachtung aus.

Damit bleiben noch die folgenden Bibliotheken übrig. Diese Liste erhebt jedoch
keinen Anspruch auf Vollständigkeit.
\begin{description}
  \item[wxWidgets] enthält nicht nur Widgets, wie der Name vermuten lässt,
	  sonder unter anderem auch ein Netzwerkmodul, Multithreading-support und
	  Klassen zur Dateibehandlung. Der Memory-foot-print ist nur halb so groß wie
	  bei Qt\footnote{http://www.wxwidgets.org/about/wxwidgets.swf}. Populäre
	  Applikationen, wie zum Beispiel Audacity, BitTorrent oder KICAD, verwenden
	  wxWidgets. Es befindet sich noch aktiv in der Entwicklung.
  \item[GTK+] ist neben wxWidgets der härteste Konkurrent für Qt. GTK+ ist in C
	  geschrieben und wird noch aktiv entwickelt. Es wird unter anderem von GNOME,
	  Firefox und GIMP verwendet und besitzt für jede Plattform eine eigene
	  Display-Engine die für die Darstellungen zuständig ist. Dadurch erscheinen
	  Programme, die mit GTK+ geschrieben sind, oft als "`Fremdkörper"'. Dies wird
	  auch als \emph{non-native look-and-feel} bezeichnet.
  \item[FLTK] hat ebenfalls einen \emph{non-native look-and-feel} und ist noch
	  aktiv in der Entwicklung. Es ist allerdings beschränkt auf die Erstellung
	  von grafischen Oberflächen.
  \item[IUP] Wie GTK+ ist auch IUP in C geschrieben und wird noch aktiv weiter
  	entwickelt, hat jedoch im Vergleich zu GTK+ und FLTK einen \emph{native look-and-feel}.
  \item[Juce] ist ein sehr aktives und umfangreiches Projekt, das von nur einem Programmierer
	  entwickelt wird. Es enthält neben Oberflächenelementen auch Klassen zur Netzwerkprogrammierung,
	  Kryptografie, \ac{JSON}-Parsing, Multithreading, Audioverarbeitung und viele
	  Weitere. Es unterstützt nicht nur die üblichen Betriebssysteme Windows, Linux und Mac OS X, sondern 
	  zusätzlich sogar Android und iOS.
  \item[FOX] - "`Free Objects for X"' ist nicht nur auf X - also Linux - beschränkt, sondern läuft
	  auch unter Windows und Mac OS X. Es beschränkt sich aber auf die
	  Oberflächenentwicklung. Features wie Multithreading, Netzwerkkommunikation,
	  sowie Unterstützung für übliche Dateiformate \ac{SVG} oder \ac{XML} fehlen.
	  Die aktuellste Entwicklerversion 1.7.30 datiert auf Oktober 2011.
  \item[TnFox]~ ist nicht mehr aktiv in der Entwicklung. Es beschreibt sich
  selbst als:
  		\quotation{"`modern, secure, robust,
	 multithreaded, exception aware, internationalisable, portable GUI toolkit
	 library designed for mission-critical work in C++ and Python forked from the
	 FOX library. It replicates the Qt \ac{API} in many
	 places."'}\footnote{http://www.nedprod.com/TnFOX/}
\end{description}

\textbf{Fox} und \textbf{TnFox} disqualifizieren sich durch ihre fehlende
Aktualität. Eine Bibliothek, die nicht mehr weiter entwickelt wird, bringt auch
keine Updates für eventuelle Bugs heraus, die ein Projekt leicht in
Schwierigkeiten bringen kann.

Auch wenn es wegen einiger Embedded-Prozessoren noch eine Existensberechtigung
hat, ist die Sprache C heute doch etwas aus der Mode gekommen. Und wenn man
nicht unbedingt auf C angewiesen ist, so sollte man lieber C++ einsetzen. Durch
das zusätzliche Abstraktionskonzept der Klassen gewinnt die Übersichtlichkeit
und damit die Wartbarkeit des Quellcodes dramatisch. Diese Entscheidung wirft
\textbf{IUP} und \textbf{GTK+} aus dem Rennen.

\textbf{FLTK} scheidet aus, weil es nur auf die Erstellung von Oberflächen
spzialisiert ist. Für größere Projekte sind jedoch auch andere Funktionen
wichtig, wie z. B. Netzwerkkommunikation, Multithreadingunterstützung oder
gängige Dateiformate wie \ac{XML} oder \ac{JSON}.

Die verbleibenden Bibliotheken \textbf{Qt}, \textbf{Juce} und \textbf{wxWidgets}
sind somit nach den vorgegebenen Kriterien nahezu gleichwertig.

Die Anzahl der aktiven Entwickler und der Umfang des umgebenden Ökosystems ist
nicht notwendigerweise ein gutes Kriterium, aber es gibt dennoch einen Hinweis
darauf, wie beliebt die Bibliothek ist. In dieser Disziplin ist Qt ungeschlagen.
Es geht seit der Übernahme durch Digia in einen offenen Entscheidungsprozess
über. Jeder, der ausreichend Reputation hat, kann selbst grundlegende
Entscheidungen mitbestimmen. Dies war lange ein Kritikpunkt an Qt, denn obwohl
es als Open-Source Projekt entwickelt wurde, wurden doch richtungsweisende
Entscheidungen von Nokia bestimmt. Mit dieser Entwicklung, der umfangreichen
Dokumentation und der großen Community ist Qt ein gute Wahl.

\subsection{Qt Creator (M.S.)}
\label{subsec:QtCreator}

Für die Softwareentwicklung mit Qt kommt besonders der Qt-Creator in Frage, da er 
eine hervorragende Integration von Qt bietet. Das äußert sich in verschiendener
Hinsicht:

\begin{enumerate}
  \item Die Integration der Online-Hilfe für alle Qt-Klassen
  \item Unterstützung des Konzepts der Signals und Slots
  \item Integration des User-Interface Designers
\end{enumerate}

Von einer modernen \ac{IDE} erwartet man aber auch Standardfunktionen:

\begin{itemize}
  \item Automatische Codevervollständigung (code completion)
  \item Syntax Highlighting
  \item Debugger
  \item Verschiedene Source-Code-Refactorings (z. B. rename)
  \item Integration von Versionierungstools wie zum Beispiel Git und \ac{SVN}
  \item Code-Folding
  \item Code-Navigation (z. B. Springe zu Deklaration)
\end{itemize}

Ein Screenshot der IDE ist in Abbildung \ref{subsec:Impl:QtCreatorPlugin} auf
Seite \pageref{subsec:Impl:QtCreatorPlugin} zu sehenn. Im professionellen Umfeld
ist jedoch neben diesen Features auch eine modellgetriebene Entwicklung Stand
der Technik. Insbesondere \ac{UML}-Klassendiagramme (siehe Kapitel
\ref{subsec:Impl:Klassendiagramm}) haben sich als sehr hilfreich erwiesen. Die
vorliegende Studienarbeit hat sich zur Aufgabe gemacht, dieses Feature als
Plugin nachzuliefern. Die Anforderungen an das Plugin sind in Kapitel
\ref{subsec:ZieleDerArbeit} beschrieben.

\subsection{\ac{UML} (A.B.)}
\label{subsec:UML}

In der Vergangenheit hat sich die Programmierung von der sehr hardwarenahen
0/1-Re\-prä\-sen\-tation immer weiter zu der menschlichen Vorstellung hin
entwickelt.
Während am Anfang noch Lochkarten gelesen werden mussten um den Computer zu
programmieren, wurde dies später durch Assembler Programmierung ersetzt. Bald
darauf folgten die ersten Hochsprachen. Heute wird hauptsächlich objektorientiert
programmiert. Der Mensch denkt jedoch nicht in Text - er denkt in Bildern.
Die nächste logische Entwicklung ist also das Programmieren in Bildern. Ein
Ansatz dazu ist die Modellgetriebene Softwareentwicklung.
Die Modellgetriebenen Softwareentwicklung versucht eine Abstraktionsschicht 
zwischen der Programmierung des Computers auf der einen Seite und der
abstrakten Vorstellung der Menschen von Programmen oder Abläufen auf der
anderen Seite zu bringen.
Mit der Unified Modeling Language (\ac{UML}) wurde eine Notationsweise zur
Darstellung von objektorientierter Software geschaffen.
Dieser umfasst 14 Diagrammtypen. Dieses Kapitel soll
einen Überblick über bekannte Vertreter geben und das Klassendiagramm in seinem
Kontext darstellen.

\subsubsection{Anwendungs-Fall-Diagramm}
Das Anwendungs-Fall-Diagramm zeigt die Sichtweisen von unterschiedlichen
Benutzergruppen auf die Software. Dabei werden für jede Benutzergruppe 
typische Aufgaben und Anwendungsgebiete definiert, die von der Software 
abgedeckt werden sollen. Das Anwendungs-Fall-Diagramm bleibt hierbei abstrakt, 
in der Sprache der Benutzer und allgemein verständlich. Es wird eingesetzt um 
den Umfang einer Software zu veranschaulichen und die einzelnen 
Anwendungsfälle der Software abzuleiten.
\begin{figure}[hbt]
\begin{center}
\includegraphics[width=0.7\textwidth]{UseCase.pdf}
\caption{Anwendungs-Fall-Diagramm}
\label{img:DomTreeEx}
\end{center}
\end{figure}

\subsubsection{Aktivitätsdiagramm}
Das Aktivitätsdiagramm beschreibt den Ablauf eines Anwendungsfalles und das
Verhalten in den unterschiedlichen Zuständen. Dieses Diagramm kommt vor allem
bei der Planung und Spezifikation der Software zum Einsatz, da es sehr allgemein
ist und kein spezielles Fachwissen voraussetzt, aber dennoch eine gute
Detailgenauigkeit besitzt. Es erleichtert die Kommunikation und hilft
Missverständnisse zu verhindern. Durch diese Eigenschaften ist es oft auch Teil
des Lasten- bzw. Pflichtenheftes von Softwareaufträgen. Ein Beispiel für ein
Aktivitätsdiagramm ist in Abbildung \ref{img:Aktivitätsdiagramm} zu sehen.
\begin{figure}[hbt]
\begin{center}
\includegraphics[width=0.5\textwidth]{aktiv.png}
\caption{Aktivitätsdiagramm zum hinzufügen von Verbindungen}
\label{img:Aktivitätsdiagramm}
\end{center}
\end{figure}

\subsubsection{Sequenzdiagramm}
Das Sequenzdiagramm beschreibt nur einen Ausschnitt von wenigen
Komponenten der Software, zeigt dabei aber den genauen Ablauf und 
die Inhalte der Kommunikation zwischen den Komponenten in einem 
zeitlichen Ablauf. Das Sequenzdiagramm ist dabei sehr detailliert.

\subsection{Klassendiagramm (A.B.)}
\label{subsec:Klassendiagramm}
Ein sehr übersichtliches Bild einer Software kann mit Hilfe eines
Klassendiagramms erstellt werden. Es zeigt die einzelnen Komponenten einer
Software und deren Beziehung untereinander. Im Gegensatz zum
Anwendungs-Fall-Diagramm und Aktivitätsdiagramm, ist es sehr viel spezieller und
auch schon auf die Programmiersprache des Projektes zugeschnitten. Damit
dient es zur internen Planung des Projektes unter den Entwicklern. Es wird zur
Einteilung der Software in verschiedene Komponenten und zur Spezifikation der
internen Softwareschnittstellen verwendet. Eine sehr gute Übersicht über die 
einzelnen Komponenten und den Funktionsumfang ist in \cite{Siebenhaller2003} zu finden.
%\subsubsection{Elemente im Klassendiagramm}
%Die Hauptaufgabe des Klassendiagramms ist die Darstellung der Klassen und ihrer
%Verbindungen. Außer diesen beiden Elementen können auch Zusatzinformationen wie
%Notizen angezeigt werden.
%\begin{itemize}
%  \item Welche Arten von Graphen gibts im Klassendiagramm
%  \item Verwendung
%\end{itemize}

\subsubsection{Klassen}
\label{subsubsec:Klassen}
Klassen werden im Klassendiagramm als Rechteck dargestellt. Ein Beispiel für
eine Klasse ist in Abbildung \ref{img:UMLClass} zu sehen.
Jede Klasse besteht aus drei Bereichen. Der oberste enthält den Namen der
Klasse, der mittlere die Methoden und der unterste die Attribute.
Der Bereich mit dem Namen kann auch zusätzliche Informationen enthalten, wie die
Superklasse oder den Abstraktionsgrad der Klasse.
Auch das Entwurfsmuster wie z. B. Singleton oder die Funktion in einem
Entwurfsmuster wie z. B. Controller können hier genannt werden.
Sehr beliebt ist auch den Stereotype einer Klasse in spitzen Klammern anzugeben.
Wenn es sich z. B. um eine Schnittstelle handelt, kann dies mit << Interface >>
angegeben werden. Die beiden Bereiche mit den Membern enthalten eine Auflistung
aller in der Klasse vorhandenen Member.
Die Sichtbarkeit der Member wird durch die Qt Creator Standard Icons
symbolisiert. Die Attribute bekommen ein blaues Rechteck und die Methoden ein
Rotes. Ein öffentlicher (public) Member besitzt keine weiteren Symbole. Ein
privater Member wird durch ein Schloss, ein geschützter (protected) Member durch
einen Schlüssel symbolisiert.
\begin{figure}[hbt]
\begin{center}
\includegraphics[width=0.2\textwidth]{UMLClass.pdf}
\caption{Visualisierung einer Klasse}
\label{img:UMLClass}
\end{center}
\end{figure}

\subsubsection{Beziehungen}
In Abbildung \ref{img:UMLVerbindungstypen} sind die Verbindungstypen zu sehen,
die für dieses Projekt implementiert wurden. Die Symbole am Anfang der
Verbindungen werden im Folgenden als Köpfe bezeichnet. Der erste Verbindungstyp
ist die Generalisierung und bedeutet, dass die Klasse am Ende der Verbindung
von der am Anfang erbt. Der zweite und dritte Verbindungstyp gehören zu den
Assoziationen. 
\begin{figure}[hbt]
\begin{center}
\includegraphics[width=0.7\textwidth]{UMLVerbindungstypen.pdf}
\caption{Verbindungsarten im Klassendiagramm}
\label{img:UMLVerbindungstypen}
\end{center}
\end{figure}

Der zweite Typ ist eine Aggregation. Sie veranschaulicht eine lose Bindung
der Klassen. Die Eigenschaft lose verdeutlicht, dass die Objekte auch getrennt
voneinander existieren und sinnvoll eingesetzt werden können.
Verglichen werden kann dies mit einem Objekt Autohaus, das eine Liste vom Objekt
Auto besitzt. Da das Auto, auch nachdem es nicht mehr im Autohaus ist, sinnvoll
weiterverwendet werden kann, ist hier eine lose Verbindung der Objekte sinnvoll.
Die Eigenschaften einer Aggregation kommen einem Pointer in C++ am nächsten.
Bei einer Aggregation besitzt das Objekt am Anfang der Verbindung einen
Pointer auf das Objekt am Ende der Verbindung.

Der dritte Typ ist die Komposition. Sie steht für eine feste Bindung der Klassen.
Bei einer festen Bindung kann das Objekt am Ende der Verbindung nicht ohne das
am Anfang existieren. Verglichen werden kann dies mit einem Raum in einem Haus,
welcher nicht sinnvoll existieren kann, ohne dass auch das Haus existiert.
Die feste Verbindung kommt der Referenz am nächsten.

Außer den implementierten Verbindungstypen existieren noch weitere. Beispielsweise 
wird die Assoziation auch direkt verwendet, um eine sehr lose Verbindung zwischen 
Klassen zu symbolisieren. In der Java Welt gibt es auch eine Generalisierung mit 
gestrichelten Linien, die das Erben von einem Interface symbolisiert. Da es in 
C++ aber keine Interfaces gibt, sondern hier mit abstrakten oder virtuellen
Membern gearbeitet wird, ist dieser Verbindungstyp nicht relevant.

\subsection{Roundtrip engineering (M.S.)}
\label{subsec:Roundtrip}

Durch \ac{MDA} wurde eine neue Abstraktion zwischen der menschlichen
Vorstellung und der binären Repräsentation eines Ablaufes geschaffen. Die
menschliche Vorstellung wird im Klassendiagramm sichbar. Der Code entspricht dabei der
binären Repräsentation (der Vorgang des Compilierens wird hier nicht
berücksichtigt). Damit diese beiden Repräsentationen nicht divergieren, muss eine
Synchronisierung geschaffen werden, die Änderungen im Modell auf den Code
abbildet. Diesen Vorgang nennt man generieren oder \emph{Forward-Engineering}.
Das entspricht etwa dem Vorgang eines Compilers bei der Übersetzung einer
Hochsprache in Maschinensprache.

In den generierten Klassen müssen nun die Methoden implementiert werden. Unter
Umständen möchte man dabei aber ein weiteres Attribut hinzufügen oder eine
Methode doch anders nennen. Diese Änderungen müssen auch ihren Weg zurück ins
Modell finden. Der Vorgang dazu heißt \emph{Reverse-Engineering}.
Beim \emph{Forward-Engineering} gibt es zwei Möglichkeiten der Umsetzung:

Entweder wird die komplette Datei neu geschrieben oder es wird auf Tokenebene
nur die Änderung übertragen. Diese Unterscheidung hat Implikationen. Wenn die
ganze Datei geschrieben wird, ist der gesamte Vorgang sehr viel schneller. Damit
werden aber unter Umständen beabsichtigte, semantisch jedoch bedeutungslose
Ergänzungen im Code überschrieben. Man müsste viele Metainformationen,
wie zum Beispiel Kommentare und die Position der Leerzeichen und Klammern im
Model hinterlegen um den nicht semantischen Informationsverlust zu minimieren. Damit
steigt der Verwaltungsaufwand.

Die andere Methode erfordert sehr viel tieferes "`Verständnis"' der Zielsprache.
Es muss also ein Parser verwendet werden. Um eine Methode anders zu nennen
müssen sämtliche Vorkommen dieser Methode in der Header- und der Source-Datei
gefunden und entsprechend geändert werden. Dieser Vorgang wird auch
\emph{Refactoring} genannt. Dabei ist es wichtig, dass Namespaces, Klassen und
Structs und sogar Macros unterstützt werden, denn sonst werden nicht
zusammengehörige Variablen verändert.

Die meisten \ac{UML}-Werkzeuge verwenden die erste Methode - das komplette
Generieren. Dabei gehen aber, wie bereits erwähnt, Informationen verloren. Die
zweite Methode scheint daher die bessere Option. Der Qt-Creator verwendet
bereits einen Parser um das Syntax Highlighting, das Code Folding, die Code
Navigation und eben das \emph{Refactoring} zu realisieren. Der Aufwand zur
Umsetzung ist also geringer. Es könnte nur Probleme mit der Performance geben.
%evtl. Querverweis auf Evaluation und Ausblick

Um Klassen darstellen und modifizieren zu können, müssen die Projektdateien
gelesen und geschrieben werden. Darunter sind die folgenden Operationen
essentiell:

\begin{itemize}
  \item Erstellen bzw. Hinzufügen
  \item Umbenennen
  \item Löschen
\end{itemize}

Diese Operationen gelten für die folgenden Elemente

\begin{itemize}
  \item Struct
  \item Klasse
	\begin{itemize}
  		\item Methode
  		\item Signal
  		\item Slot
  		\item Attribute
  \end{itemize}
\end{itemize}

\subsection{Layout (A.B.)}
\label{subsec:Layout}
Die Anordnung der Elemente in einem Klassendiagramm kann nach verschiedenen
Kriterien erfolgen. Einen guten Überblick über die Vor- und Nachteile der
resultierenden Layouts ist in \cite{Gutwenger2003} zu finden. Folgend werden
einige grundlegende Verfahren erläutert.
% und eine open source Bibliothek vorgestellt.

\subsubsection{Symmetrisches Layout}
Das symmetrische Layout gibt ein Raster vor, auf dem die Knoten des Graphen
platziert werden dürfen. Das hat den Vorteil, dass alle Knoten einer Ebene auch
auf derselben Höhe liegen und Knoten von verschiedenen Ebenen direkt
untereinander angeordnet sind. Die Verbindungslinien werden direkt von einem
Knoten zum anderen gezeichnet.

\subsubsection{Orthogonales Layout}
Das orthogonale Layout ist eine Erweiterung des symmetrischen Layouts, bei dem
die  Verbindungslinien ausschließlich vertikal oder horizontal gezeichnet werden. Der
Vorteil bei dieser Art der Darstellung ist, dass Kreuzungen von
Verbindungslinien gut erkennbar sind. Nachteile sind, dass mehr Linien für eine
Verbindung nötig sind und sich die Strecke, im Gegensatz zum direkten Weg der Verbindung, 
verlängert. Insgesamt macht das Layout für den Betrachter einen
sehr ordentlichen Eindruck und wird deshalb gerne verwendet.

\subsubsection{Hierarchisches Layout}
Das hierarchische Layout ordnet die Knoten des Graphen nach ihrer Hierarchie.
Bei Klassendiagrammen ist es sehr wichtig, die Vererbungshierarchien der
Software korrekt wiederzugeben. Nur so kann ein schneller und widerspruchsfreier
Überblick über die Software präsentiert werden.

Um übersichtliche Klassendiagramme zu zeichnen sollte das Layout eine
Kombination aus einem hierarchischen und orthogonalen Layout sein.

%\subsubsection{Open Graph Drawing Framework (OGDF)}

